﻿<?xml version="1.0" encoding="utf-8"?>
<sectionSubPage guid="66f072c1-1ee3-4c57-9d1c-06761801597a" id="4385" nodeName="Pricing factors for borrowing" isDoc="" updated="2019-08-07T16:41:24.2430000Z" parentGUID="a6501c2d-57f8-45aa-b8fc-2f2385e47b17" nodeTypeAlias="sectionSubPage" templateAlias="SectionSubPage" sortOrder="2" published="false" isBlueprint="false">
  <backToLink></backToLink>
  <bottomContent></bottomContent>
  <displayTagLine></displayTagLine>
  <doNotUseParentPageHeader>0</doNotUseParentPageHeader>
  <hideFSCSBox>1</hideFSCSBox>
  <icons></icons>
  <iconsBackgroundColour></iconsBackgroundColour>
  <metaDescription></metaDescription>
  <metaKeywords></metaKeywords>
  <pageHeader></pageHeader>
  <pageTitle></pageTitle>
  <promoBanner></promoBanner>
  <sideBarModules></sideBarModules>
  <tabs></tabs>
  <tagLine></tagLine>
  <topContent><![CDATA[<h2>Pricing factors for borrowing</h2>
<p>The price we charge for our loans and overdrafts has four components:</p>
<p><strong>The interest cost of the money we need to raise to fund your loans and overdrafts</strong></p>
<p>We have a traditional banking business model using only retail deposits to fund loans and overdrafts as we consider retail deposits to be most stable source of funding. We have to pay a competitive rate of interest to attract such deposits.</p>
<p><strong>The credit risk we carry in making loans and overdrafts</strong></p>
<p>The higher the level of risk that a customer may not be able to repay a loan, the higher the price must be to cover any potential loss. We will take into account your past history of loan and overdraft servicing, in addition to requesting financial statements from the last 3 years. In most cases we will ask you to provide security for your loans and overdrafts. The stronger the security is, the lower the probability is that we will incur a loss, and the lower the price we will charge.</p>
<p><strong>The cost of capital in making loans and overdrafts</strong></p>
<p>For every loan we make, we are required by our regulators to set capital aside to ensure depositors will get their money back, even if that loan isn’t repaid. Capital costs us money because we cannot lend it out. We need to cover that cost by pricing loans and overdrafts appropriately.</p>
<p><strong>Profit margin</strong></p>
<p>We are a commercial organisation and we need to make a profit, not just cover our costs. This way we can provide a return to our shareholders who invested capital in our business, and have the money to grow, invest in new technology and improve our services.</p>
<p>When considering loan application, we carry out affordability and creditworthiness assessments. Loans will not be issued on the value of security alone should affordability and creditworthiness assessments fail.</p>]]></topContent>
  <umbracoInternalRedirectId></umbracoInternalRedirectId>
  <umbracoNaviHide>0</umbracoNaviHide>
  <umbracoRedirect></umbracoRedirect>
  <umbracoUrlAlias></umbracoUrlAlias>
</sectionSubPage>