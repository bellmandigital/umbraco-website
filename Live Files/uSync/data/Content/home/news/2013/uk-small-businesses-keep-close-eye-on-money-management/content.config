﻿<?xml version="1.0" encoding="utf-8"?>
<news guid="07bd2724-2369-4e90-81d3-9f362419014b" id="2075" nodeName="UK small businesses keep close eye on money management" isDoc="" updated="2019-08-07T10:38:34.2000000Z" parentGUID="10260db7-f216-4215-8492-9d093e831450" nodeTypeAlias="news" templateAlias="News" sortOrder="2" published="true" isBlueprint="false">
  <content><![CDATA[<h3>Owner managed businesses avoid taking on additional debts</h3>
<p>Much has been written about the role of banks in providing small businesses with access to affordable financing in the current climate. However, the latest Owner Managed Business (OMB) Barometer research report from Bank of Cyprus UK indicates that OMBs are seeking to avoid taking on additional debt as they nurse their businesses back to recovery.</p>
<p>The research revealed that nearly six in 10 OMBs (58%) had no requirement for additional finance in the last quarter, with a third (34%) opting to inject personal funds rather than seek external funding.</p>
<p>Looking forward, just over one in 10 respondents (12%) planned to apply for increased facilities from their bank in the next 12 months, with a similar number (11%) citing access to funding as a significant barrier to growth.</p>
<p>Despite some signs of cautious optimism, the results reflect unwillingness on the part of OMBs to increase their borrowings until their business shows signs of sustained recovery.</p>
<p>Even so, OMBs continue to be pessimistic about the response they would get from their bank if they applied for new facilities, with just one in four respondents (24%) feeling confident that they would get a positive response.</p>
<p>Tony Leahy at Bank of Cyprus UK, commented: “Given the prolonged difficult climate that OMBs have had to manage through, any signs of optimism should be welcomed and supported. Whilst businesses are right to keep a tight grip on their credit management and cash flow until recovery is more evident, it is disappointing that many respondents lack confidence that their bank would support them with additional facilities if needed”</p>
<p>Tony Leahy continues: “To support recovery, it is vital that OMBs and their banks are engaged in dialogue, to avoid any shocks, but also to remove any doubt or uncertainty that may inhibit confidence in planning for the future”.</p>
<p>For further information please contact:</p>
<p>Rachel Finlay, Teamspirit Public Relations, 0207 360 7877,<span> </span><a href="mailto:rfinlay@teamspiritpr.com">rfinlay@teamspiritpr.com</a></p>
<p>Kate Mallett, Teamspirit Public Relations, 0207 360 7877,<span> </span><a href="mailto:kmallet@teamspiritpr.com">kmallet@teamspiritpr.com</a> </p>]]></content>
  <date>2013-09-12T00:00:00</date>
  <image></image>
  <snippet><![CDATA[The latest “Owner Managed Business Barometer” research report from Bank of Cyprus UK indicates that OMBs are seeking to avoid taking on additional debt as they nurse their businesses back to recovery.]]></snippet>
  <thumbnail></thumbnail>
</news>