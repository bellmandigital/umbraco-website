﻿<?xml version="1.0" encoding="utf-8"?>
<contentPage guid="b364f54a-9b1f-4c80-9800-9f1ab48e9b23" id="1598" nodeName="Security" isDoc="" updated="2019-11-19T11:12:43.9630000Z" parentGUID="b170de99-8b84-4c7e-b62d-3aa60aee3c23" nodeTypeAlias="contentPage" templateAlias="ContentPage" sortOrder="15" published="true" isBlueprint="false">
  <backToLink></backToLink>
  <bottomContent></bottomContent>
  <displayTagLine>1</displayTagLine>
  <doNotUseParentPageHeader>0</doNotUseParentPageHeader>
  <icons></icons>
  <iconsBackgroundColour></iconsBackgroundColour>
  <metaDescription></metaDescription>
  <metaKeywords></metaKeywords>
  <pageHeader><![CDATA[umb://document/400325949bac408784d9666afbf6b168]]></pageHeader>
  <pageTitle></pageTitle>
  <promoBanner><![CDATA[umb://document/314fa7e0b2c741ddbe9fe2df7955a97d]]></promoBanner>
  <tabs></tabs>
  <tabStyle></tabStyle>
  <tagLine></tagLine>
  <topContent><![CDATA[<h2>Security</h2>
<p><strong>Taking your security seriously</strong></p>
<p>Making banking safe for our customers is one of our key priorities.</p>
<p>In this section you will find information on what we do to safeguard your security, you will also find some helpful tips on what you can do to ensure you do not fall victim to fraud.</p>
<p>If you have any concerns or questions, please call <a data-udi="umb://document/3ae2ee8f861a44cdabd097dec60009f0" href="/{localLink:umb://document/3ae2ee8f861a44cdabd097dec60009f0}" title="Contact">Customer Service</a>.</p>
<p><strong>Worried about fraud?</strong></p>
<p>If you think you’ve been the victim of fraud <a href="/security/worried-about-fraud/" title="Worried about fraud?">click here</a> for helpful information on what to do and who to contact.</p>
<p><strong>Top tips for protecting yourself against fraud?</strong></p>
<p><a href="/security/protecting-yourself-from-cheque-fraud/" target="_top" title="Protecting yourself from cheque fraud">Click here</a> for our top tips on how to protect yourself from the risk of fraud</p>
<p><strong>Protecting yourself online and over the phone</strong></p>
<ul>
<li><a data-udi="umb://document/0eb33e1dedab42ceb5f3ca4b3c655e3f" href="/{localLink:umb://document/0eb33e1dedab42ceb5f3ca4b3c655e3f}" target="_top" title="Protecting yourself online and over the phone">Protecting your computer</a></li>
<li><a data-udi="umb://document/0eb33e1dedab42ceb5f3ca4b3c655e3f" href="/{localLink:umb://document/0eb33e1dedab42ceb5f3ca4b3c655e3f}" target="_top" title="Protecting yourself online and over the phone">Online Banking</a></li>
<li><a data-udi="umb://document/0eb33e1dedab42ceb5f3ca4b3c655e3f" href="/{localLink:umb://document/0eb33e1dedab42ceb5f3ca4b3c655e3f}" target="_top" title="Protecting yourself online and over the phone">Online shopping</a></li>
<li><a data-udi="umb://document/0eb33e1dedab42ceb5f3ca4b3c655e3f" href="/{localLink:umb://document/0eb33e1dedab42ceb5f3ca4b3c655e3f}" target="_top" title="Protecting yourself online and over the phone">Common security scams</a></li>
<li><a data-udi="umb://document/0eb33e1dedab42ceb5f3ca4b3c655e3f" href="/{localLink:umb://document/0eb33e1dedab42ceb5f3ca4b3c655e3f}" target="_top" title="Protecting yourself online and over the phone">Laptops and Mobiles</a></li>
</ul>
<p><strong>Protecting your Identity</strong></p>
<ul>
<li><a data-udi="umb://document/5893fd5960e44bd0b243a9bbf17b8f54" href="/{localLink:umb://document/5893fd5960e44bd0b243a9bbf17b8f54}" title="Protecting your identity">Preventing ID theft</a></li>
</ul>
<p><strong>Protecting your debit and credit cards</strong></p>
<ul>
<li><a data-udi="umb://document/496fb415f7b04911a3d7a70972493001" href="/{localLink:umb://document/496fb415f7b04911a3d7a70972493001}" title="Safety tips">Safety tips</a></li>
<li><a data-udi="umb://document/cd417ea7401b4b3ea99421a340f93315" href="/{localLink:umb://document/cd417ea7401b4b3ea99421a340f93315}" title="Text alerts">Text alerts</a></li>
<li><a data-udi="umb://document/006e0c25ad0d4aafa347ef8aafc713f0" href="/{localLink:umb://document/006e0c25ad0d4aafa347ef8aafc713f0}" title="Verified by Visa">‘Verified by Visa’</a></li>
<li><a data-udi="umb://document/0f8a396a646c47b4b2aeb7bbd23d4fb3" href="/{localLink:umb://document/0f8a396a646c47b4b2aeb7bbd23d4fb3}" title="Going abroad">Going abroad</a></li>
</ul>
<p><strong>Protecting yourself from cheque fraud</strong></p>
<ul>
<li><a data-udi="umb://document/762291965bb44848923f0283976f81d9" href="/{localLink:umb://document/762291965bb44848923f0283976f81d9}" target="_top" title="Accepting and paying in cheques">Accepting and paying in cheques</a></li>
<li><a data-udi="umb://document/eb18b1223cf242a88eaaa37a2d2ff7e3" href="/{localLink:umb://document/eb18b1223cf242a88eaaa37a2d2ff7e3}" target="_top" title="Issuing cheques">Issuing cheques</a></li>
<li><a data-udi="umb://document/9d5383a7ae2b465aa4a56adaff78dbbb" href="/{localLink:umb://document/9d5383a7ae2b465aa4a56adaff78dbbb}" target="_top" title="Chequebooks">Chequebooks</a></li>
</ul>
<p>We have also put together a list of further <a data-udi="umb://document/dde31a00b6f0429fa3a3632d831a7324" href="/{localLink:umb://document/dde31a00b6f0429fa3a3632d831a7324}" target="_top" title="Useful security and fraud protection links">security and fraud prevention links</a> that you may find useful.<br /><br /><strong>Important information</strong></p>
<p>All of the external sites you can reach through the Cynergy Bank website are here to help you. However, we are not responsible for and do not endorse the content, accuracy or availability of any of these external sites and products which you may wish to visit or install.</p>
<p> </p>]]></topContent>
  <umbracoInternalRedirectId></umbracoInternalRedirectId>
  <umbracoNaviHide>0</umbracoNaviHide>
  <umbracoUrlAlias></umbracoUrlAlias>
</contentPage>