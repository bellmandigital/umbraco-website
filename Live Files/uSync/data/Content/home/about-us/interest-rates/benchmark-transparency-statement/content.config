﻿<?xml version="1.0" encoding="utf-8"?>
<contentPage guid="5c142cab-ba3e-411d-97e7-1dac8f5b8980" id="2717" nodeName="Benchmark Transparency Statement" isDoc="" updated="2018-10-30T16:29:23.1970000Z" parentGUID="dad3d7c9-db44-4e14-9788-bffc1389f288" nodeTypeAlias="contentPage" templateAlias="ContentPage" sortOrder="14" published="false" isBlueprint="false">
  <backToLink></backToLink>
  <bottomContent></bottomContent>
  <displayTagLine></displayTagLine>
  <doNotUseParentPageHeader>0</doNotUseParentPageHeader>
  <icons></icons>
  <iconsBackgroundColour></iconsBackgroundColour>
  <metaDescription></metaDescription>
  <metaKeywords></metaKeywords>
  <pageHeader></pageHeader>
  <pageTitle><![CDATA[Benchmark Transparency Statement]]></pageTitle>
  <promoBanner></promoBanner>
  <tabs></tabs>
  <tabStyle></tabStyle>
  <tagLine></tagLine>
  <topContent><![CDATA[<h2>Benchmark Transparency Statement</h2>
<h3>For the attention of clients of Bank of Cyprus UK Limited</h3>
<p>When you enter into a transaction, such as a loan agreement, lending facility or deposit with Bank of Cyprus UK Limited (“Bank of Cyprus UK” or “Bank”), the payments and settlements due to or from you (and any subsequent valuation of the transaction) may be determined by reference to the level of a Benchmark.</p>
<p>For Benchmarks regulated under the Regulation (EU) No. 2016/1011, the Bank acts in the capacity of User.<br /><br /><strong>1. Benchmark User</strong></p>
<p>Bank of Cyprus UK is not an Administrator or a Contributor for any Benchmark used for products or transactions offered to its customers. Bank of Cyprus UK is only a User of the Benchmark (i.e. LIBOR) that is administered by ICE Benchmark Administration Limited (“IBA”). IBA is authorised and regulated by the Financial Conduct Authority (“FCA”) in the UK.</p>
<p>In practice, Bank of Cyprus UK may reference the Benchmark for internal purposes or use the Benchmark on its own in products, services or transactions which we provide or carry out with you. The Benchmark may be used for the purpose of determining payments, settlements or deliveries due to or from you as a result of a loan agreement, lending facility, deposit, or in the valuation of any such loan, lending facility or deposit.</p>
<p>Where Bank of Cyprus UK makes a regulated use of a Benchmark, in the event of a material change or cessation of that Benchmark by its Administrator, a suitable replacement or alternative Benchmark will be referenced to substitute the Benchmark no longer provided.</p>
<p>The FCA has indicated that banks will not be required to contribute to LIBOR after 2021. LIBOR may cease or materially change at that time. Contracts entered into with Bank of Cyprus UK may contain provisions relating to fall-back rates to be applied in that event.</p>
<p><strong>2. Interest rate Benchmark</strong></p>
<p>Certain loans offered by the Bank are linked to LIBOR. LIBOR stands for the London Interbank Offered Rate and is a key benchmark rate of interest at which some the world’s leading banks offer to lend money to one another and serves as the reference rate for calculating interest rates on various loans including Bank of Cyprus UK business loans.</p>
<p>LIBOR rates are calculated on a daily basis for borrowing periods ranging from overnight to one year and made public by the ICE LIBOR. Further information can be found at<span> </span><a rel="noopener" href="http://www.theice.com/iba/libor" target="_blank">www.theice.com/iba/libor</a>.</p>
<p>Bank of Cyprus UK reviews and assesses its use of Benchmarks and the effectiveness of its controls and procedures. The Bank uses the three month British Pound Sterling LIBOR as its reference rate for business loans. This rate can fluctuate significantly over time as illustrated in the table below:</p>
<p>Source:<span> </span><a rel="noopener" href="http://global-rates.com/interest-rates/libor/british-pound-sterling/gbp-libor-interest-rate-3-months.aspx" target="_blank">http://global-rates.com/interest-rates/libor/british-pound-sterling/gbp-libor-interest-rate-3-months.aspx</a></p>
<p><strong>Opening rate per year:</strong></p>
<p>02 January 2018 0.51906%<br />03 January 2017 0.37025%<br />04 January 2016 0.59125%<br />02 January 2015 0.56338%<br />02 January 2014 0.52500%<br />02 January 2013 0.51500%<br />03 January 2012 1.08406%<br />04 January 2011 0.75750%<br />04 January 2010 0.60938%<br />02 January 2009 2.70500%<br />02 January 2008 5.89000%<br /><br />Bank of Cyprus UK will adjust the interest rate applied to a business loan based on the three month British Pound Sterling LIBOR rate at the end of each three month period. Dependent on the interest rate that date may mean either a decrease or an increase in the interest rate applied to a loan facility.</p>
<p>For example, the difference in interest costs of a 1% increase or decrease of three month British Pound Sterling LIBOR for a £1 million loan is £10,000 per annum.</p>
<p><strong>3. Definitions</strong></p>
<p><strong>“Benchmark”</strong> means any Index by reference to which the amount payable under a financial instrument or a financial contract, or the value of a financial instrument, is determined, or an index that is used to measure the performance of an investment fund with the purpose of tracking the return of such index or of defining the asset allocation of a portfolio or of computing the performance fees.</p>
<p><strong>“Benchmark Methodology”</strong> means the written rules and procedures according to which information is collected and the Benchmark is determined.</p>
<p><strong>“Benchmark Regulation”</strong> is the Regulation (EU) No. 2016/1011 of the European Parliament and of the Council of 8 June 2016 on indices used as benchmarks in financial instruments and financial contracts or to measure the performance of investment funds and amending Directives 2008/48/EC and 2014/17/EU and Regulation (EU) No 596/2014.</p>
<p><strong>“Index”</strong> means any figure:</p>
<p>(a) That is published or made available to the public;<br />(b) That is regularly determined:(i) Entirely or partially by the application of a formula or any other method of calculation, or by an assessment; and<br />(ii) On the basis of the value of one or more underlying assets or prices, including estimated prices, actual or estimated interest rates, quotes and committed quotes, or other values or surveys.</p>
<p class="small">[1] Source: Title I “Subject Matter, Scope and Definitions”, Article 3 of Regulation (EU) No. 2016/1011.</p>
<p><strong>“Interest rate Benchmark”</strong> means a benchmark which for the purposes of point (1)(b)(ii) of this paragraph is determined on the basis of the rate at which banks may lend to, or borrow from, other banks, or agents other than banks, in the money market.</p>
<p><strong>“Regulated Market or Exchange”</strong> means a market or exchange that is regulated and/or supervised by a governmental or statutory regulatory authority.</p>
<p><strong>“Use of Benchmark”</strong> means:</p>
<p>(a) Issuance of a financial instrument which references an index or a combination of indices;</p>
<p>(b) Determination of the amount payable under a financial instrument or a financial contract by referencing an index or a combination of indices;</p>
<p>(c) Being a party to a financial contract which references an index or a combination of indices;</p>
<p>(d) Providing a borrowing rate that is calculated as a spread or mark-up over an index or a combination of indices and that is solely used as a reference in a financial contract to which the creditor is a party;</p>
<p>(e) Measuring the performance of an investment fund through an index or a combination of indices for the purpose of tracking the return of such index or combination of indices, of defining the asset allocation of a portfolio, or of computing the performance fees.</p>
<p>Bank of Cyprus UK Limited is authorised by the Prudential Regulation Authority and regulated by the Financial Conduct Authority and the Prudential Regulation Authority. Eligible deposits with Bank of Cyprus UK Limited are protected up to a total of £85,000 by the Financial Services Compensation Scheme, the UK’s deposit guarantee scheme. Any deposits you hold above the protected limit are unlikely to be covered. Further information is available at<span> </span><a rel="noopener" href="http://www.fscs.org.uk/" target="_blank">www.fscs.org.uk/</a></p>
<p>Bank of Cyprus UK Limited subscribes to the Standards of Lending Practice published by the Lending Standards Board which covers good practice in relation to loans, overdrafts and other types of lending that we do not currently offer. Further information is available at<span> </span><a rel="nofollow noopener noreferrer" href="http://www.lendingstandardsboard.org.uk/the-slp/" target="_blank">www.lendingstandardsboard.org.uk/the-slp/</a></p>]]></topContent>
  <umbracoInternalRedirectId></umbracoInternalRedirectId>
  <umbracoNaviHide>0</umbracoNaviHide>
  <umbracoUrlAlias></umbracoUrlAlias>
</contentPage>