﻿<?xml version="1.0" encoding="utf-8"?>
<contentPage guid="82b62697-65d9-4002-ab92-d27e78115f53" id="4483" nodeName="Family Business report" isDoc="" updated="2019-12-03T07:20:01.4170000Z" parentGUID="e4c36b6f-8263-4651-a463-20de02b6bfaa" nodeTypeAlias="contentPage" templateAlias="ContentPage" sortOrder="21" published="true" isBlueprint="false">
  <backToLink></backToLink>
  <bottomContent></bottomContent>
  <displayTagLine>1</displayTagLine>
  <doNotUseParentPageHeader>0</doNotUseParentPageHeader>
  <icons></icons>
  <iconsBackgroundColour></iconsBackgroundColour>
  <metaDescription></metaDescription>
  <metaKeywords></metaKeywords>
  <pageHeader><![CDATA[umb://document/095c301179ef41f294a814fa29136470]]></pageHeader>
  <pageTitle><![CDATA[Family Business report]]></pageTitle>
  <promoBanner></promoBanner>
  <tabs></tabs>
  <tabStyle></tabStyle>
  <tagLine></tagLine>
  <topContent><![CDATA[<h2>Family Business Report</h2>
<p><a rel="noopener" data-udi="umb://document/322b12fbfb8448f086fdd1dc410832fe" href="/%7BlocalLink:umb://document/322b12fbfb8448f086fdd1dc410832fe%7D" target="_blank" title="Cynergy Bank Family Business Perspectives" class="button blue">Download report</a></p>
<p>Family run businesses are instrumental to the stability of the UK economy. Currently there are 4.7 million family-owned firms in the UK, contributing about £460 billion to UK GDP and employing approximately 12 million people according to the Institute for Family Business.</p>
<p>Our experience is that family firms benefit from a strong culture and values, and promote stability by taking a longer-term business outlook.</p>
<p>To really understand the UK’s family business leaders, it’s crucial to understand the environment in which they operate so we commissioned our new study to better understand the needs of our customers.</p>
<p>We know many of them see no distinction between their personal and business finance and we are committed to strengthening our blended offering for family-run firms. That’s why earlier this year we introduced products to support family businesses and innovation.</p>
<p>In May, we introduced Bridging Finance, a short-term finance option that provides a funding solution where clients have a timing issue in raising more conventional funding, with a known and proven repayment exit plan.</p>
<p>We also launched a £500 million Property Entrepreneurs Fund to help property entrepreneurs continue to invest and make the most of the opportunities during Britain’s departure from the European Union.</p>
<p>We hope the findings amongst 1,008 SMEs in the UK, of which 543 are defined as family businesses, reassure you that you are not alone.</p>
<p><strong>Main findings</strong></p>
<ul>
<li>Family firms find it harder to get finance and adapt to the digital age compared with other SMEs.</li>
<li>Family business owners were more uncertain about the current economic outlook than non-family SMEs. They also reported higher levels of anxiety about access to finance, with one in six saying it was a big challenge compared with one in ten non-family businesses.</li>
<li>Almost eight in ten family business owners say family firms benefit from a strong culture and set of shared values. However, they also wish they could put more distance between their work and personal lives.</li>
<li>More than half say there are no clear boundaries and that disagreements about how to run the business can create tensions.</li>
</ul>
<p><a rel="noopener" data-udi="umb://document/322b12fbfb8448f086fdd1dc410832fe" href="/{localLink:umb://document/322b12fbfb8448f086fdd1dc410832fe}" target="_blank" title="Cynergy Bank Family Business Perspectives" class="button blue">Download report</a></p>]]></topContent>
  <umbracoInternalRedirectId></umbracoInternalRedirectId>
  <umbracoNaviHide>1</umbracoNaviHide>
  <umbracoUrlAlias><![CDATA[family-business-report]]></umbracoUrlAlias>
</contentPage>