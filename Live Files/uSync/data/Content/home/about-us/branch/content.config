﻿<?xml version="1.0" encoding="utf-8"?>
<contentPage guid="f778841c-2e97-4a05-ba1d-0aabb1f2d637" id="4699" nodeName="Branch" isDoc="" updated="2020-01-06T14:45:02.9230000Z" parentGUID="e4c36b6f-8263-4651-a463-20de02b6bfaa" nodeTypeAlias="contentPage" templateAlias="ContentPage" sortOrder="25" published="true" isBlueprint="false">
  <backToLink></backToLink>
  <bottomContent></bottomContent>
  <displayTagLine>1</displayTagLine>
  <doNotUseParentPageHeader>0</doNotUseParentPageHeader>
  <icons></icons>
  <iconsBackgroundColour></iconsBackgroundColour>
  <metaDescription></metaDescription>
  <metaKeywords></metaKeywords>
  <pageHeader><![CDATA[umb://document/371387430bde464fadc285ae8bd69ebb]]></pageHeader>
  <pageTitle></pageTitle>
  <promoBanner></promoBanner>
  <tabs></tabs>
  <tabStyle></tabStyle>
  <tagLine></tagLine>
  <topContent><![CDATA[<h2>Our Southgate branch is closing on 6 March 2020</h2>
<p>Following a careful review of our services, and how our customers are banking with us, we have taken the decision to close our Southgate branch on 6 March 2020.  We recognise that our customers’ needs are changing and fewer of our customers are using the branch than ever before. The majority of our customers now find it more convenient to do their day to day banking using <a data-udi="umb://document/93f7b4aa02674c20ae13e4f7dc089c23" href="/{localLink:umb://document/93f7b4aa02674c20ae13e4f7dc089c23}" title="Online Banking">Online Banking</a> and we’ll therefore be focusing our efforts on making further improvements to this service.</p>
<p>The <a rel="noopener" data-udi="umb://document/dd7d64dd46494b58a91d13629c372782" href="/{localLink:umb://document/dd7d64dd46494b58a91d13629c372782}" target="_blank" title="Branch information sheet" data-anchor="#">Information Sheet</a> and <a href="#Impact" data-anchor="#Impact">Customer impact statement</a> provides further details about the change and all the ways you can continue to bank with us.</p>
<p>We have also made some changes to our Conditions to reflect both the branch closure and some enhanced security measures we are putting in place for online transactions. These are available to view below. Please contact us if you would like us to send you a copy in the post.</p>
<p><a rel="noopener" data-udi="umb://document/de2c3aa1a2e6454691a17bdc8ce624af" href="/{localLink:umb://document/de2c3aa1a2e6454691a17bdc8ce624af}" target="_blank" title="Personal Current Account Conditions">Personal Current Account Conditions</a> (Nov 2019 version)</p>
<p><a rel="noopener" data-udi="umb://document/fac8bee308304cc982f298ecad85f54e" href="/{localLink:umb://document/fac8bee308304cc982f298ecad85f54e}" target="_blank" title="Business Current Account Conditions">Business Current Account Conditions</a> (Nov 2019 version)</p>
<p>We would like to thank you for continuing to bank with us and reassure you that this branch closure doesn’t affect your account in any other way.</p>
<p>If you have any additional questions about the branch closure please contact your Relationship Manager or <a data-udi="umb://document/3ae2ee8f861a44cdabd097dec60009f0" href="/{localLink:umb://document/3ae2ee8f861a44cdabd097dec60009f0}" title="Contact">contact us</a>.</p>
<p>Our Southgate office will remain open for business customers by appointment only, the rest of the operations at our Southgate office are unaffected by the branch closure.</p>
<h3><a id="Impact"></a>Customer impact statement</h3>
<h4>Usage of Southgate branch</h4>
<p>A reduction in branch usage by customers has been identified.</p>
<p><img style="width: 500px; height: 496.1612284069098px;" src="/media/2422/customer-impact-statement.png?width=500&amp;height=496.1612284069098" alt="Customer Impact Statement - usage graphs" data-udi="umb://media/bc7026c915e048409b30d74373bd011d" /></p>
<p>We have considered the following to inform our decision to close this branch:</p>
<ul>
<li>The services offered at the Branch and the number of personal and business customers who use these services</li>
<li>Change in the way customers prefer to interact with the Bank using alternative means</li>
<li>The number of vulnerable customers who are more dependent on this branch</li>
<li>The distance to alternative banking services</li>
</ul>
<p>Following an in-depth customer impact assessment, alternative ways to bank with us have been deemed suitable and we are confident that customers can comfortably continue to bank with us using these alternative services.</p>
<p> </p>]]></topContent>
  <umbracoInternalRedirectId></umbracoInternalRedirectId>
  <umbracoNaviHide>0</umbracoNaviHide>
  <umbracoUrlAlias><![CDATA[branch]]></umbracoUrlAlias>
</contentPage>