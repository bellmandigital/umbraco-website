﻿<?xml version="1.0" encoding="utf-8"?>
<sectionSubPage guid="21e1b07b-b5e0-4faa-805c-e0947e4a5088" id="1368" nodeName="Overview" isDoc="" updated="2018-10-24T16:36:04.0970000Z" parentGUID="8d81c55f-849f-46a4-ad60-2027c8073dd0" nodeTypeAlias="sectionSubPage" templateAlias="SectionSubPage" sortOrder="6" published="true" isBlueprint="false">
  <backToLink></backToLink>
  <bottomContent></bottomContent>
  <displayTagLine></displayTagLine>
  <doNotUseParentPageHeader>0</doNotUseParentPageHeader>
  <hideFSCSBox>1</hideFSCSBox>
  <icons></icons>
  <iconsBackgroundColour></iconsBackgroundColour>
  <metaDescription></metaDescription>
  <metaKeywords></metaKeywords>
  <pageHeader></pageHeader>
  <pageTitle></pageTitle>
  <promoBanner></promoBanner>
  <sideBarModules></sideBarModules>
  <tabs></tabs>
  <tagLine></tagLine>
  <topContent><![CDATA[<h2>Property lending</h2>
<p>Whether you’re an existing customer or new to Cynergy Bank, we are dedicated to meeting your financial needs across a range of sectors.</p>
<p>We have been operating successfully in the UK since 1955 with a simple and straightforward business model. We offer a secure and consistently attractive range of products for UK savers and offer small and owner-managed businesses an attentive and responsive banking relationship by providing a range of flexible products and services that can be tailored to suit every business.</p>
<p>Your relationship manager can help you identify the most appropriate financing option to meet your needs whether you’re investing, developing or dealing in property. We use our expert understanding of the commercial property market, combined with our business sector specialism, to provide financing solutions to fit your clients’ needs.</p>
<p>We can arrange finance for:</p>
<ul>
<li>Purchasing or refinancing investment properties</li>
<li>Expanding an investment portfolio Refurbishing investment property</li>
<li>Commercial, residential or mixed use property investment lending</li>
<li>Property Development</li>
</ul>
<p><strong>Property investment loans</strong></p>
<p>Buy-to-let funding is generally available for up to 70% of the value of the property, on a repayment or short-term interest-only basis, or with partial capital repayments. For commercial properties, investment funding is typically available for up to 55% LTV on terms up to 10 years. Interest rates are offered depending on the quality of the covenant interest cover, LTV and duration of the facility. We can arrange loans for your clients to buy and develop properties. These loans are repayable as a lump sum after the fully developed property has been sold or refinanced. In some circumstances, we may be able to capitalise interest. The typical loan will usually not exceed two years. Other terms including pricing and LTV are dependent upon the size and nature of the development. If your client would like to consider the purchase of commercial property via SIPP and SSAS pension schemes that can offer significant tax benefits, we have the expertise to set up these structured loans. </p>
<p><strong>Property development loans</strong></p>
<p>We can arrange loans for your clients to buy and develop properties. These loans are repayable as a lump sum after the fully developed property has been sold or refinanced. In some circumstances, we may be able to capitalise interest. The typical loan will usually not exceed two years. Other terms including pricing and LTV are dependent upon the size and nature of the development.</p>
<p><strong>SIPP and SSAS property lending </strong></p>
<p>If your client would like to consider the purchase of commercial property via SIPP and SSAS pension schemes that can offer significant tax benefits, we have the expertise to set up these structured loans. <br /> <br /> For more information on these products <a data-udi="umb://document/42579a834d784865a1dd17096c636d29" href="/{localLink:umb://document/42579a834d784865a1dd17096c636d29}" title="Meet The Team">get in touch</a> or call us today on 0345 850 5555.</p>]]></topContent>
  <umbracoInternalRedirectId></umbracoInternalRedirectId>
  <umbracoNaviHide>0</umbracoNaviHide>
  <umbracoRedirect></umbracoRedirect>
  <umbracoUrlAlias></umbracoUrlAlias>
</sectionSubPage>