﻿<?xml version="1.0" encoding="utf-8"?>
<product guid="447d714d-cd65-4946-b948-ec6fe9d3fe48" id="1360" nodeName="International Instant Access Account" isDoc="" updated="2018-10-30T14:54:41.9230000Z" parentGUID="4a6b890c-73b7-4923-8efd-fe5c2a78b497" nodeTypeAlias="product" templateAlias="Product" sortOrder="3" published="false" isBlueprint="false">
  <additionalText></additionalText>
  <aERGrossToken></aERGrossToken>
  <aERToken></aERToken>
  <backToLink></backToLink>
  <bottomContent><![CDATA[<p class="smallprnt">Variable rates can be increased or decreased. For material interest rate changes that are not to your advantage, we will notify you two months before the change takes effect.</p>
<p class="smallprnt">AER stands for Annual Equivalent Rate and illustrates what the interest rate would be if interest was paid and compounded once each year. Gross interest is the contractual rate of interest. Interest will be paid without the deduction of tax. Individuals may have tax to pay on any interest received that exceeds their Personal Savings Allowance. Further information on the<span> </span><a data-udi="umb://document/bf53fba191ff4b019ea9d43f678cee1f" href="/{localLink:umb://document/bf53fba191ff4b019ea9d43f678cee1f}" title="Personal Savings Allowance">Personal Savings Allowance</a><span> </span>can be found at<span> </span><a rel="noopener" href="http://www.gov.uk/" target="_blank">www.gov.uk.</a></p>]]></bottomContent>
  <displayTagLine></displayTagLine>
  <doNotUseParentPageHeader>0</doNotUseParentPageHeader>
  <hideFSCSBox>0</hideFSCSBox>
  <icons></icons>
  <iconsBackgroundColour></iconsBackgroundColour>
  <metaDescription></metaDescription>
  <metaKeywords></metaKeywords>
  <pageHeader></pageHeader>
  <pageTitle></pageTitle>
  <promoBanner></promoBanner>
  <sideBarModules></sideBarModules>
  <tabs><![CDATA[[
  {
    "key": "2d36b7e2-7940-4c34-abd0-fabb769a37c8",
    "name": "Features and Benefits",
    "ncContentTypeAlias": "freetextTab",
    "title": "Features and Benefits",
    "content": "<p>To open an Instant Access Account as a personal customer, you must be 18 or over, either a Cyprus resident or an existing customer.</p>\n<ul>\n<li>Unlimited withdrawals with easy access to your money.</li>\n<li>There is no minimum initial deposit required and you can deposit up to £10 million (Euro or US Dollar equivalent).</li>\n<li>Available in sterling, euro and US dollar. No interest is paid on euro and US dollar Instant Access Accounts.</li>\n<li>Eligible deposits are protected up to £85,000 by the UK<span> </span><a data-udi=\"umb://document/effd9d96403f4dad9820175bbdbe1473\" href=\"/{localLink:umb://document/effd9d96403f4dad9820175bbdbe1473}\" title=\"Financial Services Compensation Scheme\">Financial Services Compensation Scheme</a>.</li>\n</ul>"
  },
  {
    "key": "5df0f275-55f3-4804-82e8-a491aee211f0",
    "name": "Summary Box",
    "ncContentTypeAlias": "freetextTab",
    "title": "Summary Box",
    "content": "<h3>What is the interest rate?</h3>\n<div class=\"table-responsive\">\n<table border=\"0\" class=\"table-striped\" style=\"width: 645px; height: 156px;\"><caption class=\"sr-only\">Interest rates</caption>\n<thead>\n<tr style=\"height: 10px;\">\n<th class=\"normal\" style=\"width: 162px; height: 10px; text-align: left;\">Currency</th>\n<th class=\"normal\" style=\"width: 125px; height: 10px; text-align: left;\">Gross<span class=\"small\">Variable</span></th>\n<th class=\"normal\" style=\"width: 121px; height: 10px; text-align: left;\">GrossAER</th>\n<th style=\"width: 25px; height: 10px; text-align: left;\">Net</th>\n</tr>\n</thead>\n<tbody>\n<tr style=\"height: 31px;\">\n<td style=\"width: 162px; height: 31px;\">Pound Sterling</td>\n<td style=\"width: 125px; height: 31px;\">[!NORADGBPIA,GROSSRATE]%</td>\n<td style=\"width: 121px; height: 31px;\">[!NORADGBPIA,AER]%  </td>\n<td style=\"width: 25px; height: 31px;\">[!NORADGBPIA,NETRATE]%     </td>\n</tr>\n<tr style=\"height: 21px;\">\n<td style=\"width: 162px; height: 21px;\">Euro</td>\n<td style=\"width: 125px; height: 21px;\">0.00%</td>\n<td style=\"width: 121px; height: 21px;\">0.00%</td>\n<td style=\"width: 25px; height: 21px;\">0.00%</td>\n</tr>\n<tr style=\"height: 20px;\">\n<td style=\"width: 162px; height: 20px;\">US Dollar</td>\n<td style=\"width: 125px; height: 20px;\">0.00%</td>\n<td style=\"width: 121px; height: 20px;\">0.00%</td>\n<td style=\"width: 25px; height: 20px;\">0.00%</td>\n</tr>\n</tbody>\n</table>\n</div>\n<div class=\"table-responsive\">\n<h3><br />Interest</h3>\n</div>\n<ul>\n<li>Interest is calculated daily on the cleared balance in your account.</li>\n<li>Interest is paid half-yearly in June and December.</li>\n<li>Interest is paid added to the account unless you instruct us otherwise.</li>\n</ul>\n<h3>Can Cynergy Bank change the interest rate?</h3>\n<p>The interest rate is variable and can change.</p>\n<p>Details of the circumstances under which the interest rate might change are in the Changes section of the Instant Access Account Conditions.</p>\n<p>If we change the interest rate to your advantage we may make the change immediately and will notify you within 30 days of it taking effect. Where we make a change to the interest rate that is not to your advantage, we will notify you 60 days before the change takes effect.</p>\n<h3>What would the estimated balance be after 12 months, based on a £1,000, €1,000 or $1,000 deposit?</h3>\n<div class=\"table-responsive\">\n<table border=\"0\" class=\"table-striped\"><caption class=\"sr-only\">Estimated Balance</caption>\n<thead>\n<tr>\n<th class=\"normal\">Currency</th>\n<th class=\"normal\">Gross</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>Pound Sterling</td>\n<td>£1,001.00</td>\n</tr>\n<tr>\n<td>Euro</td>\n<td>€1,000.00</td>\n</tr>\n<tr>\n<td>US Dollar</td>\n<td>$1,000.00</td>\n</tr>\n</tbody>\n</table>\n</div>\n<p><strong>The estimated balances above include interest paid. These estimated balances are provided for illustrative purposes only and do not take into account a customer’s individual circumstances.</strong></p>\n<h3>How do I open and manage my account?</h3>\n<ul>\n<li>To open an Instant Access Account as a personal customer, you must be 18 or over, either a UK or Cyprus resident and an existing Cynergy Bank customer.</li>\n<li>If you are an existing personal customer, you can open an account by contacting us.</li>\n<li>You can use the following channels to manage your account: online, post, phone and branch.</li>\n<li>The minimum initial deposit is £1 and you can deposit up to £10 million (Euro or US Dollar equivalent).</li>\n</ul>\n<h3>Can I withdraw money?</h3>\n<p>Yes. Unlimited withdrawals are permitted from the available balance.</p>\n<p>We may charge you for making Faster Payments, CHAPS and SWIFT payments. Cash withdrawal fees apply for personal customers.</p>\n<p>Details of the payment charges will be the same as the equivalent charges set out in our Tariff of Bank Charges for Personal Customers.</p>\n<h3>Additional Information</h3>\n<ul>\n<li>Interest will be paid without the deduction of tax.</li>\n<li>The tax treatment and the rate of interest payable depend on individual circumstances and may be subject to change in the future.</li>\n<li>AER stands for Annual Equivalent Rate and illustrates what the interest rate would be if interest was paid and compounded once each year. Gross interest is the contractual rate of interest.</li>\n</ul>\n<p><strong>The contents of this Summary Box are intended as a summary of the main features only and should be read alongside the relevant conditions.</strong></p>"
  },
  {
    "key": "b643e8b5-aaa5-4e94-af39-21ec32b5abb4",
    "name": "Documents",
    "ncContentTypeAlias": "documentsTab",
    "title": "Documents",
    "documents": "umb://document/66c3ac0caac54935bd6e2cccd8631771",
    "text": ""
  },
  {
    "key": "4f9e3f5e-3b8d-40e0-9145-143a594ba438",
    "name": "Rates",
    "ncContentTypeAlias": "ratesTab",
    "title": "Rates",
    "ratesTable": "umb://document/d870c68a64f749ddbfd8f45060432c2f"
  },
  {
    "key": "6e985531-2ee5-4b60-afb5-2e7c964aba6c",
    "name": "How to Apply",
    "ncContentTypeAlias": "freetextTab",
    "title": "How to Apply",
    "content": "<p>Please read the<span> </span><a rel=\"noopener\" data-udi=\"umb://document/66c3ac0caac54935bd6e2cccd8631771\" href=\"/{localLink:umb://document/66c3ac0caac54935bd6e2cccd8631771}\" target=\"_blank\" title=\"Instant Access Account Conditions\">Instant Access Account Conditions</a><span> </span>before you apply to ensure you understand the features and conditions of what you are buying.</p>\n<p>If you are an existing Cynergy Bank customer and would like to open a new Instant Access account, please<span> </span><a data-udi=\"umb://document/3ae2ee8f861a44cdabd097dec60009f0\" href=\"/{localLink:umb://document/3ae2ee8f861a44cdabd097dec60009f0}\" title=\"Contact\">contact us</a>.</p>"
  }
]]]></tabs>
  <tagLine></tagLine>
  <topContent><![CDATA[<h2>International Instant Access Account</h2>
<p>Our sterling International Instant Access Account is an interest paying account available to Cyprus residents or existing customers.</p>
<p><strong>You also have the security of knowing your eligible deposits are protected up to a total of £85,000 by the<span> </span></strong><a data-udi="umb://document/effd9d96403f4dad9820175bbdbe1473" href="/{localLink:umb://document/effd9d96403f4dad9820175bbdbe1473}" title="Financial Services Compensation Scheme"><strong>Financial Services Compensation Scheme</strong></a><strong>, the UK's deposit guarantee scheme.</strong></p>]]></topContent>
  <umbracoInternalRedirectId></umbracoInternalRedirectId>
  <umbracoNaviHide>0</umbracoNaviHide>
  <umbracoRedirect></umbracoRedirect>
  <umbracoUrlAlias></umbracoUrlAlias>
</product>