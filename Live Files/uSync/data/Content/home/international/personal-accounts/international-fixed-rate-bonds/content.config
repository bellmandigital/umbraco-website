﻿<?xml version="1.0" encoding="utf-8"?>
<product guid="c7b1d16e-0bd2-4ce5-9ae7-1887bb531ef1" id="1357" nodeName="International Fixed Rate Bonds" isDoc="" updated="2019-10-01T11:20:00.7300000Z" parentGUID="4a6b890c-73b7-4923-8efd-fe5c2a78b497" nodeTypeAlias="product" templateAlias="Product" sortOrder="0" published="false" isBlueprint="false">
  <additionalText></additionalText>
  <aERGrossToken></aERGrossToken>
  <aERToken></aERToken>
  <backToLink></backToLink>
  <bottomContent><![CDATA[<p class="smallprnt">Interest rates are subject to change.</p>
<p class="smallprnt"> </p>]]></bottomContent>
  <displayTagLine>0</displayTagLine>
  <doNotUseParentPageHeader>0</doNotUseParentPageHeader>
  <hideFSCSBox>0</hideFSCSBox>
  <icons></icons>
  <iconsBackgroundColour></iconsBackgroundColour>
  <metaDescription></metaDescription>
  <metaKeywords><![CDATA[CynergyBank, Cynergy Bank, Cynergy, Empowering business owners, Empowering savers, International personal bank account, international current account, overseas bank account, International Bank account, International fixed rate bonds, international savings, international savings accounts, internation 35 day notice account, International Notice Account]]></metaKeywords>
  <pageHeader></pageHeader>
  <pageTitle><![CDATA[International Fixed Rate Bonds]]></pageTitle>
  <promoBanner></promoBanner>
  <sideBarModules></sideBarModules>
  <tabs><![CDATA[[
  {
    "key": "7973da36-bb28-462d-8412-bde5fc6e7128",
    "name": "Features and Benefits",
    "ncContentTypeAlias": "freetextTab",
    "title": "Features and Benefits",
    "content": "<ul>\n<li>Competitive interest rate, including loyalty rates and terms for existing customers.</li>\n<li>Fixed rate of interest guaranteed for the term of your bond.</li>\n<li>Interest is to be paid annually.</li>\n<li>Minimum deposit is £10,000.</li>\n<li>Maximum deposit is £1,000,000.</li>\n<li>If you have been a Cynergy Bank customer for six months or more, you will automatically receive our loyalty rates when you deposit or reinvest in an eligible product.</li>\n<li>Eligible deposits are protected up to £85,000 by the UK<span> </span><a data-udi=\"umb://document/effd9d96403f4dad9820175bbdbe1473\" href=\"/{localLink:umb://document/effd9d96403f4dad9820175bbdbe1473}\" title=\"Financial Services Compensation Scheme\">Financial Services Compensation Scheme</a>.</li>\n</ul>"
  },
  {
    "key": "4880da79-ebf4-4591-9855-f8307c1d3d48",
    "name": "Summary Box",
    "ncContentTypeAlias": "freetextTab",
    "title": "Summary Box",
    "content": "<h3>What is the interest rate?</h3>\n<div class=\"table-responsive\">\n<table border=\"0\" class=\"table-striped\"><caption class=\"sr-only\">Fees</caption>\n<thead>\n<tr>\n<td> </td>\n<th class=\"normal\" colspan=\"2\">Advertised rates<span class=\"small\">Fixed</span></th>\n<th class=\"normal\" colspan=\"2\">Loyalty rates<span class=\"small\">Fixed</span></th>\n</tr>\n<tr>\n<th class=\"normal\">Term</th>\n<th class=\"normal sub-heading\">Gross</th>\n<th class=\"normal sub-heading\">AER</th>\n<th class=\"normal sub-heading\">Gross</th>\n<th class=\"normal sub-heading\">AER</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>1 year</td>\n<td>[!NORADGBPJJ3,NEWRATE]%</td>\n<td>[!NORADGBPJJ3,NEWAER]%</td>\n<td>[!NORLOGBPJJ3,LOYALTYRATE]%</td>\n<td>[!NORLOGBPJJ3,LOYALTYAER]%</td>\n</tr>\n<tr>\n<td>2 years</td>\n<td>[!NORADGBPJJ6,NEWRATE]%</td>\n<td>[!NORADGBPJJ6,NEWAER]%</td>\n<td>[!NORLOGBPJJ6,LOYALTYRATE]%</td>\n<td>[!NORLOGBPJJ6,LOYALTYAER]%</td>\n</tr>\n<tr>\n<td>3 years</td>\n<td>[!NORADGBPJJ7,NEWRATE]%</td>\n<td>[!NORADGBPJJ7,NEWAER]%</td>\n<td>[!NORLOGBPJJ7,LOYALTYRATE]%</td>\n<td>[!NORLOGBPJJ7,LOYALTYAER]%</td>\n</tr>\n</tbody>\n</table>\n<br />If you have been a Cynergy Bank customer for six months or more, you automatically receive our loyalty rate when you deposit or reinvest in an eligible product.</div>\n<ul>\n<li>Interest is calculated daily on the cleared balance in your account.</li>\n<li>Interest is paid annually on the account opening anniversary (or the next working day).</li>\n<li>Except at renewal, interest cannot be paid back into this Fixed Rate Bond and for 2 and 3 year terms initial years interest must be paid away.</li>\n</ul>\n<h3>Can Cynergy Bank change the interest rate?</h3>\n<ul>\n<li>No. The interest rate is fixed for the term of the Fixed Rate Bond.</li>\n</ul>\n<h3>What would the estimated balance be on maturity, based on a £10,000 deposit?</h3>\n<div class=\"table-responsive\">\n<table border=\"0\" class=\"table-striped\"><caption class=\"sr-only\">Fees</caption>\n<thead>\n<tr>\n<th class=\"normal\">Term</th>\n<th class=\"normal\">Advertised rates</th>\n<th class=\"normal\">Loyalty rates</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>1 year</td>\n<td>n/a</td>\n<td>£10,085</td>\n</tr>\n<tr>\n<td>2 years</td>\n<td>n/a</td>\n<td>£10,190</td>\n</tr>\n<tr>\n<td>3 years</td>\n<td>n/a</td>\n<td>£10,315</td>\n</tr>\n</tbody>\n</table>\n<br />The estimated balances are based on interest being paid away each year and assume no further deposits or withdrawals are made. They are provided for illustrative purposes only and do not take into account a customer’s individual circumstances.</div>\n<h3>How do I open and manage my account?</h3>\n<ul>\n<li>Personal customers who are 18 or over and either a UK or Cyprus resident can open a Bond online.</li>\n<li>The minimum initial deposit is £10,000.</li>\n<li>The maximum deposit is £1 million (or higher at our discretion, please <a data-udi=\"umb://document/3ae2ee8f861a44cdabd097dec60009f0\" href=\"/{localLink:umb://document/3ae2ee8f861a44cdabd097dec60009f0}\" title=\"Contact\">contact us</a>).</li>\n<li>If you change your mind, you have 14 days from the date your Fixed Rate Bond is opened in which to cancel it. Within these 14 days you do not need to provide any reason for cancellation, just give notice by phone, online banking secure message or post and we will return the full balance plus any accrued interest to you. Cancellation requests cannot be accepted by email.</li>\n<li>You can manage your account in the following ways: online, post, phone and branch.</li>\n</ul>\n<h3>Can I withdraw money?</h3>\n<ul>\n<li>Following the 14 day cancellation period, other than in ‘exceptional circumstances’ you cannot cancel or make a withdrawal from the Fixed Rate Bond before the end of the term. Please see our <a rel=\"noopener\" data-udi=\"umb://media/503d77c9f8a648e1abaf97ed99eabc1c\" href=\"/media/2247/general-conditions-for-cynergy-bank-fixed-rate-bonds-february-2019-personal-bond-https-enabled-ec-280619.pdf\" target=\"_blank\" title=\"Bond Conditions Pack Personal 20 June 19 Final.pdf\">General Fixed Rate Bond Conditions</a> for more details.</li>\n<li>\n<p>Unless you tell us otherwise, Fixed Rate Bonds are automatically renewed at the end of the fixed term. We will re-invest the principal amount and interest (except where the interest is paid-away) into our nearest equivalent Fixed Rate Bond. We will write to you around two weeks before your renewal date to remind you of your options.</p>\n</li>\n</ul>\n<h3>Additional Information</h3>\n<ul>\n<li>You cannot transfer or assign the Fixed Rate Bond to anyone else.</li>\n<li>Interest will be paid without the deduction of tax. Individuals may have tax to pay on any interest received that exceeds their Personal Savings Allowance. Further information on the <a data-udi=\"umb://document/bf53fba191ff4b019ea9d43f678cee1f\" href=\"/{localLink:umb://document/bf53fba191ff4b019ea9d43f678cee1f}\" title=\"Personal Savings Allowance\">Personal Savings Allowance</a> can be found at <a rel=\"noopener\" href=\"http://www.gov.uk\" target=\"_blank\" title=\"www.gov.uk\">www.gov.uk</a>.</li>\n<li>The tax treatment and the rate of interest payable depend on individual circumstances and may be subject to change in the future.</li>\n<li>AER stands for Annual Equivalent Rate and illustrates what the interest rate would be if interest was paid and compounded once each year. Gross interest is the contractual rate of interest before the deduction of income tax.</li>\n</ul>\n<p><strong>The contents of this Summary Box are intended as a summary of the main features only and should be read alongside the relevant conditions.</strong></p>"
  },
  {
    "key": "b496f200-de91-48ab-b873-2f84edcd87cf",
    "name": "Documents",
    "ncContentTypeAlias": "documentsTab",
    "title": "Documents",
    "documents": "umb://document/f3553e9ff8de4cc5b3d51aa172587f70",
    "text": ""
  },
  {
    "key": "a10ffb20-f66e-430a-a930-f880cf5f0d8c",
    "name": "Rates",
    "ncContentTypeAlias": "ratesTab",
    "title": "Rates",
    "ratesTable": "umb://document/1dcdd05e2a2f40b9940fa83ca33e6aee"
  }
]]]></tabs>
  <tagLine></tagLine>
  <topContent><![CDATA[<h2>International Fixed Rate Bonds</h2>
<p><strong>This account is temporarily unavailable to new applicants.</strong></p>
<p>If you can put away your money without access for a set period of time, our International Fixed Rate Bonds may be suitable for you.</p>
<p>You also have the assurance that you are protected up to a total of £85,000 by the <a data-udi="umb://document/effd9d96403f4dad9820175bbdbe1473" href="/%7BlocalLink:umb://document/effd9d96403f4dad9820175bbdbe1473%7D" title="Financial Services Compensation Scheme">Financial Services Compensation Scheme</a>, the UK's deposit guarantee scheme</p>]]></topContent>
  <umbracoInternalRedirectId></umbracoInternalRedirectId>
  <umbracoNaviHide>0</umbracoNaviHide>
  <umbracoRedirect></umbracoRedirect>
  <umbracoUrlAlias></umbracoUrlAlias>
</product>