﻿<?xml version="1.0" encoding="utf-8"?>
<sectionSubPage guid="7583a399-c140-4f22-b00d-1f9c3a4c7025" id="1365" nodeName="Identification requirements to open an International Account" isDoc="" updated="2019-10-01T11:18:05.8230000Z" parentGUID="adf5c8ae-5fc9-4e33-82bd-bb77e3646540" nodeTypeAlias="sectionSubPage" templateAlias="SectionSubPage" sortOrder="1" published="false" isBlueprint="false">
  <backToLink></backToLink>
  <bottomContent></bottomContent>
  <displayTagLine>0</displayTagLine>
  <doNotUseParentPageHeader>0</doNotUseParentPageHeader>
  <hideFSCSBox>0</hideFSCSBox>
  <icons></icons>
  <iconsBackgroundColour></iconsBackgroundColour>
  <metaDescription></metaDescription>
  <metaKeywords></metaKeywords>
  <pageHeader></pageHeader>
  <pageTitle></pageTitle>
  <promoBanner></promoBanner>
  <sideBarModules></sideBarModules>
  <tabs></tabs>
  <tagLine></tagLine>
  <topContent><![CDATA[<h2>Identification requirements to open an international account</h2>
<p>To protect you, and as part of fulfilling our legal account opening obligations, we are required to identify all account holders before we can open your account.</p>
<p>In addition, the UK regulatory environment also requires us to conduct ongoing monitoring of our relationships with our customers. In order to properly meet that obligation we may, from time to time, contact you to ensure that the documents, data or information we hold about you remains up to date. For example, we may need to update your proof of identity evidence. We may also sometimes need to make enquiries of you to fully understand the usage of your account generally or the nature of a specific transaction.</p>
<h3>Identification documentation for Non-UK companies</h3>
<p>In addition to the individual identity and documentation requirements below, organisations residing outside the UK must provide us with an original or certified copy of all of the following:</p>
<ul>
<li>The Certificate of Incorporation and Memorandum and Articles of Association (or equivalent)</li>
<li>The resolution appointing directors/secretary</li>
<li>Verification of beneficial ownership by either the company’s accountants or solicitors, or share certificates</li>
</ul>
<h3>Non-UK residents</h3>
<p>Applicants residing outside the UK must send, with their new account application</p>
<ul>
<li>A certified copy of a photo identification document</li>
<li>A certified copy or an original proof of permanent residential address</li>
</ul>
<p>Any document you send must be valid, not expired and include the full name and address details that match your application. The same document cannot be used to verify both your identity and address</p>
<div class="table-responsive">
<table border="0" class="table-striped"><caption class="sr-only"></caption>
<thead>
<tr>
<td> </td>
<th class="normal">We can accept the following documents</th>
</tr>
</thead>
<tbody>
<tr>
<td>Photo identification documents</td>
<td>
<ul>
<li>a valid passport</li>
<li>a valid photo card driving licence issued by an EU state</li>
<li>a national identity card (for the European Economic Area or Swiss Nationals only)</li>
</ul>
</td>
</tr>
<tr>
<td>Proof of residential address</td>
<td>
<ul>
<li>a utility bill dated within the last six months</li>
<li>a UK or EU bank statement dated within the last six months</li>
</ul>
<p><strong>PO Box addresses, mobile phone bills and printed online statements are not acceptable address identification documents. </strong></p>
</td>
</tr>
</tbody>
</table>
</div>
<p>For applicants in the EU, the acceptable documents listed in the table above must be certified as true copies of the original(s) by either:</p>
<ul>
<li>A UK or EU bank</li>
<li>UK or EU accountant</li>
<li>UK or EU solicitor</li>
<li>A UK Post Office</li>
</ul>
<p>Certification provided by Bank of Cyprus in Cyprus is also acceptable for Cyprus residents. Bank of Cyprus certifiers must stamp, sign and date any copies and confirm it is a true copy of the original. </p>
<p>For applicants outside the EU we will accept documents certified by an equivalent professional. The professional certifying the documents must provide written confirmation that it is a true copy of the original. The individual certifying the documents must be clearly identifiable and thus all relevant official stamps must be used:</p>
<ul>
<li>Printed name of the individual certifying the document(s)</li>
<li>Professional registration reference (if applicable) eg Law Society membership number</li>
<li>Company name and stamp</li>
<li>Address</li>
<li>Email address</li>
<li>Business telephone number</li>
<li>Date</li>
</ul>]]></topContent>
  <umbracoInternalRedirectId></umbracoInternalRedirectId>
  <umbracoNaviHide>0</umbracoNaviHide>
  <umbracoRedirect></umbracoRedirect>
  <umbracoUrlAlias></umbracoUrlAlias>
</sectionSubPage>