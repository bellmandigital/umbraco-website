﻿<?xml version="1.0" encoding="utf-8"?>
<product guid="4b807e1d-12b8-46c2-89db-ee945c4c5c6a" id="1323" nodeName="Professional Client Account" isDoc="" updated="2019-06-25T12:30:04.6530000Z" parentGUID="d2b14ece-5a3f-425c-9199-7817cb9777fb" nodeTypeAlias="product" templateAlias="Product" sortOrder="7" published="false" isBlueprint="false">
  <additionalText></additionalText>
  <aERGrossToken></aERGrossToken>
  <aERToken></aERToken>
  <backToLink></backToLink>
  <bottomContent><![CDATA[<p class="smallprnt">Variable rates can be increased or decreased. For material interest rate changes that are not to your advantage, we will notify you two months before the change takes effect.</p>
<p class="smallprnt">This account pays an interest rate below the Bank of England Base Rate.</p>
<p class="smallprnt">AER stands for Annual Equivalent Rate and illustrates what the interest rate would be if interest was paid and compounded once each year. Gross interest is the contractual rate of interest. Interest will be paid without the deduction of tax. Individuals may have to pay tax on any interest received that exceeds their Personal Savings Allowance. Further information on the <a data-udi="umb://document/bf53fba191ff4b019ea9d43f678cee1f" href="/{localLink:umb://document/bf53fba191ff4b019ea9d43f678cee1f}" title="Personal Savings Allowance">Personal Savings Allowance </a>can be found at <a rel="noopener" href="http://www.gov.uk" target="_blank">www.gov.uk.</a></p>]]></bottomContent>
  <displayTagLine></displayTagLine>
  <doNotUseParentPageHeader>0</doNotUseParentPageHeader>
  <hideFSCSBox>0</hideFSCSBox>
  <icons></icons>
  <iconsBackgroundColour></iconsBackgroundColour>
  <metaDescription></metaDescription>
  <metaKeywords></metaKeywords>
  <pageHeader></pageHeader>
  <pageTitle></pageTitle>
  <promoBanner></promoBanner>
  <sideBarModules></sideBarModules>
  <tabs><![CDATA[[
  {
    "key": "b9671176-6187-4026-8598-34d14686793f",
    "name": "Features & Benefits",
    "ncContentTypeAlias": "freetextTab",
    "title": "Features & Benefits",
    "content": "<p>To open a Professional Client Account you must also have (or open) a <a data-udi=\"umb://document/99155287526d45b3925d89fc5a31ca83\" href=\"/{localLink:umb://document/99155287526d45b3925d89fc5a31ca83}\" title=\"Business Current Account\">Business Current Account</a> with us. If you close your business current account then we will also close your Professional Client Account and all charges for this account will be deducted from your nominated business current account before it is closed.</p>\n<ul>\n<li>Instant access to funds.</li>\n<li>Competitive interest rates.</li>\n<li>No minimum or maximum withdrawal limit, as long as you have sufficient cleared funds available.</li>\n<li>No cash withdrawals.</li>\n<li>Cheque book.</li>\n<li>Cheque payments and transfers to other Cynergy Bank accounts free of charge.</li>\n<li>Other transfers available at standard charges.</li>\n<li>Discounted rates for transfers performed via Online Banking.</li>\n<li>Online statements.</li>\n<li>Minimum account opening deposit of £1.</li>\n<li>Maximum deposit capped at £10 million.</li>\n<li>All interest will be paid to account balance unless instructed otherwise.</li>\n<li>Designated client accounts available.</li>\n<li>No standing order or direct debits.</li>\n<li>All charges will be applied to your nominated business account.</li>\n<li>Tax statements on designated client accounts.</li>\n<li>Most eligible deposits are protected up to £85,000 by the UK <a href=\"/about-us/financial-services-compensation-scheme/\" title=\"Financial Services Compensation Scheme\">Financial Services Compensation Scheme</a>.</li>\n</ul>"
  },
  {
    "key": "787597d9-f7d0-48dc-8c79-191a376e5c5a",
    "name": "Documents",
    "ncContentTypeAlias": "documentsTab",
    "title": "Documents",
    "documents": "umb://document/7d3ba50dc2564006a2eca4a618a1d03f,umb://document/c79afc91b8da4f62a6d925bfe33d98cf,umb://document/e496ee8a0e024bcfa6ab049062135c2f",
    "text": ""
  },
  {
    "key": "ec4e21bb-2d97-42b8-814a-5b20cd186dbf",
    "name": "Rates",
    "ncContentTypeAlias": "ratesTab",
    "title": "Rates",
    "ratesTable": "umb://document/a98c206c839b465f9a948a4c7efaa22b"
  },
  {
    "key": "3d0f5afb-c059-4a5d-ae1a-a908392d5640",
    "name": "How to Apply",
    "ncContentTypeAlias": "freetextTab",
    "title": "How to Apply",
    "content": "<p>To apply for a Cynergy Bank Professional Client Account, <strong>please contact Customer Service on 0345 850 5555</strong> from 9.00am to 5.00pm Monday to Friday. Calls may be recorded for monitoring and training.</p>"
  }
]]]></tabs>
  <tagLine></tagLine>
  <topContent><![CDATA[<h2>Professional Client Account</h2>
<p>Our Professional Client Account is ideal for solicitors, accountants, insurance companies and insurance brokers.</p>
<p>You also have the security of knowing your eligible deposits are protected up to a total of £85,000 by the <a href="/about-us/financial-services-compensation-scheme/" title="Financial Services Compensation Scheme">Financial Services Compensation Scheme</a>, the UK's deposit guarantee scheme.</p>
<p><strong>This product is available for UK businesses only.</strong></p>]]></topContent>
  <umbracoInternalRedirectId></umbracoInternalRedirectId>
  <umbracoNaviHide>0</umbracoNaviHide>
  <umbracoRedirect></umbracoRedirect>
  <umbracoUrlAlias></umbracoUrlAlias>
</product>