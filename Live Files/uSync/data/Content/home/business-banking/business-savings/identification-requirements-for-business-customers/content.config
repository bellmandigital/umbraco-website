﻿<?xml version="1.0" encoding="utf-8"?>
<sectionSubPage guid="830a9496-20ed-4225-ad66-739b934643f1" id="1538" nodeName="Identification requirements for business customers" isDoc="" updated="2019-06-25T12:30:04.7570000Z" parentGUID="d2b14ece-5a3f-425c-9199-7817cb9777fb" nodeTypeAlias="sectionSubPage" templateAlias="SectionSubPage" sortOrder="8" published="true" isBlueprint="false">
  <backToLink></backToLink>
  <bottomContent></bottomContent>
  <displayTagLine></displayTagLine>
  <doNotUseParentPageHeader>0</doNotUseParentPageHeader>
  <hideFSCSBox>0</hideFSCSBox>
  <icons></icons>
  <iconsBackgroundColour></iconsBackgroundColour>
  <metaDescription></metaDescription>
  <metaKeywords></metaKeywords>
  <pageHeader></pageHeader>
  <pageTitle></pageTitle>
  <promoBanner></promoBanner>
  <sideBarModules></sideBarModules>
  <tabs></tabs>
  <tagLine></tagLine>
  <topContent><![CDATA[<h2>Identification requirements for business customers</h2>
<p>To protect you, and as part of fulfilling our legal account opening obligations, we are required to identify all account holders before we can open your account.</p>
<h3>UK residents</h3>
<p>As a UK resident you are not required to send us identification documents with your new account application, as we will make electronic enquiries to verify the details for each individual who is part of the application. If we cannot verify your details electronically we may also require additional paper based identification documents, which are listed below under 'Non-UK residents', before opening the account.<span> </span><strong>We will contact you if this is required</strong>.</p>
<h3>Non-UK residents</h3>
<p>Applicants residing outside the UK must send, with their new account application</p>
<ul>
<li>A certified copy of a photo identification document</li>
<li>A certified copy or an original proof of permanent residential address</li>
</ul>
<p>Any document you send must be valid, not expired and include the full name and address details that match your application. The same document cannot be used to verify both your identity and address.</p>
<div class="table-responsive">
<table border="0" class="table-striped" style="height: 84px;"><caption class="sr-only"></caption>
<thead>
<tr style="height: 21px;">
<th class="normal" style="width: 252px; height: 10px; text-align: left;">
<p><strong>Document type        </strong></p>
</th>
<th class="normal" style="width: 576px; height: 10px;">
<p>We can accept the following documents</p>
</th>
</tr>
</thead>
<tbody>
<tr style="height: 21px;">
<td style="width: 252px; height: 21px; text-align: left;">Photo identification documents                                    </td>
<td style="width: 576px; height: 21px; text-align: left;">
<ul>
<li>a valid passport</li>
<li>a valid photo card driving licence issued by an EU state</li>
<li>a national identity card (for the European Economic Area or Swiss Nationals only)</li>
</ul>
</td>
</tr>
<tr style="height: 21px;">
<td style="width: 252px; height: 21px;">Proof of residential address</td>
<td style="width: 576px; height: 21px; text-align: left;">
<ul>
<li>a utility bill dated within the last six months</li>
<li>a UK or EU bank statement dated within the last six months</li>
</ul>
<p><strong>PO box addresses, mobile phone bills and printed online statements are not acceptable address identification documents</strong>.</p>
</td>
</tr>
</tbody>
</table>
</div>
<p>For applicants in the EU, the acceptable documents listed in the table above must be certified as true copies of the original(s) by either:</p>
<ul>
<li>A UK or EU bank</li>
<li>UK or EU accountant</li>
<li>UK or EU solicitor</li>
<li>A UK post office</li>
</ul>
<p>Certification provided by Bank of Cyprus in Cyprus is also acceptable for Cyprus residents. Bank of Cyprus certifiers must stamp, sign and date any copies and confirm it is a true copy of the original. </p>
<p>For applicants outside the EU we will accept documents certified by an equivalent professional. The professional certifying the documents must provide written confirmation that it is a true copy of the original. The individual certifying the documents must be clearly identifiable and thus all relevant official stamps must be used:</p>
<ul>
<li>Printed name of the individual certifying the document(s)</li>
<li>Professional registration reference (if applicable) eg Law Society membership number</li>
<li>Company name and stamp</li>
<li>Address</li>
<li>Email address</li>
<li>Business telephone number</li>
<li>Date</li>
</ul>
<h2>Identification documentation for UK organisations</h2>
<h3>UK Corporates</h3>
<p>UK public and private limited companies, UK companies limited by guarantee and limited liability partnerships (LLPs) must be registered with Companies House and we will make electronic enquiries to verify the details provided for the organisation. We may also require some, or all, of the documentation listed below under ‘non-UK company’ before opening the account and we will contact you if this is required. The UK residents and Non-UK residents<span> </span><em>identification requirements for individuals</em><span> </span>also apply to all individuals and authorised signatories named on the application.</p>
<h3>UK Partnerships</h3>
<p>In addition to the individual identity and documentation requirements, partnerships must provide a certified copy of:</p>
<ul>
<li>The partnership agreement</li>
</ul>
<h3>UK Societies, Clubs and Associations</h3>
<p>In addition to the individual identity and documentation requirements, Societies, Clubs and Associations must send certified copies of all of the following:</p>
<ul>
<li>The rules or constitution of the society/club/association</li>
<li>The resolution of the governing committee authorising the applicants to act on behalf of the society/club/association</li>
</ul>
<h3>UK Trusts</h3>
<p>In addition to the individual identity and documentation requirements, Trusts must send a certified copy of the:</p>
<ul>
<li>Trust Deed</li>
</ul>
<h3>Identification documentation for Non-UK companies</h3>
<p>In addition to the individual identity and documentation requirements, organisations residing outside the UK must provide us with an original or certified copy of all of the following:</p>
<ul>
<li>The certificate of Incorporation and Memorandum and Articles of Association (or equivalent)</li>
<li>The resolution appointing directors/secretary</li>
<li>Verification of beneficial ownership by either the company’s accountants or solicitors, or share certificates</li>
</ul>]]></topContent>
  <umbracoInternalRedirectId></umbracoInternalRedirectId>
  <umbracoNaviHide>0</umbracoNaviHide>
  <umbracoRedirect></umbracoRedirect>
  <umbracoUrlAlias></umbracoUrlAlias>
</sectionSubPage>