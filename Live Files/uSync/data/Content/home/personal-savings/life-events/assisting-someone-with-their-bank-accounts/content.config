﻿<?xml version="1.0" encoding="utf-8"?>
<sectionSubPage guid="dcd0b077-cee4-4d1e-8a97-52730a3563ca" id="5035" nodeName="Assisting someone with their bank accounts" isDoc="" updated="2020-01-31T14:40:07.9230000Z" parentGUID="32a5f854-521b-4eee-929f-870fcd2819e1" nodeTypeAlias="sectionSubPage" templateAlias="SectionSubPage" sortOrder="3" published="true" isBlueprint="false">
  <backToLink></backToLink>
  <bottomContent></bottomContent>
  <displayTagLine>0</displayTagLine>
  <doNotUseParentPageHeader>0</doNotUseParentPageHeader>
  <hideFSCSBox>0</hideFSCSBox>
  <icons></icons>
  <iconsBackgroundColour></iconsBackgroundColour>
  <metaDescription></metaDescription>
  <metaKeywords></metaKeywords>
  <pageHeader></pageHeader>
  <pageTitle><![CDATA[Assisting someone with their bank accounts]]></pageTitle>
  <promoBanner></promoBanner>
  <sideBarModules></sideBarModules>
  <tabs></tabs>
  <tagLine></tagLine>
  <topContent><![CDATA[<h3><strong>Assisting someone with their bank accounts</strong></h3>
<h2 style="line-height: normal;"><span style="font-size: 10.0pt; font-family: 'Verdana',sans-serif;">Introduction </span><span style="font-size: 1.0pt; font-family: 'Verdana',sans-serif;"></span></h2>
<p>Cynergy Bank understands that it is sometimes necessary for a customer’s account to be handled by another person. The ways this can be done are set out in this guidance.</p>
<p>If you are given the authority to handle another person’s account, you normally have the same power to manage the account as the account holder.  This is also dependent upon the terms and conditions of the account held.</p>
<p>It is important for the account holder to think about how any specific requirements or arrangements may affect the running of their account. For example, an account holder specifying that both of their attorneys  should authorise withdrawals from their account would work well for a simple savings account but would not work if transactions needed to be carried out by phone or online.</p>
<p>This guide provides information on allowing someone to deal with your account and how we can help. We want to make this process is as simple and straightforward as possible for you.</p>
<p>There are different options available depending on your circumstances:</p>
<ul>
<li><a data-udi="umb://document/b8ce2a474f6540b5b24bf6416e3e185b" href="/{localLink:umb://document/b8ce2a474f6540b5b24bf6416e3e185b}" title="Third Party Mandate" data-anchor="#">Third Party Mandate</a></li>
<li><a data-udi="umb://document/b2e42261d3f44d9ba211eaccb5da14a2" href="/{localLink:umb://document/b2e42261d3f44d9ba211eaccb5da14a2}" title="Power of Attorney" data-anchor="#">Power of Attorney (POA)</a></li>
<li><a data-udi="umb://document/d7df205d95bc40bd97d9e23510eb34cc" href="/{localLink:umb://document/d7df205d95bc40bd97d9e23510eb34cc}" title="Court of Protection Order">Court of Protection Order (COP)</a></li>
</ul>
<p><strong>What actions can be taken by me on behalf of the account holder?</strong></p>
<p>This depends on what type of authority you hold:</p>
<div class="table-responsive">
<table border="0" class="table-striped" style="width: 625px; height: 519px;"><caption class="sr-only">Interest rates</caption>
<thead>
<tr style="height: 18px;">
<th class="normal" style="width: 309px; height: 18px;"><strong>Actions to be done on behalf of the account holder</strong></th>
<th style="width: 271px; height: 18px;"><strong>Third Party Mandate</strong></th>
<th class="normal" style="width: 266px; height: 18px;"><strong>Ordinary Power of Attorney</strong></th>
<th style="width: 346px; height: 18px;"><strong>Lasting / Enduring Power of Attorney</strong></th>
<th class="normal" style="width: 383px; height: 18px;"><strong>Court of Protection Order (Deputyship)</strong></th>
</tr>
</thead>
<tbody>
<tr style="height: 38px;">
<td style="width: 309px; height: 38px;">Obtain information about account holder’s account(s) from bank</td>
<td style="width: 271px; height: 38px; text-align: center;"><span>✔</span></td>
<td style="width: 266px; height: 38px; text-align: center;"><span>✔</span></td>
<td style="width: 346px; height: 38px; text-align: center;"><span>✔</span></td>
<td style="width: 383px; height: 38px; text-align: center;"><span>✔</span></td>
</tr>
<tr style="height: 38px;">
<td style="width: 309px; height: 38px;">Open/close accounts in the account holder’s name</td>
<td style="width: 271px; height: 38px; text-align: center;">-</td>
<td style="width: 266px; height: 38px; text-align: center;">-</td>
<td style="width: 346px; height: 38px; text-align: center;"><span>✔</span></td>
<td style="width: 383px; height: 38px; text-align: center;"><span>✔</span></td>
</tr>
<tr style="height: 38px;">
<td style="width: 309px; height: 38px;">Issue cheques</td>
<td style="width: 271px; height: 38px; text-align: center;"><span>✔</span></td>
<td style="width: 266px; height: 38px; text-align: center;"><span>✔</span></td>
<td style="width: 346px; height: 38px; text-align: center;"><span>✔</span></td>
<td style="width: 383px; height: 38px; text-align: center;"><span>✔</span></td>
</tr>
<tr style="height: 10px;">
<td style="width: 309px; height: 10px;">Make payments (i.e. bills)</td>
<td style="width: 271px; height: 10px; text-align: center;"><span>✔</span></td>
<td style="width: 266px; height: 10px; text-align: center;"><span>✔</span></td>
<td style="width: 346px; height: 10px; text-align: center;"><span>✔</span></td>
<td style="width: 383px; height: 10px; text-align: center;"><span>✔</span></td>
</tr>
<tr style="height: 58px;">
<td style="width: 309px; height: 58px;">Withdraw cash</td>
<td style="width: 271px; height: 58px; text-align: center;"><span>✔</span></td>
<td style="width: 266px; height: 58px; text-align: center;"><span>✔</span></td>
<td style="width: 346px; height: 58px; text-align: center;"><span>✔</span></td>
<td style="width: 383px; height: 58px; text-align: center;"><span>✔</span></td>
</tr>
<tr style="height: 38px;">
<td style="width: 309px; height: 38px;">Deposit cash/ cheques</td>
<td style="width: 271px; height: 38px; text-align: center;"><span>✔</span></td>
<td style="width: 266px; height: 38px; text-align: center;"><span>✔</span></td>
<td style="width: 346px; height: 38px; text-align: center;"><span>✔</span></td>
<td style="width: 383px; height: 38px; text-align: center;"><span>✔</span></td>
</tr>
<tr style="height: 38px;">
<td style="width: 309px; height: 38px;">Retrieve items from safekeeping</td>
<td style="width: 271px; height: 38px; text-align: center;"><span>-</span></td>
<td style="width: 266px; height: 38px; text-align: center;"><span>-</span></td>
<td style="width: 346px; height: 38px; text-align: center;"><span>✔ </span></td>
<td style="width: 383px; height: 38px; text-align: center;"><span>✔ </span></td>
</tr>
<tr style="height: 18px;">
<td style="width: 309px; height: 18px;">Apply for ISAs</td>
<td style="width: 271px; height: 18px; text-align: center;"><span>-</span></td>
<td style="width: 266px; height: 18px; text-align: center;"><span>-</span></td>
<td style="width: 346px; height: 18px; text-align: center;"><span>✔ </span></td>
<td style="width: 383px; height: 18px; text-align: center;"><span>✔ </span></td>
</tr>
<tr style="height: 18px;">
<td style="width: 309px; height: 18px;">Access Internet banking</td>
<td style="width: 271px; height: 18px; text-align: center;"><span>✔ </span></td>
<td style="width: 266px; height: 18px; text-align: center;"><span>✔ </span></td>
<td style="width: 346px; height: 18px; text-align: center;"><span>✔ </span></td>
<td style="width: 383px; height: 18px; text-align: center;"><span>✔ </span></td>
</tr>
<tr style="height: 17px;">
<td style="width: 309px; height: 17px;">Access Telephone banking</td>
<td style="width: 271px; height: 17px; text-align: center;"><span>✔  </span></td>
<td style="width: 266px; height: 17px; text-align: center;"><span>✔  </span></td>
<td style="width: 346px; height: 17px; text-align: center;"><span>✔  </span></td>
<td style="width: 383px; height: 17px; text-align: center;"><span>✔  </span></td>
</tr>
<tr style="height: 18px;">
<td style="width: 309px; height: 18px;">Hold a debit card</td>
<td style="width: 271px; height: 18px; text-align: center;"><span>-</span></td>
<td style="width: 266px; height: 18px; text-align: center;"><span>-</span></td>
<td style="width: 346px; height: 18px; text-align: center;"><span>-</span></td>
<td style="width: 383px; height: 18px; text-align: center;"><span>✔  </span></td>
</tr>
<tr style="height: 38px;">
<td style="width: 309px; height: 38px;">Hold a cheque/pay-in book</td>
<td style="width: 271px; height: 38px; text-align: center;"><span>✔</span></td>
<td style="width: 266px; height: 38px; text-align: center;"><span>✔</span></td>
<td style="width: 346px; height: 38px; text-align: center;"><span>✔</span></td>
<td style="width: 383px; height: 38px; text-align: center;"><span>✔</span></td>
</tr>
<tr style="height: 78px;">
<td style="width: 309px; height: 78px;">Manage the account if account holder becomes mentally incapacitated</td>
<td style="width: 271px; height: 78px; text-align: center;"><span>-</span></td>
<td style="width: 266px; height: 78px; text-align: center;"><span>-</span></td>
<td style="width: 346px; height: 78px; text-align: center;"><span>✔</span></td>
<td style="width: 383px; height: 78px; text-align: center;"><span>✔</span></td>
</tr>
<tr style="height: 38px;">
<td style="width: 309px; height: 38px;">Sign a mortgage loan agreement</td>
<td style="width: 271px; height: 38px; text-align: center;"><span>-</span></td>
<td style="width: 266px; height: 38px; text-align: center;"><span>-</span></td>
<td style="width: 346px; height: 38px; text-align: center;"><span>✔</span></td>
<td style="width: 383px; height: 38px; text-align: center;"><span>✔</span></td>
</tr>
<tr style="height: 18px;">
<td style="width: 309px; height: 18px;">Arrange an overdraft</td>
<td style="width: 271px; height: 18px; text-align: center;"><span>-</span></td>
<td style="width: 266px; height: 18px; text-align: center;"><span>-</span></td>
<td style="width: 346px; height: 18px; text-align: center;"><span>-</span></td>
<td style="width: 383px; height: 18px; text-align: center;"><span>-</span></td>
</tr>
</tbody>
</table>
</div>
<p><strong>Customer Correspondence</strong></p>
<ul>
<li><strong>Online Easy Access accounts</strong>: All correspondence documents and emails in relation to the account, will be sent to the Attorney, not the Donor. This is applicable regardless of the type of power of attorney that is held.</li>
<li><strong>All other account types</strong>: All correspondence will be sent to the Donor unless the Donor/ Attorney instruct us otherwise.</li>
</ul>
<p> </p>
<p><strong>What does the Bank need from you?</strong></p>
<p>The Bank requires original or certified copies of the following documents:</p>
<ul>
<li>If you have granted a Lasting Power of Attorney, the Bank will want to see that it has been stamped by the Office of the Public Guardian and is therefore registered;</li>
<li>If the Donor has granted an Enduring Power of Attorney and no longer has mental capacity, the Bank will want to see that it has been stamped by the Court of Protection and is therefore registered.</li>
</ul>
<p>The Bank will need to verify the Donor and Attorney’s identity and address.  Initially the Bank will conduct electronic searches for UK residents.  In the event we are unable to verify the identity of UK residents via electronic searches, we will request the following certified documents:</p>
<p><strong>Photo Identification</strong> – this must be either:</p>
<ul>
<li>A valid passport</li>
<li>A valid photo card driving license issued by an EU state</li>
<li>A national identity card (for the European Economic Area or Swiss Nationals only)</li>
</ul>
<p><strong>Proof of Address</strong> – this must be either:</p>
<ul>
<li>A utility bill dated within the last six months</li>
<li>A UK or EU bank statement dated within the last six months</li>
</ul>
<p>Please note that PO Box addresses, mobile phone bills and printed online statements are not acceptable.</p>
<p>Non-UK residents will need to supply the above certified documents.</p>
<p>If the Donor is living in a care home we will require:</p>
<ul>
<li>An original letter from the care home confirming the Donor’s address dated within a month of the application</li>
<li>A letter from HM Revenue &amp; Customs or the Department for Work &amp; Pensions dated within six months.</li>
</ul>
<p>If original documents are provided to the Bank, we will take photocopies for our records and return the originals via Recorded Delivery to the address provided by the sender of the documents.</p>
<p><strong>What are the Certification and Verification standards?</strong><strong> </strong></p>
<ul>
<li>The Lasting Power of Attorney, Enduring Power of Attorney and Court of Protection Order can <strong><u>only</u> </strong>be certified by a solicitor.</li>
<li>The Attorney and Donor identification documents can be certified by:<br />EU Bank, EU Accountant, EU Solicitor or UK Post Office.<br /><br /></li>
<li>All certified documents must be stamped to indicate they are ‘true copies of the original’. The certification must also include:
<ul>
<li>the Certifier’s name;</li>
<li>the Certifier’s occupation;</li>
<li>the Certifier’s professional registration reference (if applicable) (e.g. the Solicitors Regulation Authority registration number);</li>
<li>the Certifier’s wet signature;</li>
<li>the company name, address and telephone number (if applicable), otherwise, their personal address and telephone number; and</li>
<li>the date of the certification.</li>
</ul>
</li>
</ul>
<p> </p>
<ul>
<li> The person certifying must not be:
<ul>
<li>related to you</li>
<li>living at the same address</li>
<li>in a relationship with you</li>
</ul>
</li>
</ul>
<p><strong><u>Submitting your documents </u></strong></p>
<ul>
<li>You can also post the documents directly to us if they have all been correctly certified. Please send to the following address:</li>
</ul>
<p>Cynergy Bank<br />PO Box 17484<br />87 Chase Side<br />London<br />N14 5WH</p>
<p><strong><u>Processing the Power of Attorney and Court of Protection Order</u></strong></p>
<ul>
<li>Once all the correct documentation have been received and all account holders, Attorneys and Deputies have been identified, an application is made by the Bank to the Office of Public Guardian (OPG) to confirm that the Power of Attorney and Court of Protection Order is still valid.<br /><br /></li>
<li>The OPG can take up to ten days to respond to our request. Upon receipt of the OPG confirmation we will proceed to give the Attorney access to the required accounts.<br /><br /></li>
<li>A letter confirming the third party access process has been completed will be sent via Recorded Delivery to the attorney together with any original documentation. <br /><br /></li>
<li>The process of setting up Power of Attorney / Deputy access can take up to 20 working days to complete.</li>
</ul>
<p><strong>Where can I find additional information?</strong></p>
<p>Useful Links:</p>
<p><a rel="noopener" href="http://www.nhs.uk/Conditions/social-care-and-support-guide/Pages/lasting-power-of-attorney.aspx" target="_blank" title="http://www.nhs.uk/Conditions/social-care-and-support-guide/Pages/lasting-power-of-attorney.aspx">http://www.nhs.uk/Conditions/social-care-and-support-guide/Pages/lasting-power-of-attorney.aspx</a></p>
<p><a rel="noopener" href="https://www.moneyadviceservice.org.uk/en/search?query=power+of+attorney" target="_blank" title="https://www.moneyadviceservice.org.uk/en/search?query=power+of+attorney" data-anchor="?query=power+of+attorney">https://www.moneyadviceservice.org.uk/en/search?query=power+of+attorney</a></p>
<p><a rel="noopener" href="https://www.ageuk.org.uk/information-advice/money-legal/legal-issues/power-of-attorney/" target="_blank" title="https://www.ageuk.org.uk/information-advice/money-legal/legal-issues/power-of-attorney/">https://www.ageuk.org.uk/information-advice/money-legal/legal-issues/power-of-attorney/</a></p>]]></topContent>
  <umbracoInternalRedirectId></umbracoInternalRedirectId>
  <umbracoNaviHide>0</umbracoNaviHide>
  <umbracoRedirect></umbracoRedirect>
  <umbracoUrlAlias></umbracoUrlAlias>
</sectionSubPage>