﻿<?xml version="1.0" encoding="utf-8"?>
<contentPage guid="12f8030f-3a97-4b0f-b98f-160e05c63e14" id="1912" nodeName="Privacy Policy" isDoc="" updated="2019-08-01T17:41:04.5830000Z" parentGUID="76d44e15-aab7-4334-9f3b-b203a5a045ac" nodeTypeAlias="contentPage" templateAlias="ContentPage" sortOrder="0" published="true" isBlueprint="false">
  <backToLink></backToLink>
  <bottomContent></bottomContent>
  <displayTagLine>1</displayTagLine>
  <doNotUseParentPageHeader>0</doNotUseParentPageHeader>
  <icons></icons>
  <iconsBackgroundColour></iconsBackgroundColour>
  <metaDescription></metaDescription>
  <metaKeywords></metaKeywords>
  <pageHeader></pageHeader>
  <pageTitle></pageTitle>
  <promoBanner></promoBanner>
  <tabs></tabs>
  <tabStyle></tabStyle>
  <tagLine></tagLine>
  <topContent><![CDATA[<h2>Privacy policy</h2>
<h3>Introduction</h3>
<p>This Privacy Policy explains how we use any personal data we collect about you or that you provide to us. Cynergy Bank Limited (or "we/us/our/Cynergy Bank") is authorised by the Prudential Regulation Authority ("PRA") and regulated by the Financial Conduct Authority ("FCA") and the PRA. Cynergy Bank is registered in England and Wales as company number 04728421. For the purposes of this privacy policy the controller of your personal data is Cynergy Bank. This means that we, either alone or jointly with others will determine who and how your personal data is processed. Personal Data means information which either by itself or when combined with other information that we hold or which is available to us, can be used to identify you.</p>
<p>For the purposes of this Privacy Policy, the following definitions apply</p>
<ul>
<li>“Cynergy Bank Group” means Cynergy Bank Limited and its group entities;</li>
<li>“Bank of Cyprus Group” means Bank of Cyprus Public Company Limited and its group entities.</li>
</ul>
<p>Our Data Protection Office can be contacted at <a href="mailto:dpo@cynergybank.co.uk">dpo@cynergybank.co.uk</a> if you have any queries about this privacy policy or if you wish to exercise any rights mentioned in it.</p>
<p><strong>Categories of Personal Data that we process.</strong></p>
<p><strong>If you apply for any product with us we generally always collect:</strong></p>
<ul>
<li>Your financial details such as your salary or other income;</li>
<li>Your title, name, home address and address history as well as contact details such as telephone and mobile numbers and email address;</li>
<li>National Insurance and Tax Identification number (TIN) used for identification purposes and for use with ISA's;</li>
<li>Date of birth;</li>
<li>Nationality and tax residence in order to comply with our legal and regulatory obligations;</li>
<li>Other details regarding proof of residency such as utility and passport information which we retain on our file;</li>
<li>Information regarding your employment status and your employers identity;</li>
<li>If you are opening an online account, your IP and MAC address as relevant;</li>
<li>Personal data about you which is obtained from third parties (such as credit reference agencies where you have applied for an overdraft or other credit facilities connected with your account, fraud prevention agencies and such publically available sources such as the electoral roll and in some cases records of debt judgements and bankruptcy information - for further details see the Credit Reference Agencies section;</li>
<li>Any other information you give to us or our agents.</li>
</ul>
<p><strong>Joint applicants</strong></p>
<p>You must show this privacy notice to the other applicant(s) and obtain confirmation from them that they know you share their information with us for the purpose described and where necessary obtain their consent for us to process their personal details.</p>
<p><strong>If you are a prospective customer, or a non-customer counterparty in a transaction of a customer (e.g. account or payment authorisation (by SWIFT or not) and over-the-counter transactions) or prospective security provider (e.g. a guarantor for a credit facility) or an authorised representative/agent or beneficial owner of a legal entity or of a natural person which/who is a prospective customer, the relevant personal data which we collect may include:</strong></p>
<p>Name, address, contact details (telephone, email), EU basic payment account identification, birth date, place of birth (city and country), marital status, employed/self-employed, if you hold/held a prominent public function (for PEPs), FATCA / CRS info and authentication data.</p>
<p><strong>Current Account applications</strong></p>
<p>When you apply for a Current Account we will require current income and expenses, employment position and history, property ownership and personal debts, number of dependent children, personal investments and investment income, other banking relationship details, tax residence and tax ID, credit reference agency data, residence or work permit in case of non-EU nationals.</p>
<p>For individuals who will be providing their personal guarantees, the Bank will request personal data disclosing their economic and financial background and credit reference agency data.</p>
<p><strong>Children's data</strong></p>
<p>We understand the importance of protecting children's privacy. We may collect personal data in relation to children only provided that we have first obtained their parents' or legal guardian's consent or unless otherwise permitted under law. We do not provide any online services to children but we may allow children, with their parents' or legal guardian's consent, to access accounts opened for their benefit on the Bank's online banking system in order to view their account balances. For the purposes of this privacy statement, "children" are individuals who are under the age of eighteen (18) years.</p>
<p><strong>Mortgages</strong></p>
<p>When you apply for a mortgage we will require current income and expenses, employment position and history, property ownership and personal debts, number of dependent children, personal investments and investment income, life insurances (life insurance companies, policy numbers, current surrender values), other banking relationship details, tax residence and tax ID, residence or work permit in case of non-EU nationals.</p>
<p>When a mortgage intermediary processes your personal data on our behalf, this Privacy Policy will apply and you should contact us to exercise your rights under data protection laws. When a mortgage intermediary processes your personal data as a data controller in its own right, its own privacy notice will apply and you should ask them for a copy if you do not have one by the time you are introduced to us.</p>
<p><strong>Credit Reference Agencies</strong></p>
<p>We carry out credit and identity checks when you apply for a product or services for you or your business. We may use Credit Reference Agencies to help us with this.</p>
<p>If you use our services, from time to time we may also search information that the Credit Reference Agencies have, to help us manage those accounts.</p>
<p>We will share your personal data with Credit Reference Agencies and they will give us information about you such as missed payments on other accounts you hold. The data we exchange can include:</p>
<ul>
<li>Name, address and date of birth</li>
<li>Credit application</li>
<li>Details of any shared credit</li>
<li>Financial situation and history</li>
<li>Public information, from sources such as the electoral register and Companies House.</li>
</ul>
<p>We'll use this data to:</p>
<ul>
<li>Assess whether you or your business is able to afford to make repayments</li>
<li>Make sure what you've told us is true and correct</li>
<li>Help detect and prevent financial crime</li>
<li>Manage accounts with us</li>
<li>Trace and recover debts</li>
<li>Verify your identity and your address</li>
<li>Make sure that we tell you about relevant offers; and</li>
<li>Ensure any offers provided to you are appropriate to your circumstances.</li>
</ul>
<p>If you apply for a product with someone else, we will link your records with theirs. We will do the same if you tell us you have a spouse, partner or civil partner – or that you are in business with other partners or directors.</p>
<p>When Credit Reference Agencies receive a search from us they will place a search footprint on your credit file that may be seen by other lenders.</p>
<p>You should tell them about this before you apply for a product or service. It is important that they know your records will be linked together, and that credit searches may be made on them.</p>
<p>We will go on sharing your personal data with Credit Reference Agencies for as long as you are a customer. This will include details about your settled accounts and any debts not fully repaid on time. It will also include details of funds going into the account, and the account balance. If you borrow, it will also include details of your repayments and whether you repay in full and on time. The Credit Reference Agencies may give this information to other organisations that want to check your credit status. We will also tell the Credit Reference Agencies when you settle your accounts with us.</p>
<p>Credit Reference Agencies will also link your records together. These links will stay on your files unless one of you asks the Credit Reference Agencies to break the link. You will normally need to give proof that you no longer have a financial link with each other.</p>
<p>You can find out more about the Credit Reference Agencies on their websites, in the Credit Reference Agency Information Notice (CRAIN) which sets out how your data will be processed by Callcredit, Equifax and Experian. Please go to www.equifax.co.uk/crain, www.callcredit.co.uk/crain or www.experian.co.uk/crain/index to read the notice in full. This includes details about:</p>
<ul>
<li>Who they are</li>
<li>Their role as fraud prevention agencies</li>
<li>The data they hold and how they use it</li>
<li>How they share personal data</li>
<li>How long they can keep data</li>
<li>Your data protection rights</li>
</ul>
<p>Here are links to the information notice for each of the three main Credit Reference Agencies:</p>
<p><u><a rel="noopener" href="https://www.equifax.com/international/uk/documents/Equifax_Personal_Data_Management_Policy.pdf" target="_blank">http://www.equifax.com/international/uk/documents/Equifax_Personal_Data_Management_Policy.pdf</a></u></p>
<p><u><a rel="noopener" href="https://www.experian.co.uk/consumer/privacy.html" target="_blank">https://www.experian.co.uk/consumer/privacy.html</a></u></p>
<p><u><a rel="noopener" href="https://www.callcredit.co.uk/legal-information/privacy-centre" target="_blank">https://www.callcredit.co.uk/legal-information/privacy-centre</a></u></p>
<p><strong>Fraud Prevention Agencies</strong></p>
<p>We may need to confirm your identity before we provide products or services to you or your business. Once you have become a customer of ours, we will also share your personal data as needed to help detect fraud and money-laundering risks. We use Fraud Prevention Agencies (FPA) such as CIFAS and National Hunter to help us with this, and if false or inaccurate information is provided and fraud identified, details will be passed to Fraud Prevention Agencies to prevent money laundering.</p>
<p>Both we and Fraud Prevention Agencies can only use your personal data if we have a proper reason to do so. It must be needed either for us to obey the law, or for a ‘legitimate interest'.</p>
<p>A legitimate interest is when we have a business or commercial reason to use your information. This must not unfairly go against what is right and best for you.</p>
<p>We will use the information to:</p>
<ul>
<li>Confirm identities</li>
<li>Help prevent fraud and money-laundering</li>
<li>Fulfil any contracts you or your business has with us</li>
</ul>
<p>We or a Fraud Prevention Agency may allow law enforcement agencies to access your personal data. This is to support their duty to detect, investigate, prevent and prosecute crime.</p>
<p>FPAs can keep personal data for different lengths of time. They can keep your data for up to six years if they find a risk of fraud or money-laundering.</p>
<p><strong>The information we use</strong></p>
<p>These are some of the kinds of personal information that we use:</p>
<ul>
<li>Name</li>
<li>Date of birth</li>
<li>Residential address</li>
<li>History of where you have lived</li>
<li>Contact details, such as email addresses and phone numbers</li>
<li>Financial data</li>
<li>Data relating to your or your business products or services</li>
<li>Employment details</li>
<li>Data that identifies computers or other devices you use to connect to the internet. This includes your Internet Protocol (IP) address</li>
</ul>
<p><strong>Why we process your personal data and on what legal basis</strong></p>
<p>We are committed to protecting your privacy and handling your data in an open and transparent manner and as such we process your personal data in accordance with the General Data Protection Regulation (GDPR) and the local data protection law for one or more of the following reasons:</p>
<p><strong>A. For the performance of a contract</strong></p>
<p>We process personal data in order to perform banking transactions and offer financial services based on contracts with our customers but also to be able to complete our acceptance procedure so as to enter into a contract with prospective customers.<br />The purpose of processing personal data depends on the requirements for each product or service and the contract terms and conditions provide more details of the relevant purposes.</p>
<p><strong>B. For compliance with a legal obligation</strong></p>
<p>There are a number of legal obligations emanating from multiple laws and regulations to which we are subject as well as statutory requirements, e.g. UK banking law, the Money Laundering Law, Tax laws, Law on Deposit Guarantee Protection and Resolution of Credit and Other Institutions Scheme, Payments Law and Payment Scheme Rules. There are also various supervisory authorities whose laws and regulations we are subject to e.g. Financial Conduct Authority (FCA) the Prudential Regulation Authority (PRA), Bank of England, Financial Ombudsman Service (FOS), the Financial Services Compensation Scheme (FSCS), the European Central Bank where we have a contractual requirement to do so following our separation from the Bank of Cyprus Group . Such obligations and requirements impose on us necessary personal data processing activities for credit checks, identity verification, compliance with court orders, tax law or other reporting obligations and anti-money laundering controls.</p>
<p><strong>C. For the purposes of safeguarding legitimate interests</strong></p>
<p>We process personal data so as to safeguard the legitimate interests pursued by us or by a third party. A legitimate interest is when we have a business or commercial reason to use your information. But even then, it must not unfairly go against what is right and best for you. Examples of such processing activities include:</p>
<ul>
<li>Initiating legal claims and preparing our defence in litigation procedures</li>
<li>Means and processes we undertake to provide for the Bank's IT and system security, preventing potential crime, asset security, admittance controls and anti-trespassing measures</li>
<li>Setting up CCTV systems, e.g. at ATMs, for the prevention of crime or fraud</li>
<li>To adhere to best practice, rules and requirements of bodies such as HMRC; the FCA; the PRA; the FOS; the Information Commissioner's Office; the FSCS</li>
<li>Measures to manage business and for further developing products and services,</li>
<li>Sharing your personal data to other companies in the Bank of Cyprus Group where we have a contractual requirement to do so following our separation from the Bank of Cyprus Group</li>
<li>Our own and risk management</li>
<li>The transfer, assignment (whether outright or as security for obligations) and/or sale to one or more persons (including the Bank of England) of and/or charge and/or encumbrance over, any or all of the Bank's benefits, rights, title or interest under any agreement between the customer and the Bank.</li>
</ul>
<p><strong>D. You have provided your consent</strong></p>
<p>Provided that you have given us your specific consent for processing (other than for the reasons set out hereinabove) then the lawfulness of such processing is based on that consent. You have the right to revoke consent at any time. However, any processing of personal data prior to the receipt of your revocation will not be affected.</p>
<p>Examples of when we process data with your consent are:</p>
<ul>
<li>When you request us to share your data with someone else</li>
<li>When you indicate you wish to receive direct marketing from us</li>
<li>For some special categories of Personal data such as data regarding your health or if you have special circumstances which may require us to tailor how we communicate with you; in such circumstances we will explain to you when we ask for your consent what purpose and how we will use your data.</li>
</ul>
<p><strong>E. Processing for a substantial public interest</strong></p>
<p>We may process data for a substantial public interest under laws which apply to us where this helps us to meet our broader social obligations such as processing information about your health or if you have a special need which may require us to tailor how we communicate with you or where we need to fulfil our legal or regulatory obligations.</p>
<p><strong>Who receives your personal data?</strong></p>
<p>In the course of the performance of our contractual and statutory obligations, your personal data may be provided to various departments within Cynergy Bank Limited but also to other companies of the Cynergy Bank Group; and the Bank of Cyprus Group where we have a contractual requirement to do so following our separation from the Bank of Cyprus Group. Various service providers and suppliers may also receive your personal data so that we may perform our obligations. Such service providers and suppliers enter into contractual agreements with the Bank by which they observe confidentiality and data protection according to the data protection law and GDPR.</p>
<p>We may disclose data about you for any of the reasons set out above, or if we are legally required to do so, or if we are authorised under our contractual, regulatory or statutory obligations or if you have given your consent. All data processors appointed by us to process personal data on our behalf are bound by contract to comply with the GDPR provisions. Under the circumstances referred to above, recipients of personal data may be, for example:</p>
<ul>
<li>Supervisory and other regulatory and public authorities where a statutory obligation exists. Some examples are the FCA, PRA, the European Central Bank where we have a contractual requirement to do so following our separation from the Bank of Cyprus Group, the income tax authorities, criminal prosecution authorities,</li>
<li>Credit and financial institutions such as correspondent banks</li>
<li>Share and stock investment and management companies</li>
<li>Valuators and surveyors</li>
<li>Non-performing loan management companies</li>
<li>External legal firms</li>
<li>Financial and business advisors</li>
<li>Auditors and accountants</li>
<li>Marketing companies (where you have provided consent) and market research companies</li>
<li>Companies which help us to provide you with debit, cards such as Visa and process those payments</li>
<li>Fraud prevention agencies</li>
<li>File storage companies, archiving and/or records management companies, cloud storage companies</li>
<li>Companies who assist us with the effective provision of our services to you by offering technological expertise, solutions and support and facilitating payments</li>
<li>Purchasing and procurement and website and advertising agencies</li>
<li>Potential or actual purchasers and/or transferees and/or assignees and/or charges (including the Bank of England FLS) of any of the Bank's benefits, rights, title or interest under any agreement between the customer and the Bank, and their professional advisors, service providers, suppliers and financiers</li>
<li>For our anti-money laundering process, such as credit reference agencies</li>
<li>Receivers</li>
<li>Debt Collection Agencies</li>
</ul>
<p><strong>Transfer of your personal data to a third country or to an international organisation</strong></p>
<p>Whilst we are based in the UK sometimes it's necessary to transfer information outside the UK. Data transferred within the European Economic Area (EEA) is protected by European data protection standards. Some countries outside the EEA do not have adequate protection for personal data under laws that apply. We will therefore make sure that adequate protection is in place before data is transferred in such circumstances.</p>
<p><strong>To what extent there is automated decision-making and whether profiling takes place</strong></p>
<p>In establishing and carrying out a business relationship, we generally do not use any automated decision-making. We may process some of your data automatically, with the goal of assessing certain personal aspects (profiling), in order to enter into or perform a contract with you, in the following cases:</p>
<ul>
<li>Data assessments (including on payment transactions) are carried out in the context of combating money laundering and fraud. An account may be detected as being used in a way that is unusual for you or your business. These measures may also serve to protect you.</li>
<li>Credit scoring is used as part of the assessment of your creditworthiness. This calculates whether you or your business will meet your payment obligations pursuant to a contract. This helps us make responsible lending decisions that are fair and informed.</li>
</ul>
<p>In all of the above cases an individual review will be completed before a final decision is made.</p>
<p><strong>How we treat your personal data for marketing activities and whether profiling is used for such activities</strong></p>
<p>We may process your personal data to tell you about products, services and offers that may be of interest to you or your business.</p>
<p>The personal data that we process for this purpose consists of information you provide to us and data we collect and/or infer when you use our services, such as information on your transactions. We study all such information to form a view on what we think you may need or what may interest you.</p>
<p>We can only use your personal data to promote our products and services to you if we have your explicit consent to do so.</p>
<p>You have the right to object at any time to the processing of your personal data for marketing purposes by contacting either in person or in writing or by calling 0345 850 5555 (+44 (0)20 3375 6422 from outside the UK). Calls may be recorded for monitoring and training.</p>
<p><strong>How long we keep your personal data for</strong></p>
<p>We will keep your personal data for as long as we have a business relationship with you as an individual or in respect of our dealings with a legal entity you are authorised to represent or are a beneficial owner.</p>
<p>Once our business relationship with you has ended, we may keep your data for up to ten (10) years. This period is based on a mixture of our legal and regulatory obligations and limitation periods. The reasons for keeping your data are:</p>
<ul>
<li>To respond to queries or complaints or regulatory requests;</li>
<li>To maintain records according to any rules that apply to us; and</li>
<li>For research and statistical purposes to ensure we continue to make informed lending decisions and understand the performance of our savings products.</li>
</ul>
<p>We may keep your data for longer than 10 years if we cannot delete it for legal, regulatory or technical reasons.</p>
<p>For prospective customer personal data (such as where you give us data but don't subsequently proceed with the application), or authorised representatives/agents or beneficial owners of a legal entity prospective customer, we shall keep your personal data for 12 months from the date of notification of the rejection of your application for banking services and/or facilities or from the date of withdrawal of such application.</p>
<p><strong>Your data protection rights</strong></p>
<p>You have the following rights in terms of your personal data we hold about you:</p>
<ul>
<li><strong>The right to receive access to your personal data.</strong> This enables you to e.g. receive a copy of the personal data we hold about you and to check that we are lawfully processing it. In order to receive such a copy you can download a <a rel="noopener" data-udi="umb://document/ef2254cd4ab14a2da3ee24297cd2f6d4" href="/{localLink:umb://document/ef2254cd4ab14a2da3ee24297cd2f6d4}" target="_blank" title="Data Subject Access Request Form"><u>Data Subject Access Requests form</u> </a>(see the Data Subject Access Requests).</li>
<li><strong>The right to request correction (rectification)</strong> of the personal data we hold about you. This enables you to have any incomplete or inaccurate data we hold about you corrected.</li>
<li><strong>The right to request erasure of your personal data.</strong> This enables you to ask us to erase your personal data (known as the ‘right to be forgotten') where there is no good reason for us continuing to process it. Please note however that this right does not take precedence over our obligations as a regulated business to retain your data in certain circumstances.</li>
<li><strong>The right to object to processing of your personal data</strong> where we are relying on a legitimate interest and there is something about your particular situation which makes you want to object to processing on this ground. If you lodge an objection, we will no longer process your personal data unless we can demonstrate compelling legitimate grounds for the processing which override your interests, rights and freedoms.</li>
</ul>
<p>You also have the right to object where we are processing your personal data, for direct marketing purposes. If you object to processing for direct marketing purposes, then we shall stop the processing of your personal data for such purposes.</p>
<ul>
<li>The right to request the restriction of processing of your personal data. This enables you to ask us to restrict the processing of your personal data, i.e. use it only for certain things, if:</li>
</ul>
<p>It is not accurate,<br />It has been used unlawfully but you do not wish for us to delete it,<br />It is not relevant any more, but you want us to keep it for use in possible legal claims,<br />You have already asked us to stop using your personal data but you are waiting us to confirm if we have legitimate grounds to use your data.</p>
<ul>
<li><strong>The right to request to receive a copy</strong> of the personal data you have provided to us concerning you in a format that is structured and commonly used and transmit such data to other organisations. You also have the right to have your personal data transmitted directly by ourselves to other organisations you will name (known as the right to data portability).</li>
<li><strong>The right to withdraw the consent that you gave us</strong> with regard to the processing of your personal data at any time. Note that any withdrawal of consent shall not affect the lawfulness of processing based on consent before it was withdrawn or revoked by you.</li>
</ul>
<p>To exercise any of your rights, or if you have any other questions about our use of your personal data, please contact your Relationship Manager, visit our North London branch or send a secure message if you are registered for Online Banking.</p>
<p>You can also contact our Data Protection Officer at dpo@cynergybank.co.uk.</p>
<p>We endeavour to address all of your requests promptly.</p>
<p><strong>Right to lodge a complaint</strong></p>
<p>If you have exercised any or all of your data protection rights and still feel that your concerns about how we use your personal data have not been adequately addressed by us, you have the right to complain by <u><a data-udi="umb://document/3ae2ee8f861a44cdabd097dec60009f0" href="/{localLink:umb://document/3ae2ee8f861a44cdabd097dec60009f0}" title="Contact">contacting us</a></u>. You also have the right to complain to the Information Commissioner's Office. Find out on their website how to submit a complaint at <u><a rel="noopener" href="https://ico.org.uk/" target="_blank">https://ico.org.uk/</a></u>.</p>
<p><strong>How to get a copy of your personal data (Data Subject Access Request)</strong></p>
<p>You can obtain a copy of your personal data held by us, by downloading and completing the <u><a rel="noopener" data-udi="umb://media/368b1425ff4e48b9a7f03c5b386096c2" href="/media/1852/dsar-web-form-cynergy-bankv2.pdf" target="_blank" title="DSAR Web Form Cynergy Bank.pdf">Data Access Request Form </a></u>and returning it to us or by writing to us at Cynergy Bank, PO Box 17484, 87 Chase Side, London, N14 5WH.</p>
<p>We'll need address verification and identification documents for each individual making a Data Subject Access Request.</p>
<p>Original or certified address verification and certified identification documents which are acceptable include:</p>
<ul>
<li>A certified copy of each individual's passport,</li>
<li>A certified copy of a utility bill or bank statement (dated within 6 months) confirming each individual's residential address. Please note we do not accept mobile phone bills.</li>
</ul>
<p>Documents can be certified by an EU bank, an EU solicitor, an EU accountant, or a UK Post Office. These copies must be stamped to indicate they are 'true copies of the original'. The certification must also include:</p>
<ul>
<li>The name and address of the certifying firm/organisation (official business stamp required),</li>
<li>The full name and signature of the certifying officer,</li>
<li>The date of certification.</li>
</ul>
<p>We'll deal with your request as quickly as possible, but in no more than 30 calendar days from receipt of all required identification.</p>
<p><strong>Frequently asked questions</strong></p>
<p>To help you understand the basic principles of data privacy law and address some of the common questions that arise with regard to the protection of your personal data, please refer to the <u><a data-udi="umb://document/6d376cc3991046138adbba8ac203cfc9" href="/{localLink:umb://document/6d376cc3991046138adbba8ac203cfc9}" title="FAQ">Frequently Asked Questions.</a></u></p>
<p><strong>Cookies</strong></p>
<p>Browsers are software used to access any webpage, like Internet Explorer, Firefox or Google Chrome, to help navigate through websites. Cookies are small amounts of text that websites send to your browser to help it navigate through the pages.</p>
<p>Cookies contain information that is transferred to your computer's hard drive/mobile devices. These cookies are used to store information, such as the time that the current visit occurred, whether you have been to the site before and what site referred you to the web page.</p>
<p>Our website uses performance cookies to monitor how the website is used. All information is stored in an anonymous form and no personal data is captured by this site automatically.</p>
<p>We use performance cookies on our website and Live Chat to:</p>
<ul>
<li>Store the location of the IP address so that customers accessing the site from outside the UK are routed to the international customer's section of the website;</li>
<li>Help Cynergy Bank improve the website by understanding what pages are popular;</li>
<li>Identify and correct errors;</li>
<li>Provide statistics on how our website is used so we can improve the site; and</li>
<li>Monitor the effectiveness of our adverts.</li>
</ul>
<p>IP address, operating system and browser type. This is statistical data, used to analyse user's browsing patterns to help us understand how customers use the site, and does not identify any individual.</p>
<p>Information on how to disable cookies is available <u><a rel="noopener" href="http://www.allaboutcookies.org/manage-cookies/" target="_blank">http://www.allaboutcookies.org/manage-cookies/</a></u></p>
<p><strong>IP address</strong></p>
<p>Your IP address is the individual identification number that is assigned to your computer when connected to the Internet. This is automatically logged by our web-server and Online Banking application. We use the IP address to route users to the international customer's pages if the IP address is located outside the UK. Unless we suspect fraud, your IP address will not be used to identify you personally.</p>
<p><strong>Google Analytics</strong></p>
<p>Google Analytics is a web analytics tool that helps us understand how visitors engage with our website. Google Analytics enables us to view a variety of reports about how visitors interact with our website so we can improve it. Google Analytics uses first-party cookies to collect information about how visitors use our site. We then use the information to compile reports and to help us improve our site.</p>
<p>Google Analytics collects information anonymously. It reports website trends without identifying individual visitors. You can opt out of Google Analytics without affecting how you visit our site – for more information on opting out of being tracked by Google Analytics across all websites you use, visit this Google page. Our 'Performance' cookies will not be used to:</p>
<ul>
<li>Gather information that can be used to advertise products or services to you on other websites;</li>
<li>Target adverts to you on any other website.</li>
</ul>
<p>Using our site indicates that you accept the use of our cookies. If you disable them we cannot guarantee how our site will perform.</p>
<p><strong>Disabling cookies</strong></p>
<p>Most popular browsers give users control over the cookies stored on their machines. You can manually set your browser to accept or reject all or certain cookies or to prompt you every time a cookie is offered. Please note that a cookie will be used to remember your preferences, therefore:</p>
<ul>
<li>If you delete all your cookies you will have to update your preferences with us again.</li>
<li>you use a different device, computer profile or browser you will have to tell us your preferences again.</li>
</ul>
<p>Please note that parts of our website may direct you to a third party's website over which we have no control. For more information about cookies please visit the website set up by the Interactive Advertising Bureau (Europe) at <u><a rel="noopener" href="http://www.allaboutcookies.org/" target="_blank">http://www.allaboutcookies.org/</a></u>.</p>
<p><strong>Other websites</strong></p>
<p>Our website contains links to other websites. This privacy policy only applies to this website. Other websites will have their own privacy policies and when we transfer you to another website we will make this clear to you.</p>
<p><strong>Changes to our privacy policy</strong></p>
<p>We keep our privacy policy under regular review and we will place any updates on the relevant section of our website.</p>
<p>If you have any questions about this privacy policy or the information we hold about you; then you may phone us on 0345 850 5555 or write to us at Cynergy Bank, PO Box 17484, 87 Chase Side, London, N14 5WH.</p>]]></topContent>
  <umbracoInternalRedirectId></umbracoInternalRedirectId>
  <umbracoNaviHide>0</umbracoNaviHide>
  <umbracoUrlAlias></umbracoUrlAlias>
</contentPage>