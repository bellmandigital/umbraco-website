﻿<?xml version="1.0" encoding="utf-8"?>
<product guid="f7a407be-6bcc-4721-b162-845f60874091" id="2678" nodeName="Fixed Rate Mortgage" isDoc="" updated="2018-12-10T18:13:50.7070000Z" parentGUID="b9d128c8-ba33-4641-9a98-a3c41efa1316" nodeTypeAlias="product" templateAlias="Product" sortOrder="7" published="true" isBlueprint="false">
  <additionalText></additionalText>
  <aERGrossToken></aERGrossToken>
  <aERToken></aERToken>
  <backToLink></backToLink>
  <bottomContent><![CDATA[<p>LIMITED AVAILABILITY - PRODUCT MAY BE WITHDRAWN WITHOUT NOTICE.</p>
<p>YOUR MORTGAGE IS SECURED ON YOUR HOME. THINK CAREFULLY BEFORE SECURING OTHER DEBTS AGAINST YOUR HOME. YOUR HOME MAY BE REPOSSESSED IF YOU DO NOT KEEP UP REPAYMENTS ON YOUR MORTGAGE.</p>]]></bottomContent>
  <displayTagLine></displayTagLine>
  <doNotUseParentPageHeader>0</doNotUseParentPageHeader>
  <hideFSCSBox>1</hideFSCSBox>
  <icons></icons>
  <iconsBackgroundColour></iconsBackgroundColour>
  <metaDescription></metaDescription>
  <metaKeywords></metaKeywords>
  <pageHeader><![CDATA[umb://document/336e9e6a155849068b590dd901461bea]]></pageHeader>
  <pageTitle><![CDATA[Fixed Rate Mortgage]]></pageTitle>
  <promoBanner><![CDATA[umb://document/314fa7e0b2c741ddbe9fe2df7955a97d]]></promoBanner>
  <sideBarModules></sideBarModules>
  <tabs><![CDATA[[
  {
    "key": "96fe7488-032b-4139-a429-d1838c9ade0f",
    "name": "Features & Benefits",
    "ncContentTypeAlias": "freetextTab",
    "title": "Features & Benefits",
    "content": "<ul>\n<li>Our mortgage applications are individually underwritten and a credit search is undertaken. We do not credit score.</li>\n<li>Mortgage applicants that cannot afford the mortgage alone can seek support from family members to jointly apply for a mortgage. However, the legal ownership of the property will be registered solely in the first applicant’s name. This is known as ‘Joint mortgage sole proprieter’ and is a great solution for younger people looking to get on to the property ladder with some help from their family.</li>\n<li>We may allow applicants’ deposits to be gifted by a close family member, who we may allow to reside in the property subject to receiving independent legal advice.</li>\n<li>We will consider lending beyond the age of 70 or the customer's intended retirement age where it is evident the applicant has the ability to repay the borrowing requested for the full term.</li>\n<li>We have a flexible approach to how we can assess income. For example, we will consider up to 100% of bonus with a two-year history and we can consider income where the applicant has been self-employed for less than two years.</li>\n<li>We consider contractor applicants on non-permanent employment contracts. Contractor income calculated as a daily rate over 46 weeks or the gross value where agreed as a fixed contract value. We also cater for Specialist Contractors.</li>\n<li>For self-employed applicants, affordability may be assessed using the latest year figures, subject to an assessment of the last two years business performance.</li>\n</ul>"
  },
  {
    "key": "134ffc2d-6b5a-42b9-990e-b7f60d9d02a6",
    "name": "Summary Box",
    "ncContentTypeAlias": "freetextTab",
    "title": "Summary Box",
    "content": "<table border=\"0\" class=\" table table-bordered table-striped table-hover\">\n<tbody>\n<tr>\n<td style=\"width: 104px;\"><strong>Mortgage type</strong></td>\n<td colspan=\"4\" style=\"width: 872px;\">\n<p><strong>Fixed rate</strong></p>\n<p>Fixed rate mortgages have an interest rate that stays the same for a set period of time. During the fixed rate period your monthly repayments stay the same. At the end of the fixed rate period the interest rate will change, usually to the lender's standard variable rate (SVR).</p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 104px;\">Fixed rate term</td>\n<td style=\"width: 218px;\"> 2 years</td>\n<td style=\"width: 218px;\"> 2 years</td>\n<td style=\"width: 218px;\"> 5 years</td>\n<td style=\"width: 218px;\"> 5 years</td>\n</tr>\n<tr>\n<td style=\"width: 104px;\">Maximum Loan to Value (LTV)</td>\n<td style=\"width: 218px;\"> 60%</td>\n<td style=\"width: 218px;\"> 80%</td>\n<td style=\"width: 218px;\"> 60%</td>\n<td style=\"width: 218px;\"> 80%</td>\n</tr>\n<tr>\n<td style=\"width: 104px;\">Capital repayment interest rates<br /> </td>\n<td style=\"width: 218px;\">1.69% / 4.3% (APRC)</td>\n<td style=\"width: 218px;\">1.89% / 4.3% (APRC)</td>\n<td style=\"width: 218px;\">2.14% / 3.8% (APRC)</td>\n<td style=\"width: 218px;\">2.29% / 3.9% (APRC)</td>\n</tr>\n<tr>\n<td rowspan=\"2\" valign=\"top\" style=\"width: 104px;\">Interest only interest rates</td>\n<td style=\"width: 218px;\">2.44% / 4.6% (APRC)</td>\n<td style=\"width: 218px;\">N/A</td>\n<td style=\"width: 218px;\">2.89% / 4.3% (APRC)</td>\n<td style=\"width: 218px;\">N/A</td>\n</tr>\n<tr>\n<td colspan=\"4\" style=\"width: 872px;\">Following the fixed rate period, the mortgage will revert to the Bank of Cyprus UK variable rate (currently 4.50%)</td>\n</tr>\n<tr>\n<td style=\"width: 104px;\"> Fees</td>\n<td colspan=\"4\" style=\"width: 872px;\">\n<ul>\n<li>£1,395 product fee for purchase and remortgage applications.</li>\n<li>£1,995 product fee for interest only applications.</li>\n<li>Product fee is payable on completion. Can be added to the loan however the loan including fees may not exceed the maximum product LTV.</li>\n<li>£195 - £1,980 valuation fee based on the purchase price up to £3m. Fee by negotiation when the purchase price exceeds £3m.</li>\n<li>£58 Mortgage Exit Administration Fee (MEAF).</li>\n</ul>\n</td>\n</tr>\n<tr>\n<td style=\"width: 104px;\"> Features</td>\n<td colspan=\"4\" style=\"width: 872px;\">\n<ul>\n<li>Up to a maximum 10% per annum penalty-free overpayments per year allowed during the term of the product.</li>\n<li>Portable.</li>\n<li>Free Legals for remortgages up to £1m.</li>\n<li>Free basic valuation for remortgages up to £2m.</li>\n</ul>\n<p>(Not available to interest-only mortgages/specialists mortgages)</p>\n<p>Note: Solicitor panel used for remortgages is O’Neill Patient.</p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 104px;\"> Loan size</td>\n<td colspan=\"4\" style=\"width: 872px;\">\n<p>There is no maximum loan size. LTV limits are applicable as follows:</p>\n<p>Capital and repayment mortgages</p>\n<ul>\n<li>Loans below £1m allowed up to 80% LTV</li>\n<li>Loans between £1m and £2m allowed up to 70% LTV</li>\n<li>Loans over £2m allowed up to 60% LTV (please contact us for product details)</li>\n</ul>\n<p>Interest only and Part and Part mortgages available to a maximum of 60% LTV.</p>\n</td>\n</tr>\n<tr>\n<td style=\"width: 104px;\">Early Repayment Charge</td>\n<td colspan=\"4\" style=\"width: 872px;\">The ERC applies from the date of completion.<br />\n<ul>\n<li>For 2 year fixed rate loans the ERC is 2% in year 1 and 1% in year 2 of the balance outstanding.</li>\n<li>For 5 year fixed rate loans the ERC is 5% in year 1, 4% in year 2, 3% in year 3, 2% in year 4 and 1% in year 5 of the balance outstanding.</li>\n<li>During the ERC period you are permitted to make penalty-free overpayments up to a maximum of 10% of the balance outstanding at the account anniversary per year. If the overpayments exceed 10% in a year during the ERC period, you will have to pay the relevant ERC percentage rate on the amount of overpayment exceeding the permitted level. Lump sum payments must be a minimum of £1,000.</li>\n<li>The full ERC is payable on the balance outstanding if you repay your mortgage in full during the ERC period.</li>\n</ul>\n</td>\n</tr>\n<tr>\n<td style=\"width: 104px;\"> Eligibility</td>\n<td colspan=\"4\" style=\"width: 872px;\">\n<ul>\n<li>Minimum property value of £50,000.</li>\n<li>The property must be located in England or Wales (Properties in Scotland, Northern Ireland, Channel Islands, Isle of Man or overseas properties not accepted).</li>\n<li>The mortgage term must be between 5 and 35 years.</li>\n<li>Minimum age 18, UK residents only.</li>\n<li>Mortgages are subject to underwriting and lending criteria.</li>\n</ul>\n</td>\n</tr>\n<tr>\n<td style=\"width: 104px;\">How to apply</td>\n<td colspan=\"4\" style=\"width: 872px;\">\n<p>Our experienced Mortgage Adviser can help you find a mortgage you feel at home with and support you throughout the application and lifetime of the mortgage.<br /><br /><strong>Costas Agathangelou CeMAP</strong><br />Mortgage Adviser<br /><br />Email: cagathangelou@cynergybank.co.uk<br /><br />Phone: 020 8920 1541</p>\n</td>\n</tr>\n</tbody>\n</table>\n<p>These features are not an exhaustive list.</p>\n<h4>Representative example for a 60% LTV 2 year fixed rate mortgage</h4>\n<p>A 25 year mortgage of £340,000 payable over 25 years initially on a fixed rate for 2 years at 1.39% and then on our standard variable rate, currently 4.5% for the remaining 23 years require 24 monthly payments of £1,324.28 and 276 monthly payments of £1,845.24.<br /><br />The total amount payable would be £543,011 which consists of:</p>\n<ul>\n<li>The original mortgage amount of £340,000.</li>\n<li>Interest of £202,953</li>\n<li>£1,395 product fee plus £590 valuation fee (property value of £560,000) and a £58 MEAF.</li>\n</ul>\n<p>The overall cost for comparison is 4.1% APRC representative.</p>"
  },
  {
    "key": "cf762438-7245-4d10-9852-210893e4f99f",
    "name": "How to apply",
    "ncContentTypeAlias": "freetextTab",
    "title": "How to apply",
    "content": "<p>We can help you</p>\n<p>Our experienced Mortgage Adviser can help you find a mortgage you feel at home with and support you throughout the application and lifetime of the mortgage.</p>\n<table border=\"0\" class=\" table table-bordered table-striped table-hover\">\n<tbody>\n<tr>\n<td colspan=\"2\"><strong>Costas Agathangelou CeMAP</strong><br />Mortgage Adviser<br /><br />Email:<br />cagathangelou@cynergybank.co.uk<br /><br />Phone:<br />020 8920 1541</td>\n<td>Costas has over 25 years’ experience at Cynergy Bank helping customers with their mortgage requirements.</td>\n</tr>\n</tbody>\n</table>"
  }
]]]></tabs>
  <tagLine></tagLine>
  <topContent><![CDATA[<h2><strong data-redactor-tag="strong">Fixed rate mortgages to help you buy your home</strong></h2>
<p class="btn btn-primary"><strong>Fixed rates from 1.69% / 4.3% (APRC) for two years up to 60% LTV*</strong></p>
<p>Our mortgages are tailored towards the needs of our customers, whether you are a first time buyer looking for a residential mortgage or an existing homeowner looking to remortgage.</p>
<p>Our range of purchase and remortgage products includes:</p>
<ul>
<li>Residential capital repayment</li>
<li>Residential interest only <br /><strong>Interest only mortgages will require an acceptable method of repayment to the Bank</strong></li>
<li>Part and Part mortgages (part capital repayment and part interest only)</li>
<li>Second homes</li>
<li>Lending into retirement</li>
</ul>
<p>*Other specialist lending rates and terms are available.</p>]]></topContent>
  <umbracoInternalRedirectId></umbracoInternalRedirectId>
  <umbracoNaviHide>1</umbracoNaviHide>
  <umbracoRedirect></umbracoRedirect>
  <umbracoUrlAlias><![CDATA[fixed-rate-mortgage]]></umbracoUrlAlias>
</product>