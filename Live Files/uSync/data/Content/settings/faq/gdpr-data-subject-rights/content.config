﻿<?xml version="1.0" encoding="utf-8"?>
<fAQSet guid="f83144f4-bc14-4891-aedb-124e23df409e" id="2251" nodeName="GDPR: Data Subject rights" isDoc="" updated="2020-02-11T16:18:07.8070000Z" parentGUID="cdf0b4d6-152c-42e0-acf2-6c1ea93be269" nodeTypeAlias="fAQSet" templateAlias="" sortOrder="18" published="true" isBlueprint="false">
  <questionAnswerSet><![CDATA[[
  {
    "key": "202448ae-d49f-4d7c-9732-a61ff2fc946b",
    "name": "What is a Data Subject Access Request?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What is a Data Subject Access Request?",
    "answer": "<p>You have the right under the General Data Protection Regulation (GDPR) to request to see the personal data Cynergy Bank holds about you. This is called a Data Subject Access Request (DSAR).</p>\n<p>A DSAR is not designed to deal with general enquiries or questions that you may have about your account, for general queries please contact Customer Services on 0345 850 5555 (+44 (0)20 3375 6422 from outside the UK). We can then aim to provide you with the information you require, quickly, without you having to make a formal request.</p>\n<p>DSARs can be specific and targeted by you to answer a specific question regarding the personal data we hold on you. We always prefer if you specify the information you are hoping to receive so we can turn around your request in the quickest time possible and not waste your time by providing you with too much information.</p>\n<h3 style=\"margin: 0cm 0cm 0pt; text-align: justify;\"><span style=\"font-family: 'Verdana',sans-serif; font-size: 10pt;\"> </span></h3>"
  },
  {
    "key": "2ecfb5f4-d842-4332-9871-b653ca649315",
    "name": "How do I make a Data Subject Access Request?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "How do I make a Data Subject Access Request?",
    "answer": "<p>To make a Data Subject Access Request, you can submit it via the following options;</p>\n<p>a) Download the <a rel=\"noopener\" data-udi=\"umb://document/ef2254cd4ab14a2da3ee24297cd2f6d4\" href=\"/{localLink:umb://document/ef2254cd4ab14a2da3ee24297cd2f6d4}\" target=\"_blank\" title=\"Data Subject Access Request Form\">Data Subject Access Request form</a> or the <a rel=\"noopener\" data-udi=\"umb://document/8a79eef159124036a82f594fa16c3a38\" href=\"/{localLink:umb://document/8a79eef159124036a82f594fa16c3a38}\" target=\"_blank\" title=\"Data Subject Access Request Employee Form\">Employee/Former Employee Subject Access Request form</a>, complete it and send it to;</p>\n<p>Data Subject Access Request (DSAR)<br />Customer Service<br />Cynergy Bank<br />PO Box 17484<br />87 Chase Side<br />London<br />N14 5WH</p>\n<p>b) Contact our Customer Service Team on 0345 850 5555 who will talk you through the process and lodge the request on your behalf.</p>\n<p>If we need more details to help us find your information or identify you, we will contact you. Once we have all the necessary information, we will respond to your request within 30 days.</p>\n<p>We must confirm your identity using our standard identification procedures before any information is released; this is to ensure that personal information is not inadvertently released to the wrong person.</p>"
  },
  {
    "key": "e13a19f9-eed9-49f9-aecc-3c08837b1264",
    "name": "Do I have to pay to make a Data Subject Access Request?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Do I have to pay to make a Data Subject Access Request?",
    "answer": "<p>No. The first request is free of charge. However, for any further requests or copies, we may charge a reasonable fee based on our administrative costs.</p>"
  },
  {
    "key": "b243efa6-3363-4adb-a4f1-a1855e1520f7",
    "name": "What proof of ID do I need?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What proof of ID do I need?",
    "answer": "<p class=\"Default\">Your privacy is important to us. Therefore, as with any banking transaction, it is important that the bank is sure of your identity (the data subject) before processing a request and releasing information. The ID that you will need to provide is listed below;</p>\n<p class=\"Default\">Original or certified address verification and certified identification documents which are acceptable include:</p>\n<ul>\n<li>Utility bill (dated within the last 3 months) (mobile phone bills not accepted)</li>\n<li>Council tax bill (for the current year)</li>\n<li>Bank statement (dated within the last 3 months)</li>\n<li>Mortgage statement from a recognised lender for the current year</li>\n<li>Driving license, which shows your current address</li>\n<li>Passport</li>\n</ul>\n<p class=\"Default\">We can accept photocopies of ID but these must be certified and stamped either by a Cynergy Bank employee, an accountant, solicitor or an independent financial advisor. It has to be clear that the ID has been certified by one of these people, providing their name and business address.</p>\n<p class=\"Default\">If you are acting on behalf of the data subject you are still required to provide the above documentation on behalf of the data subject so that we can validate them and their signed authority for you to act on their behalf.</p>\n<p>You should provide the above information to the Cynergy Bank either by post or in person at the address detailed above (see “Q. How do I make a Data Subject Access Request”).</p>\n<p>Please note, we do not recommend that you send the documentation by email as the Cynergy Bank cannot guarantee the security of doing so. If however, you still wish to, you do so as at your own risk and the Cynergy Bank shall not be held liable for any loss that may be suffered as a result.  </p>"
  },
  {
    "key": "44fe82b4-11fa-4ebd-8824-968e57376575",
    "name": "How long does it take?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "How long does it take?",
    "answer": "<p>The legal requirement is for the request to be completed within 30 calendar days. However in some cases it may take longer for us to process your request, for instance if your request is not clear and we require further information from you.</p>\n<p>In accordance with the regulation, we reserve the right to seek additional information from you which would enable us to process your request. Should this be the case, it may be necessary to extend the 30 day period and we will be in touch to inform you of the reasons for this. We will also advise you of any further clarification or information we require. The 30 days for processing your DSAR will then commence from the date we receive this additional information from you.</p>"
  },
  {
    "key": "d728695e-2085-4fd3-b08e-7c1abd9dbfbd",
    "name": "What happens when I make a Data Subject Access Request?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What happens when I make a Data Subject Access Request?",
    "answer": "<p>When your application is received, it will be checked to ensure that we have received all the correct, requested documentation.</p>\n<p>If your request is incomplete then we will contact you requesting more information before we accept your request.</p>\n<p>Where the request is not valid, we will write to you, returning your request, along with a reason for not accepting the request. You then have the option to submit a new request with the corrected information.</p>\n<p>Where the request is correct and we have received satisfactory ID, Cynergy Bank will issue out an acknowledgement letter or email to you and confirm the latest date by which you should receive a copy of the information we hold (if any). Where no information is held, you will receive a letter to confirm this.</p>"
  },
  {
    "key": "8ab7a47e-4a78-453a-a520-39f0c8850246",
    "name": "Can I make a DSAR on someone else’s behalf?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Can I make a DSAR on someone else’s behalf?",
    "answer": "<p>You only have right to access your own personal data under the regulation. We will not provide personal data about any other individual e.g. your family, friends except in the following circumstances:</p>\n<ul>\n<li>You have their written permission to do so (and you can evidence this to our satisfaction).</li>\n</ul>\n<ul>\n<li>You are a parent requesting information about a child under 16 that you have legal responsibility for; however, there is no automatic right to the data. If a child is old enough to give informed consent and understands the contents of the information, the Bank will be guided by their wishes. In all cases, disclosure would only occur if it is in the best interests of the child.</li>\n</ul>\n<ul>\n<li>A solicitor/accountant is requesting information on behalf of a client - a signed authority form from the person concerned is required.</li>\n</ul>\n<ul>\n<li>An agent (i.e. a family member) has written authorisation or power of attorney to act on behalf of the person.</li>\n</ul>\n<ul>\n<li>You have a court order authorising you to make the request (e.g. police request)</li>\n</ul>\n<p class=\"Default\">If none of the above apply you are unlikely to be able to make a DSAR on their behalf.</p>"
  },
  {
    "key": "7e57ab92-0a70-405b-b550-907fc71e6e7a",
    "name": "How will I receive my information?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "How will I receive my information?",
    "answer": "<p>You can request to receive your information via post to your address or collect from our North London office. We prefer if you are able to collect information from our office as this is more secure and minimises the risk that information may be lost or delayed. Please state your preference when raising your request.</p>"
  },
  {
    "key": "73475b7c-be83-4e48-bf7d-b821a363226d",
    "name": "How do I appeal?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "How do I appeal?",
    "answer": "<p>If you are dissatisfied with information you have received, you can request us to review the information in the first instance. If you have reason to believe that there are specific documents missing from your disclosure, it will help us investigate if you can list them or provide us with more information about the location of those documents.</p>\n<p>Following that, if you remain dissatisfied with our response, you are entitled raise a Complaint or refer the matter to the Information Commissioner’s Office.</p>\n<p>The ICO is the national regulating agency for matters associated with data protection and may undertake to investigate on your behalf. Should you wish to avail yourself of this option, the ICO can be contacted in the following ways:</p>\n<p>Website: <a rel=\"noopener\" href=\"http://www.ico.org.uk/\" target=\"_blank\">http://www.ico.org.uk</a></p>\n<p>Phone: 0303 123 1113</p>\n<p>Post:</p>\n<p>Information Commissioner's Office<br />Wycliffe House<br />Water Lane<br />Wilmslow<br />Cheshire<br />SK9 5AF</p>\n<p>Tel: 0303 123 1113 (local rate)</p>\n<p>Please note that some historic data may no longer be held due to our data retention policy e.g. information on closed accounts or some financial products is destroyed 7 years past their closure date.</p>\n<p>If you are submitting a data subject access request relating to an ongoing appeals process or another kind of ongoing review, some of this data may be considered exempt from disclosure. If this is data that you have directly requested, we will ordinarily inform you that we have considered that particular document exempt from disclosure. We will normally be able to disclose these documents once the appeals process is over.</p>"
  },
  {
    "key": "713daabd-a3e9-4035-975f-a2b527a0ed42",
    "name": "I made a request over 30 days ago and haven’t had a response yet.",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "I made a request over 30 days ago and haven’t had a response yet.",
    "answer": "<p class=\"Default\">Please check your email and post to see if you received a letter from us asking for more information. For example if you did not send the right ID we will have contacted you to ask for this. The time to comply does not start running until we have confirmed the identity of the Data Subject.</p>\n<p class=\"Default\">Please check and see if we have asked you for clarification for example if what you are asking for was not clear. If you have checked and we are not waiting to hear from you with ID, or to clarify then:</p>\n<ul>\n<li>Contact the person who sent the acknowledgement letter/email to ask them for a progress update.</li>\n</ul>\n<ul>\n<li>If you haven’t heard back from the bank at all, please contact us on <a rel=\"noopener\" href=\"mailto:dsar@cynergybank.co.uk\" target=\"_blank\" title=\"dsar@cynergybank.co.uk\">dsar@cynergybank.co.uk</a> with as much information as possible and we will look into it for you.</li>\n</ul>"
  },
  {
    "key": "6f5fe962-5ef7-4054-83ac-898930bc0236",
    "name": "I’m not happy with how my DSAR was handled.",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "I’m not happy with how my DSAR was handled.",
    "answer": "<p class=\"Default\">We’re sorry to hear that. If you’d like to ask for a review of how your DSAR was handled please contact our Data Protection Officer directly on <a rel=\"noopener\" href=\"mailto:dpo@cynergybank.co.uk\" target=\"_blank\" title=\"mailto:dpo@cynergybank.co.uk\">dpo@cynergybank.co.uk</a> or speak with one of our Customer Service agents our helpline explaining clearly and concisely why you are unhappy. We will review the DSAR and your concerns and then write to you with our findings.                                   </p>\n<p class=\"Default\">If you have already had a review and are still dissatisfied you can complain to the Information Commissioner’s Office on the details provided in above.</p>"
  }
]]]></questionAnswerSet>
</fAQSet>