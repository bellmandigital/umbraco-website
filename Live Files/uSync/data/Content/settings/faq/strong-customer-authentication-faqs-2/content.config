﻿<?xml version="1.0" encoding="utf-8"?>
<fAQSet guid="71f68329-77ec-4183-9440-c09319847934" id="5070" nodeName="Strong Customer Authentication FAQs 2" isDoc="" updated="2020-02-11T16:18:06.6530000Z" parentGUID="cdf0b4d6-152c-42e0-acf2-6c1ea93be269" nodeTypeAlias="fAQSet" templateAlias="" sortOrder="1" published="false" isBlueprint="false">
  <questionAnswerSet><![CDATA[[
  {
    "key": "caa6a513-40ff-446a-9eb5-64f6dd7449f3",
    "name": "Why are you improving Online Banking Security?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Why are you improving Online Banking Security?",
    "answer": "<p>New EU regulations have been adopted under the revised Payment Services Directive (PSD2) which will enhance the security of online transactions to help protect you and your money and reduce fraud. These changes are referred to as Strong Customer Authentication (SCA). As a result of these changes you will need to authenticate your access every time you log in to Online Banking and sometimes when you are processing transactions, so we know it’s actually you.   </p>\n<p>The additional security is being introduced to help protect you and your money when online and to help fight fraud.</p>"
  },
  {
    "key": "35db6db7-90fe-4fae-8cc3-a8bf9a113adc",
    "name": "What is Strong Customer Authentication?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What is Strong Customer Authentication?",
    "answer": "<p>Banks across Europe are currently introducing additional security for online transactions, known as Strong Customer Authentication (SCA). This means you may be asked to provide an additional authentication code when using Online Banking to verify it’s you logging in or completing a transaction.</p>"
  },
  {
    "key": "76b9b3cf-9af6-4262-b52c-d2f6d8c012f8",
    "name": "Why is Strong Customer Authentication being introduced?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Why is Strong Customer Authentication being introduced?",
    "answer": "<p>The additional security is being introduced to help protect you and your money when online and to help fight fraud.</p>"
  },
  {
    "key": "1998fbeb-fc53-4bac-94bf-e78d19dc832d",
    "name": "What countries does this regulation apply to?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What countries does this regulation apply to?",
    "answer": "<p>The new rules apply to banks across Europe.</p>"
  },
  {
    "key": "a3cffb4d-62c9-43a0-bcf1-fa130092edb3",
    "name": "When is it being introduced?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "When is it being introduced?",
    "answer": "<p>From 14 March 2020, users will no longer be able to access to our Online Banking using just their User ID and passcode.     </p>"
  },
  {
    "key": "47d793c8-83a5-467a-aca5-cc244a3c827b",
    "name": "Who does it apply to?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Who does it apply to?",
    "answer": "<p>The new security measures will apply to all users of our online services.</p>"
  },
  {
    "key": "a75c3deb-f590-4da7-9a0c-a7012d7892b0",
    "name": "I don’t use Online Banking, will this impact me?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "I don’t use Online Banking, will this impact me? ",
    "answer": "<p>No, for the time being, these changes only affect Online Banking users. </p>"
  },
  {
    "key": "f7ddbe29-cfda-4d79-938e-9e798430d842",
    "name": "How will Online Banking change?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "How will Online Banking change?",
    "answer": "<p>We recommend Online Banking users to use the free “Cynergy Bank Authenticator” app when logging in and making certain transactions through Online Banking.</p>"
  },
  {
    "key": "1adfc5d2-8a8f-492a-acf7-b94e326f27fd",
    "name": "How does the Cynergy Bank Authenticator app work?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "How does the Cynergy Bank Authenticator app work?",
    "answer": "<p>The app is used to scan special authentication images that we will display in Online Banking.  The images are unique to you and the transaction you have requested – they look like this:</p>\n<p><img style=\"width: 164px; height: 165px;\" src=\"/media/2502/cynergy-bank-authenticator-app.jpg?width=164&amp;height=165\" alt=\"\" data-udi=\"umb://media/cce0e0349b01456295fa6730a45b96c9\" /></p>\n<p>Once you have scanned an image the app will display a unique authentication code that you will need to input into Online Banking to complete certain actions such as logging in.</p>"
  },
  {
    "key": "87517b11-6ce9-4a7b-90c9-c782f6f59a02",
    "name": "Is the app available to download yet?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Is the app available to download yet?",
    "answer": "<p>You can download the app to any smartphone or tablet that uses the iOS or Android operating system. </p>"
  },
  {
    "key": "371ce596-e2dd-4ea3-bdd5-ebeade208e26",
    "name": "What devices can I download the app to?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What devices can I download the app to?",
    "answer": "<p>You can download the app to any smartphone or tablet that uses operating systems iOS 8.0 onwards or Android 4.1 onwards.  We’ll contact our Online Banking users and update this page when it’s available to download. </p>"
  },
  {
    "key": "b5d32523-173a-4b3d-8ddd-26d9123afaed",
    "name": "Do I need to register the app before I can use it?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Do I need to register the app before I can use it?",
    "answer": "<p>Yes, once the new ‘Cynergy Bank Authenticator’ app is available, you’ll need to download it to your smartphone or tablet.  Once you’ve done this:</p>\n<ol>\n<li>Log in to Online Banking (on a different device – ideally a laptop or desktop) and follow the simple on-screen steps to register your app.</li>\n<li>Once registered, you need the app to authenticate when you log in to Online Banking, make withdrawals and certain other requests via Online Banking.</li>\n</ol>\n<p>(Content regarding step by step guide to register and use the new ‘Cynergy Bank Authenticator’ app need to be included here or another FAQ item in this section?)</p>"
  },
  {
    "key": "8a6a4bcc-7ea8-4615-a30e-a0ab612bee0e",
    "name": "How many devices can I register the app to?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "How many devices can I register the app to?",
    "answer": "<p>You’ll only be able to register the app, and use it, on a single device, for example, your smartphone. </p>"
  },
  {
    "key": "606b8a98-9b09-45a6-8769-aedae9917488",
    "name": "Can I use the app for both my personal and business accounts?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Can I use the app for both my personal and business accounts?",
    "answer": "<p>Yes, the new app will be registered to you as an individual and the same app can be used across all your accounts with Cynergy Bank.</p>"
  },
  {
    "key": "d7908aa6-ceb7-4ed2-a051-17a4ad70ce20",
    "name": "Can I share the app with a joint account holder?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Can I share the app with a joint account holder?",
    "answer": "<p>No, the new app will be registered to you as an individual.  Each account holder will need to register and use their own app to access Online Banking.</p>"
  },
  {
    "key": "95de7217-ba16-496d-ba5d-08c7d00663c2",
    "name": "Can I register/use the app on the same device I log into Online Banking with?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Can I register/use the app on the same device I log into Online Banking with?",
    "answer": "<p>No. At the moment you must register and use the app on a different device to the one you log in to Online Banking with.</p>\n<p>This is because you need to be able to point the device you have downloaded your app to at a screen that is displaying an authentication image.</p>\n<p>We recommend you download the app to your smartphone or tablet and then use a different device, ideally a laptop or desktop, when you need to register and use the app. </p>\n<p>We are working on how you can use the app and Online Banking on the same device and will update you when this functionality is available.</p>"
  },
  {
    "key": "ae3ffe88-c53f-4165-922f-4d44d453f4cb",
    "name": "I don’t have a smartphone or tablet – will I still be able to use Online Banking?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "I don’t have a smartphone or tablet – will I still be able to use Online Banking?",
    "answer": "<p>Yes, if you don’t have a smartphone or tablet you will need the new, upgraded version of our Digipass®.  </p>\n<p>It’s a small device, about the size of a large matchbox, that you can use instead of our app to Authenticate when using Online Banking.</p>\n<p>You can order from our Customer Service team – we’ll update this page when it’s available to order – please don’t request one yet.</p>\n<p>(Content regarding step by step guide to register and use upgraded Digipass® need to be included here or another FAQ item in this section?)</p>"
  },
  {
    "key": "951683cd-adb6-4156-8c25-6c5d1a95a286",
    "name": "I currently use a Digipass® to log into Online Banking, do I have to use a new Digipass®?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "I currently use a Digipass® to log into Online Banking, do I have to use a new Digipass®? ",
    "answer": "<p>No, the Authenticator app will be available all business and personal customers, whether you use a Digipass® or not.  If you’d prefer to keep using a Digipass® you’ll need to request the upgraded version of our Digipass®. </p>\n<p>You can order from our Customer Service team.</p>"
  },
  {
    "key": "ab978e37-b201-4854-bb65-619d51032461",
    "name": "What should I do with my existing Digipass?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What should I do with my existing Digipass?",
    "answer": "<p>You will need to use your existing Digipass to complete the registration process for either the new Authenticator app or our upgraded Digipass. Once you have completed registration on Online Banking you’ll be able to dispose of your old Digipass. Please treat this as electronic waste.</p>"
  },
  {
    "key": "4a527e98-8842-4156-949c-cbe4da802cdf",
    "name": "Can I use the Digipass for both my personal and business accounts?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Can I use the Digipass for both my personal and business accounts?",
    "answer": "<p>Yes, the new Digipass will be registered to you as an individual and the same app can be used across all of your accounts with Cynergy Bank.</p>"
  },
  {
    "key": "878bf1eb-3e51-4d2e-82e2-ace4560eb7e1",
    "name": "Can I share the Digipass with a joint account holder?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Can I share the Digipass with a joint account holder?",
    "answer": "<p>No, the new Digipass will be registered to you as an individual. Each account holder will need to register and use their own Digipass to access Online Banking.</p>"
  },
  {
    "key": "a99e4eeb-b4dd-48f2-8988-f45d60f3de1f",
    "name": "Where can I find more information on Strong Customer Authentication?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Where can I find more information on Strong Customer Authentication?",
    "answer": "<p>You can find more information on the following sites:</p>\n<p><a rel=\"noopener\" href=\"https://www.fca.org.uk/consumers/strong-customer-authentication\" target=\"_blank\">https://www.fca.org.uk/consumers/strong-customer-authentication</a></p>\n<p><a rel=\"noopener\" href=\"https://eba.europa.eu/regulation-and-policy/payment-services-and-electronic-money/regulatory-technical-standards-on-strong-customer-authentication-and-secure-communication-under-psd2\" target=\"_blank\">https://eba.europa.eu/regulation-and-policy/payment-services-and-electronic-money/regulatory-technical-standards-on-strong-customer-authentication-and-secure-communication-under-psd2</a></p>"
  }
]]]></questionAnswerSet>
</fAQSet>