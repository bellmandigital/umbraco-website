﻿<?xml version="1.0" encoding="utf-8"?>
<fAQSet guid="9ed2b2c9-ea54-4477-a991-2910cadab9fb" id="2249" nodeName="Visa Secure (formerly known as Verified by Visa)" isDoc="" updated="2020-02-11T16:18:07.6800000Z" parentGUID="cdf0b4d6-152c-42e0-acf2-6c1ea93be269" nodeTypeAlias="fAQSet" templateAlias="" sortOrder="16" published="true" isBlueprint="false">
  <questionAnswerSet><![CDATA[[
  {
    "key": "a4d44fc9-1889-428b-9d93-75266587dc03",
    "name": "What is Visa Secure?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What is Visa Secure?",
    "answer": "<p><a data-udi=\"umb://document/006e0c25ad0d4aafa347ef8aafc713f0\" href=\"/{localLink:umb://document/006e0c25ad0d4aafa347ef8aafc713f0}\" title=\"Visa Secure (formerly known as Verified by Visa)\">Visa Secure</a> is a program that helps confirm your identity when you make an online purchase using your debit card. There’s no need to download anything or register your card. This service will automatically work at checkout at any of Visa’s participating online merchants that display the Visa Secure badge. During an online purchase you may be prompted at checkout to verify your identity by entering a one-time passcode that is sent to the mobile number you have registered with the Bank. This extra authentication step is designed to protect you from fraud. The Visa logo will be displayed to give you peace of mind.</p>\n<p> </p>\n<p> </p>"
  },
  {
    "key": "3cb50033-42aa-4876-9dd4-66f87c0db85e",
    "name": "What is One-Time Passcode (OTP)?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What is One-Time Passcode (OTP)?",
    "answer": "<p>OTP is a security feature service for online transactions. When you are transacting online where the merchant site requires you to provide an OTP, it will be sent to your mobile phone via SMS. You must then enter the unique OTP to complete the transaction. This service applies only to debit card holders whose mobile phone number is registered in the Cynergy Bank system.</p>"
  },
  {
    "key": "deec8797-f844-41d3-8ee0-41a9fcde0c08",
    "name": "What are the benefits of One-Time Passcodes (OTP)?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What are the benefits of One-Time Passcodes (OTP)?",
    "answer": "<p>OTP provides increased security as it requires you to go through an authentication process when you are making an online transaction using your debit card with a merchant online that has a Visa Secure logo.</p>"
  },
  {
    "key": "86841c26-f1f9-495a-8a30-2525ab18207f",
    "name": "Do I need to register my debit card to enable me to use this service?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Do I need to register my debit card to enable me to use this service?",
    "answer": "<p>Registration for Visa Secure is not required. However, this service applies only to debit cardholders whose mobile phone number is already registered in the Cynergy Bank system.</p>"
  },
  {
    "key": "b2dc2991-c4da-427e-8b54-88a0932b6e6d",
    "name": "Are One-Time Passcodes (OTP) required for all transactions with merchants online?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Are One-Time Passcodes (OTP) required for all transactions with merchants online?",
    "answer": "<p>No, only those online merchants who participate and have the Visa Secure logo may ask you to enter the OTP to complete a transaction.</p>"
  },
  {
    "key": "4c901101-5744-44f4-8ca3-7f6c04ae82b3",
    "name": "What do I do if I do not have a mobile phone number registered for my debit card or need to update my mobile phone number?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What do I do if I do not have a mobile phone number registered for my debit card or need to update my mobile phone number? ",
    "answer": "<p>Please send us a Secure Message from Online Banking with the details or call Customer Service on 0345 850 5555 (+44 20 3375 6422 if outside the UK) between 8.00am and 6.00pm Monday to Friday. Calls may be recorded for monitoring and training.</p>"
  },
  {
    "key": "034b552a-f5dd-4361-9c42-bd35b3de38c6",
    "name": "What should I do if I have not received the One-Time Passcode?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What should I do if I have not received the One-Time Passcode?",
    "answer": "<p>If you do not receive the SMS with your OTP, you can click \"Send new code\" to request a new OTP.</p>"
  },
  {
    "key": "f85aeb53-71ed-49cb-8da9-4c3d8e978480",
    "name": "What will happen if I have wrongly entered my One-Time Passcode (OTP) several times?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What will happen if I have wrongly entered my One-Time Passcode (OTP) several times?",
    "answer": "<p>You may make three attempts to enter your OTP. However, if you still enter the wrong OTP after the third attempt, you can request a new OTP by selecting “Send new code”.</p>"
  },
  {
    "key": "2e13cd42-fe1c-423a-b89d-2964d0cbb40f",
    "name": "Can I receive the One-Time Passcode (OTP) when I am overseas?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Can I receive the One-Time Passcode (OTP) when I am overseas?",
    "answer": "<p>Yes, we will send the OTP to your mobile phone number already registered in the Cynergy Bank system. When you are overseas and using an overseas mobile service provider, please make sure your mobile phone is able to receive international SMS which also means your mobile phone can receive the OTP.</p>"
  }
]]]></questionAnswerSet>
</fAQSet>