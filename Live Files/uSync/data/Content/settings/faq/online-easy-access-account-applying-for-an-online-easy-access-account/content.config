﻿<?xml version="1.0" encoding="utf-8"?>
<fAQSet guid="5cdbc1b0-001e-439f-9402-f1ea9404f3e6" id="1612" nodeName="Online Easy Access Account: Applying for an Online Easy Access Account" isDoc="" updated="2020-02-11T16:18:06.7470000Z" parentGUID="cdf0b4d6-152c-42e0-acf2-6c1ea93be269" nodeTypeAlias="fAQSet" templateAlias="" sortOrder="2" published="true" isBlueprint="false">
  <questionAnswerSet><![CDATA[[
  {
    "key": "01f99d89-ea4f-4f3b-b2d3-c8e725a685b1",
    "name": "Once I have submitted my application, when will my Online Easy Access account be opened?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Once I have submitted my application, when will my Online Easy Access account be opened?",
    "answer": "<p>If we are able to confirm your identity electronically, we will open your account within a few minutes of receiving your fully completed online application. We will also provide you with your account number and sort code which you will be able to use immediately to make your deposit(s). If we have not been able to confirm your identity electronically, we will email you to let you know if we need more information to confirm your identity. Once we have received any additional information we will process your application and confirm to you by email once your account has been opened. If application is successful you will also receive your Online Banking User ID by email and your log in passcode by post.</p>"
  },
  {
    "key": "af6f214c-aeea-4ad1-b3e1-ed6e2fae72ab",
    "name": "What are the eligibility requirements to open an Online Easy Access Account?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What are the eligibility requirements to open an Online Easy Access Account?",
    "answer": "<p>The Online Easy Access Account is for UK individuals only with the following eligibility requirements:</p>\n<ul>\n<li>Customers must: \n<ul>\n<li>be a UK resident</li>\n<li>be aged 18 years or over</li>\n</ul>\n</li>\n</ul>\n<ul>\n<li>Customers must also be able to provide:\n<ul>\n<li>details of a UK account (your nominated account) that accepts electronic transfers by Faster Payment and CHAPS - all external withdrawals will be paid here</li>\n<li>a National Insurance number or UK Tax ID number</li>\n<li>a valid email address</li>\n<li>a valid UK mobile phone number</li>\n</ul>\n</li>\n</ul>\n<p>The account is not available to international customers and businesses.</p>"
  },
  {
    "key": "6569207f-d074-4077-9015-ed4d26eb4540",
    "name": "Can you open a joint Online Easy Access Account?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Can you open a joint Online Easy Access Account?",
    "answer": "<p><span>Yes. A maximum of 2 account holders are able to operate an Online Easy Access Account. Both parties to a joint account will need to complete the online application. </span></p>"
  },
  {
    "key": "1a92793b-aa72-447a-9915-45630af44eee",
    "name": "Can I apply for an Online Easy Access account under a power of attorney?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Can I apply for an Online Easy Access account under a power of attorney?",
    "answer": "<p><span>Yes. You can apply for an Online Easy Access Account under a power of attorney using the online application. We can accept Lasting Power of Attorney for financial decisions, Enduring Power of Attorney made and signed before October 1, 2007 and Ordinary Power of Attorney documents.</span><span></span></p>\n<p><span>All attorneys are required to complete a Third Party Agreement form and may be required to provide certified identification documents.</span></p>\n<p>Please note:  For power of attorney applications we need to complete additional checks. We will endeavour to complete these as soon as possible however as a result it may take up to 15 days to open the account. When the account is open we will email you to let you know.</p>"
  },
  {
    "key": "38f2284b-85ef-4848-86b1-efb15ec85c00",
    "name": "If I hold an Online Easy Access Account can I open another?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "If I hold an Online Easy Access Account can I open another?",
    "answer": "<p>Yes, existing customers can open additional Online Easy Access Accounts.</p>\n<p>To do this Log into Online Banking and select Fast Track apply. As we hold your details the application should only takes a couple of minutes. Once your account is open you can fund it by:</p>\n<ul>\n<li>Transferring savings into it from an external account</li>\n</ul>\n<p>Using your new account details, (account number and sort code can be found in the ‘Account Detail’ section in online Banking), log into your external account and transfer your savings to us using their online banking.</p>\n<ul>\n<li>Transferring savings across from an existing Cynergy Bank account</li>\n</ul>\n<p>In Online Banking you can request to transfer savings from an existing account into your new Online Easy Access Account. Once requested this will be implemented immediately. </p>"
  }
]]]></questionAnswerSet>
</fAQSet>