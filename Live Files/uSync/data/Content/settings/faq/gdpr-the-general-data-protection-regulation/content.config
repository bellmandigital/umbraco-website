﻿<?xml version="1.0" encoding="utf-8"?>
<fAQSet guid="dcda9f37-1db8-4b18-99ee-d271a911f73e" id="2250" nodeName="GDPR: The General Data Protection Regulation" isDoc="" updated="2020-02-11T16:18:07.7430000Z" parentGUID="cdf0b4d6-152c-42e0-acf2-6c1ea93be269" nodeTypeAlias="fAQSet" templateAlias="" sortOrder="17" published="true" isBlueprint="false">
  <questionAnswerSet><![CDATA[[
  {
    "key": "34b38a38-3de9-4cb6-8639-3b26c73f5213",
    "name": "What is GDPR?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What is GDPR?",
    "answer": "<p>The EU General Data Protection Regulation (“GDPR”) is a new comprehensive data protection law that updates existing EU laws to strengthen the protection of “personal data” (any information relating to an identified or identifiable natural person, so called “data subjects”) in light of rapid technological developments, the increasingly global nature of business and more complex international flows of personal data. It replaces the current patchwork of national data protection laws with a single set of rules, directly enforceable in each EU member state.</p>"
  },
  {
    "key": "9f71e047-456c-48cb-9af1-40d76e628673",
    "name": "When will the GDPR come into effect?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "When will the GDPR come into effect?",
    "answer": "<p>The GDPR was approved by the EU Parliament on April 14 2016 and came into effect on May 25 2018.</p>"
  },
  {
    "key": "c3d4808b-7ff6-44a7-ae1e-59d372cccae9",
    "name": "Who does the GDPR affect?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Who does the GDPR affect?",
    "answer": "<p>The new legal framework mainly affects businesses offering goods or services or performs monitoring of EU-based individuals, be it these are customers, prospects, contractors or employees. It also affects any businesses located outside the EU, which hold or process personal data of individuals residing within the EU.</p>"
  },
  {
    "key": "84f6bc73-2630-49c7-b3cc-8522e7cb16f0",
    "name": "What are the main changes in the GDPR?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What are the main changes in the GDPR?",
    "answer": "<p>The main changes in the GDPR are:</p>\n<ul>\n<li>The legislation is now technology neutral, so it applies to personal data held in any format whether paper or electronic, held in files, on laptops, phones, audio recordings, etc</li>\n<li>The definition of Personal Data has changed to include location data and online identifiers</li>\n<li>The definition of Sensitive Personal Data has been extended to include genetic and biometric data but only for the purpose of uniquely identifying a living individual</li>\n<li>The term Sensitive Personal Data itself changes to Sensitive Category Data (SCD) that data subject rights are extended and improved</li>\n<li>The requirement to know and state – in fair processing notices – the lawful basis for all types of processing of personal and sensitive personal data, and for this to be made clear at all times.</li>\n<li>The introduction of compulsory data breach notification</li>\n<li>Increased fines for data, and notification, breaches</li>\n<li>The requirement for transparency and accountability</li>\n<li>Increased responsibility of data processors for data processing</li>\n</ul>"
  },
  {
    "key": "32188066-8e63-43e8-9972-b5e31126b998",
    "name": "Will we still have this Regulation after the UK leaves the EU?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Will we still have this Regulation after the UK leaves the EU?",
    "answer": "<p>We will still keep this regulation once the UK leaves the EU for two reasons:</p>\n<ul>\n<li>this regulation comes into effect before we leave the EU so we need to implement it for the time we are in the EU, and</li>\n<li>when the UK is no longer part of the EU it will be necessary for the UK to prove that our standards for processing personal data are at least as good as those throughout the EU, for the remaining countries to be able to transfer data to the UK. One way of proving the UK data processing standards is to retain the basis of the EU Regulation on which all other member states are using to regulate data processing. The Data Protection Bill 2017 is currently going through the Parliamentary process.</li>\n</ul>"
  },
  {
    "key": "30d7e0e1-53ab-4fb8-8542-a94d3c14040d",
    "name": "What is the difference between a data processor and a data controller?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What is the difference between a data processor and a data controller?",
    "answer": "<p>A controller is the entity that determines the purposes, conditions and means of the processing of personal data. The controller is the one who collects the data from the data subject.</p>\n<p>The processor is an entity which processes personal data on behalf or upon the request of the controller.</p>\n<p>As a controller, the GDPR places further obligations on you to ensure your contracts with processors comply with the GDPR.</p>\n<p>For example, Cynergy Bank is a controller while an external vendor of the bank, such as an IT company, is a processor.</p>"
  },
  {
    "key": "81b40efb-2c07-4671-a4bb-07d49db2d558",
    "name": "What we mean by personal data and special categories of personal data?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What we mean by personal data and special categories of personal data?",
    "answer": "<p>Personal data are any information relating to an individual, whether it relates to his or her private, professional or public life. It can be a name, an address, a telephone number, an email address, bank details, or an IP address or a combination of them.</p>\n<p>Special categories of personal data, also known as sensitive category data, which uniquely identify a person, are classified in the GDPR as sensitive data, like genetic and biometric information. Sensitive data are under very strict processing restrictions, like the stricter handling of that data such as the need to provide explicit consent.</p>"
  },
  {
    "key": "8565e666-1f20-4efd-be80-360917ef2a20",
    "name": "What does \"processing\" mean?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What does \"processing\" mean?",
    "answer": "<p><span>Processing means anything that is done to, or with, personal data (including simply collecting, storing or deleting those data). This definition is significant because it clarifies that the EU data protection law is likely to apply wherever an organisation does anything that involves or affects personal data.</span></p>"
  },
  {
    "key": "7d71d0a0-cd77-49c7-bf26-b332140aa8ca",
    "name": "What is a Privacy Policy?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What is a Privacy Policy?",
    "answer": "<p>If an organisation holds information on individuals, they must provide a detailed explanation on these, such as information they hold on them, how their data is processed and where it is kept. This can be done through a privacy statement or notice which should be made publicly available. The GDPR accordingly states that this statement should be clear, easy to access and free of charge.</p>\n<p>The Privacy policy for the Cynergy Bank can be found <a data-udi=\"umb://document/12f8030f3a974b0fb98f160e05c63e14\" href=\"/{localLink:umb://document/12f8030f3a974b0fb98f160e05c63e14}\" title=\"Privacy Policy\">here.</a></p>"
  },
  {
    "key": "f6c6aed0-b2f8-47b7-a913-9297e9db1e1c",
    "name": "What are the rules on security under the GDPR?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "What are the rules on security under the GDPR?",
    "answer": "<p>GDPR safeguards personal data by ensuring they are processed in a manner that ensures their security, including protection against unauthorised or unlawful processing as well as against accidental loss, destruction or damage. Organisations should have appropriate technical or organisational measures in place to prevent such personal data leaks or unlawful processing.</p>"
  },
  {
    "key": "95370642-f398-4f74-9df9-792de429ca5a",
    "name": "Do we have a Data Protection Officer (DPO)?",
    "ncContentTypeAlias": "fAQQuestionAnswer",
    "question": "Do we have a Data Protection Officer (DPO)?",
    "answer": "<p>Organisations are required to appoint a Data Protection Officer (DPO) if its main activities involve the processing of personal data on a large scale and/or involve continuous monitoring of personal data.<br /><br />Our Data Protection Officer (DPO) can be contacted at <a rel=\"noopener\" href=\"mailto:dpo@cynergybank.co.uk\" target=\"_blank\" title=\"mailto:dpo@cynergybank.co.uk\">dpo@cynergybank.co.uk</a></p>"
  }
]]]></questionAnswerSet>
</fAQSet>