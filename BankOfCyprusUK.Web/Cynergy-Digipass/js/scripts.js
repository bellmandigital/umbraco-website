//Pre load images
    $.preloadImages = function() {
      for (var i = 0; i < arguments.length; i++) {
        $("<img />").attr("src", arguments[i]);
      }
    }

    $.preloadImages("animations/introduction.gif","animations/step-1.gif","animations/step-2.gif","animations/step-3.gif","animations/step-4.gif","animations/step-5.gif","animations/completed.png");
        
    
    $( '.slide-1').fadeIn();
    $( ".rgBack" ).hide
    
    $("footer button").click(function(){
      nextSlide();
    });

    $(".start").click(function(){
      nextSlide();
    });
    
    $(".rgBack").click(function(){
      prevSlide();
    });
        
    $('#restart').click(function() {
        window.location.reload(true);
    });
        
    var slideNumber = 1;
     
    function nextSlide() {
        slideNumber = slideNumber+1;
        $("div[class*='slide']").hide();  
        $( '.slide-'+slideNumber).fadeIn();
        backBtn();
        needCycle();
        loadImg();
    }
        
    function prevSlide() {
        slideNumber = slideNumber-1;
        $("div[class*='slide']").hide(); 
        $( '.slide-'+slideNumber).fadeIn();
        backBtn();
        needCycle();
        loadImg();
        
    }
        
    function loadImg() {
        if (slideNumber == 2) {
            $('.animation img').attr('src','animations/introduction.gif');
        } else if (slideNumber == 5) {
            $('.animation img').attr('src','animations/step-1.gif');
        } else if (slideNumber == 6) {
            $('.animation img').attr('src','animations/step-2.gif');
        } else if (slideNumber == 7) {
            $('.animation img').attr('src','animations/step-3.gif');
        } else if (slideNumber == 8) {
            $('.animation img').attr('src','animations/step-4.gif');
        } else if (slideNumber == 9) {
            $('.animation img').attr('src','animations/step-5.gif');
        } else if (slideNumber == 10) {
            $('.animation img').attr('src','animations/completed.png');
        }    
    }       

    function backBtn() {
        if (slideNumber < 2) {
            $(".rgBack").addClass("hide");
            $(".rgBack").removeClass("show");
        } else {
            $(".rgBack").addClass("show");
            $(".rgBack").removeClass("hide");
        }
    }
        
    function needCycle() {
        if (slideNumber == 5) {
            $('.step-5-container > p').fadeLoop();
    }
        
	(function($) {
        var default_config = {
            fadeIn: 1000,
            stay: 5000,
            fadeOut: 1000
        };

        function fade(index, $elements, config) {
            $elements.eq(index)
              .fadeIn(config.fadeIn)
              .delay(config.stay)
              .fadeOut(config.fadeOut, function() {
                  fade((index + 1) % $elements.length, $elements, config);
              });
        }

        $.fn.fadeLoop = function(config) {     
            fade(0, this, $.extend({}, default_config, config));
            return this;
        };

    }(jQuery));    
    }