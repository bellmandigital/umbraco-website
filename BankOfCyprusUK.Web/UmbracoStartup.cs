﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using System.Xml.Linq;
using BankOfCyprusUK.Web.Code;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence;
using Umbraco.Core.Publishing;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace BankOfCyprusUK.Web
{

	public class UmbracoStartup : ApplicationEventHandler
	{
	
		protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
		{
			base.ApplicationStarted(umbracoApplication, applicationContext);
            
			RouteTable.Routes.MapRoute(
				name: "MainSurface",
				url: "MainSurface/{action}/{id}",
				defaults: new { controller = "MainSurface", action = "Index", id = UrlParameter.Optional }
			);
			RouteTable.Routes.MapRoute(
				"RatesCache",
				"RatesCache/{action}/{id}",
				new
				{
					controller = "RatesCache",
					action = "Refresh",
					id = UrlParameter.Optional
				});

			ContentService.Created += ContentService_Created;
			ContentService.Published += ContentService_Published;
			RatesManager.RefreshCache();
			
		}


		XNamespace _xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";
		private void ContentService_Published(IPublishingStrategy sender, PublishEventArgs<IContent> e)
		{
			UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);
			var list = new List<IPublishedContent>();

			GetPublishedNodes(helper.TypedContentAtRoot().Where(n => n.IsVisible()), list, false);

			var path = HostingEnvironment.MapPath("~/sitemap.xml");
			if (System.IO.File.Exists(path)) System.IO.File.Delete(path);


			XDocument doc = new XDocument(
				new XDeclaration("1.0", "utf-8", "yes"),
				new XElement(_xmlns + "urlset",
					new XAttribute(XNamespace.Xmlns + "xsd", "http://www.w3.org/2001/XMLSchema"),
					new XAttribute(XNamespace.Xmlns + "xsi", "http://www.w3.org/2001/XMLSchema-instance")));
			//XNamespace ns1 = "http://www.sitemaps.org/schemas/sitemap/0.9";
			//XNamespace ns2 = "http://www.w3.org/2001/XMLSchema-instance";
			//XNamespace ns3 = "http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd";

			var root = doc.FirstNode as XElement;

			foreach (var content in list)
			{

				var url = content.Url.ToAbsoluteUrl().EnsureEndsWith('/');
				XElement locNode = new XElement(_xmlns + "url", new XElement(_xmlns + "loc", url));
				root.Add(locNode);
			}

			doc.Save(path);
		}

		private void GetPublishedNodes(IEnumerable<IPublishedContent> nodes, IList<IPublishedContent> list, bool getExcluded)
		{
			var excludedDt = new List<string>
			{
				"fAQSet",
				"fAQFolder",
				"meta",
				"carouselPageHeader",
				"singleImagePageHeader",
				"sectionThreeColumnFeatureRow",
				"sectionGreyFeatureRow",
				"sectionIconFeatureRow",
				"sectionImageFeatureRow",
				"linkFolder",
				"singleImagePromoBanner",
				"link",
				"teamMember",
				"team",
				"freetextTab",
				"documentsTab",
				"ratesTab",
				"baseTab",
				"fAQTab",
				"ratesTable",
				"ratesList",
				"tag",
				"tagFolder",
                "content",
                "pageContent"
			};
			var items = nodes.Where(n => !excludedDt.Contains(n.DocumentTypeAlias));

			//items = getExcluded ? items.Where(n => n.GetPropertyValue<int>("excludeFromSitemap") == 1) : items.Where(n => n.GetPropertyValue<int>("excludeFromSitemap") != 1);
			foreach (var item in items)
			{
				if (getExcluded && item.GetPropertyValue<int>("excludeFromSitemap") == 1) list.Add(item);
				else if (!getExcluded && item.GetPropertyValue<int>("excludeFromSitemap") != 1) list.Add(item);

				GetPublishedNodes(item.Children, list, getExcluded);
			}
		}

		void ContentService_Created(IContentService sender, Umbraco.Core.Events.NewEventArgs<Umbraco.Core.Models.IContent> e)
		{

		}
	}
}