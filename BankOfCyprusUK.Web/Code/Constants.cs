﻿namespace BankOfCyprusUK.Web.Code
{
    public static class Constants
    {
        public static int OverdraftCalculatorNodeId => ConfigHelper.GetConfigSetting<int>("Umbraco.OverdraftCalculatorNodeId");

        public static int OverdraftCalculatorAprilNodeId => ConfigHelper.GetConfigSetting<int>("Umbraco.OverdraftCalculatorAprilNodeId");

        public static string BaseRateAlias => "baseRate";

        public static string AuthRateAlias => "authorisedOverdraftMarginAboveBaseApplied";

        public static string UnAuthRateAlias => "unauthorisedOverdraftMarginAboveBaseApplied";
    }
}
