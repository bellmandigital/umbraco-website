﻿using BankOfCyprus.OverdraftCalculator.Models;
using System;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace BankOfCyprusUK.Web.Code
{
    public class CalculatorService : ICalculatorService
    {
        public decimal GetAuthorisedRate(IPublishedContent node)
        {
            return node.GetPropertyValue<decimal>(Constants.AuthRateAlias);
        }
            
        public decimal GetBaseRate(IPublishedContent node)
        {
            return node.GetPropertyValue<decimal>(Constants.BaseRateAlias);
        }

        public decimal GetUnauthorisedRate(IPublishedContent node)
        {
            return node.GetPropertyValue<decimal>(Constants.UnAuthRateAlias);
        }

        public OverdraftCalculatorResultViewModel GetResults(int arrangedOverdraft, int amountOverdrawn, int numberOfDays, decimal baseRate, decimal authorisedRate, decimal unauthorisedRate)
        {
            var isUnauthorised = false;
            decimal unArrangedOverdraftCharge = 0;
            var unauthorisedOverdraftBalance = amountOverdrawn - arrangedOverdraft;
            if (unauthorisedOverdraftBalance > 0)
                isUnauthorised = true;
          
            // Authorised Overdraft Cost
            var initialBorrowing = arrangedOverdraft;
            var interestForEARCalculation = initialBorrowing * (baseRate + authorisedRate) / 100;
  
            //var totalEARAPRPercentage = (interestForEARCalculation / initialBorrowing) * 100; // TODO: IS THIS USED?

            // Total Charge For Credit To Client
            var annualChargeForCredit = interestForEARCalculation;
            var dailyChargeForCredit = annualChargeForCredit / 365;

            var totalChargeForCredit = numberOfDays * dailyChargeForCredit;


            // Set the cost of borrowing
            decimal costOfBorrowing = totalChargeForCredit;


            // Unauthorised Overdraft Cost
            if (isUnauthorised)
            {             
                var unauthorisedInterestForEARCalculation = unauthorisedOverdraftBalance * (baseRate + unauthorisedRate) / 100;

                // Total Charge For Credit To Client
                var unauthorisedAnnualChargeForCredit = unauthorisedInterestForEARCalculation;
                var unauthorisedDailyChargeForCredit = unauthorisedAnnualChargeForCredit / 365;
                var unauthorisedTotalChargeForCredit = numberOfDays * unauthorisedDailyChargeForCredit;

                unArrangedOverdraftCharge = unauthorisedTotalChargeForCredit;
                costOfBorrowing += unauthorisedTotalChargeForCredit;
            }

            costOfBorrowing = Math.Round(costOfBorrowing, 2);
            unArrangedOverdraftCharge = Math.Round(unArrangedOverdraftCharge, 2);
            totalChargeForCredit = Math.Round(totalChargeForCredit, 2);

            var resultModel = new OverdraftCalculatorResultViewModel
            {
                IsUnauthorised = isUnauthorised,
                NumberOfDays = numberOfDays,
                ArrangedOverdraftAmount = arrangedOverdraft,
                ChargedArrangeOverdraftInterestEAR = totalChargeForCredit,
                UnArrangedOverdraftCharge = unArrangedOverdraftCharge,
                CostOfBorrowing = costOfBorrowing
            };

            return resultModel;                 
        }
    }
}