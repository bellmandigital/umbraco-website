﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Persistence;

namespace BankOfCyprusUK.Web.Code
{
	public class SearchAndReplaceRatesStream : Stream
	{
		private Stream _base;
		private MemoryStream _memoryBase = new MemoryStream();

		public SearchAndReplaceRatesStream(Stream stream)
		{
			_base = stream;
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotImplementedException();
		}

		public override void SetLength(long value)
		{
			throw new NotImplementedException();
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			throw new NotImplementedException();
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			_memoryBase.Write(buffer, offset, count);
		}

		public override bool CanRead { get; }
		public override bool CanSeek { get; }
		public override bool CanWrite { get; }
		public override long Length { get; }
		public override long Position { get; set; }

		public override void Flush()
		{
			// Get static HTML code
			string html = Encoding.UTF8.GetString(_memoryBase.GetBuffer());

			// Modify your HTML
			// Sample: Replace absolute links to relative
			Regex regex = new Regex(@"\[\!([^]]*)\]");
			Match match = regex.Match(html);
			while (match.Success)
			{
				string token = match.Value;
				string newValue = GetRate(token);
				html = html.Replace(token, newValue);

				match = match.NextMatch();
			}

			// Flush modified HTML
			byte[] buffer = Encoding.UTF8.GetBytes(html);
			_base.Write(buffer, 0, buffer.Length);
			_base.Flush();
		}

		private string GetRate(string token)
		{
			IList<RatesTableModel> rates = RatesManager.Rates;

			var parts = token.Replace("[!", "").Replace("]", "").Split(',');
			var key = parts[0];
			var subkey = parts[1];
			var rate = rates.OrderByDescending(n => n.RecordActiveSince)
				.FirstOrDefault(n => n.DataLookupKey == key && n.DataLookupSubKey == subkey);

			return rate == null ? token : rate.DataLookupP1_Out;
		}
	}
}