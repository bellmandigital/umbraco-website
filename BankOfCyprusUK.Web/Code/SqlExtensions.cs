﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Umbraco.Core.Persistence;

namespace BankOfCyprusUK.Web.Code
{
	public static class SqlExtensions
	{
		public static Sql Select<TColumn>(this Sql sql, params Expression<Func<TColumn, object>>[] columnMember)
		{
			var columns = columnMember.Select(s => s.Body.NodeType == ExpressionType.Convert ?
				((MemberExpression)((UnaryExpression)s.Body).Operand).Member.Name :
				((MemberExpression)s.Body).Member.Name
			).ToArray();

			return sql.Select(columns);
		}

		public static Sql AndWhere(this Sql self, string sql, ref bool hasWhere, params object[] args)
		{
			if (hasWhere)
				return self.Append("AND (" + sql + ")", args);
			hasWhere = true;
			return self.Where(sql, args);
		}

		public static Sql OrWhere(this Sql self, string sql, ref bool hasWhere, params object[] args)
		{
			if (hasWhere)
				return self.Append("OR (" + sql + ")", args);
			hasWhere = true;
			return self.Where(sql, args);
		}
	}
}