﻿using System;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace BankOfCyprusUK.Web.Code
{
	[TableName("WbsteDtaRfrnceTble")]
	[PrimaryKey("uid")]
	public class RatesTableModel
	{
		[PrimaryKeyColumn]
		public Guid uid { get; set; }

		[NullSetting(NullSetting = NullSettings.NotNull)]
		public long Id { get; set; }

		[NullSetting(NullSetting = NullSettings.NotNull)]
		public string DataLookupKey { get; set; }

		[NullSetting(NullSetting = NullSettings.NotNull)]
		public string DataLookupSubKey { get; set; }

		[NullSetting(NullSetting = NullSettings.NotNull)]
		public int RowNumber { get; set; }

		[NullSetting(NullSetting = NullSettings.NotNull)]
		public DateTime RecordActiveSince { get; set; }

		[NullSetting(NullSetting = NullSettings.Null)]
		public string DataLookupP1_Out { get; set; }

		[NullSetting(NullSetting = NullSettings.Null)]
		public string DataLookupP1_DB { get; set; }

		[NullSetting(NullSetting = NullSettings.Null)]
		public string DataLookupP1_Desc { get; set; }

		//[Column("CompetitionDescription")]
		//[NullSetting(NullSetting = NullSettings.Null)]
		//[SpecialDbType(SpecialDbTypes.NTEXT)]
		//public string Description { get; set; }

	}
}