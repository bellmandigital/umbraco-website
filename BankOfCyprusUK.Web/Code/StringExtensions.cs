﻿using System;
using System.Web;

namespace BankOfCyprusUK.Web.Code
{
	public static class StringExtensions
	{
		public static string Truncate(this string value, int maxChars)
		{
			return value.Length <= maxChars ? value : value.Substring(0, maxChars) + "...";
		}

		public static string ToAbsoluteUrl(this string relativeUrl)
		{
			try
			{
				if (string.IsNullOrEmpty(relativeUrl))
					return relativeUrl;

				if (HttpContext.Current == null)
					return relativeUrl;

				if (relativeUrl.StartsWith("/"))
					relativeUrl = relativeUrl.Insert(0, "~");
				if (!relativeUrl.StartsWith("~/"))
					relativeUrl = relativeUrl.Insert(0, "~/");

				var url = HttpContext.Current.Request.Url;
				var port = url.Port != 80 ? (":" + url.Port) : String.Empty;

				return String.Format("{0}://{1}{2}{3}", url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl));
			}
			catch
			{
				return relativeUrl;
			}
		}
	}
}