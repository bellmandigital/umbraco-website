﻿using BankOfCyprus.OverdraftCalculator.Models;
using Umbraco.Core.Models;

namespace BankOfCyprusUK.Web.Code
{
    public interface ICalculatorService
    {
        decimal GetBaseRate(IPublishedContent calculatorNode);
        decimal GetAuthorisedRate(IPublishedContent calculatorNode);
        decimal GetUnauthorisedRate(IPublishedContent calculatorNode);
        OverdraftCalculatorResultViewModel GetResults(int arrangedOverdraft, int amountOverdrawn, int numberOfDays, decimal baseRate, decimal authorisedRate, decimal unauthorisedRate);
    }
}
