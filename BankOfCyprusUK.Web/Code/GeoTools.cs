﻿using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Hosting;
using MaxMind.Db;

namespace BankOfCyprusUK.Web.Code
{
	public class GeoTools
	{
		public static bool IsInternationalRequest(HttpRequestBase request)
		{
			if (request.IsLocal) return false;

			var ip = GetIPAddress(request);
			var db = HostingEnvironment.MapPath("~/app_data/GeoLite2-Country.mmdb");

			using (var reader = new Reader(db))
			{
				var data = reader.Find<Dictionary<string, object>>(IPAddress.Parse(ip));
				if (data == null) return false;
				if (data["country"] is IDictionary<string, object>)
				{
					var list = data["country"] as IDictionary<string, object>;
					return list["iso_code"].ToString() != "GB";
				}
				return false;
			}
		}

		public static string GetIPAddress(HttpRequestBase request)
		{
			string ipAddress = request.ServerVariables["HTTP_X_FORWARDED_FOR"];

			if (!string.IsNullOrEmpty(ipAddress))
			{
				string[] addresses = ipAddress.Split(',');
				if (addresses.Length != 0)
				{
					return addresses[0];
				}
			}

			return request.UserHostAddress;
		}
	}
}