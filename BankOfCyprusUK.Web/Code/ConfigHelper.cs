﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankOfCyprusUK.Web.Code
{
    public class ConfigHelper
    {
        public static T GetConfigSetting<T>(string key)
        {
            var type = typeof(T);
            var setting = ConfigurationManager.AppSettings[key];

            switch (type.Name.ToLower())
            {
                case "double":
                case "decimal":
                    double retDouble;
                    double.TryParse(setting, out retDouble);
                    return (T)(object)retDouble;
                case "int32":
                    int retInt;
                    int.TryParse(setting, out retInt);
                    return (T)(object)retInt;
                case "string":
                    return (T)(object)setting;
                case "datetime":
                    DateTime retDt;
                    DateTime.TryParse(setting, out retDt);
                    return (T)(object)retDt;
                default:
                    return default(T);
            }
        }
    }
}
