﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankOfCyprusUK.Web.Code
{
    public class TradeDoublerModule : IHttpModule
    {
        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += new EventHandler(Request);
        }

        private void Request(object sender, EventArgs e)
        {
            if(HttpContext.Current != null)
            {
                var tduid = HttpContext.Current.Request.QueryString["tduid"];
                var redirectUrl = HttpContext.Current.Request.QueryString["url"];
                //store the value if exists for 24 hours
                if(tduid != null)
                {
                    HttpCookie tdCookie = new HttpCookie("TD_Cookie");
                    tdCookie.Expires = DateTime.Now.AddHours(24);
                    tdCookie.Value = tduid;
                    HttpContext.Current.Response.Cookies.Add(tdCookie);
                }
                //redirect to page if redirect exists
                if(redirectUrl != null)
                {
                    HttpContext.Current.Response.Redirect(redirectUrl);
                }
            }

        }
    }
}