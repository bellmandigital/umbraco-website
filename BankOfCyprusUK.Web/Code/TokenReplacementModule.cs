﻿using System;
using System.Diagnostics;
using System.Web;
using Umbraco.Web;

namespace BankOfCyprusUK.Web.Code
{
	public class TokenReplacementModule : IHttpModule
	{

		/// <summary>
		/// You will need to configure this module in the Web.config file of your
		/// web and register it with IIS before being able to use it. For more information
		/// see the following link: https://go.microsoft.com/?linkid=8101007
		/// </summary>
		#region IHttpModule Members

		public void Dispose()
		{
			//clean-up code here.
		}

		public void Init(HttpApplication context)
		{
			// Below is an example of how you can handle LogRequest event and provide 
			// custom logging implementation for it
			context.LogRequest += new EventHandler(OnLogRequest); 
			
			//context.BeginRequest += (o, e) =>
			//{
			//	context.Response.Filter = new SearchAndReplaceRatesStream(context.Response.Filter);
			//};

			context.PostRequestHandlerExecute += new EventHandler(OnEndRequest);
		}

		private void OnEndRequest(object sender, EventArgs e)
		{
			

			if (HttpContext.Current.Response.ContentType.Contains("html")
			    && !HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains(".css")
			    && !HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains(".js")
			    && !HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains(".png")
			    && !HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains(".jpg")
			    && !HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains(".gif")
			    && !HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains(".ico"))
			{
                if (UmbracoContext.Current == null || HttpContext.Current.Response.Filter == null)
                {
                    return;
                }

                if (!UmbracoContext.Current.IsFrontEndUmbracoRequest)
                {
                    if (!HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains("/main"))
                    {
                        return;
                    }
                }
				Debug.WriteLine("Filtered Request - " + HttpContext.Current.Request.Url.ToString());
				HttpContext.Current.Response.Filter = new SearchAndReplaceRatesStream(HttpContext.Current.Response.Filter);
			}
		}

		#endregion

		public void OnLogRequest(Object source, EventArgs e)
		{
			//custom logging logic can go here
		}
	}
}
