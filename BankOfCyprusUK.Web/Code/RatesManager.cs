﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using System.Web.Hosting;
using Umbraco.Core.Persistence;
using Umbraco.Web;

namespace BankOfCyprusUK.Web.Code
{
	public static class RatesManager
	{
		private static readonly MemoryCache _cache = new MemoryCache("Rates");

		private static IList<RatesTableModel> LoadRates()
		{
			var db = UmbracoContext.Current.Application.DatabaseContext.Database;

			var sql = new Sql();
			sql = sql.OrderByDescending<RatesTableModel>(n => n.RecordActiveSince);

			var result = db.Fetch<RatesTableModel>(sql);


			//db.FirstOrDefault<RatesTableModel>()

			return result;
		}

		public static IList<RatesTableModel> Rates
		{
			get
			{
				lock(_cache)
				{
					if (_cache["rates"] == null)
					{
						RefreshCache();
					}

					return _cache["rates"] as IList<RatesTableModel>;
					
				}
			}
		}

		public static void RefreshCache()
		{
			lock (_cache)
			{
				_cache.Remove("rates");
				var rates = LoadRates();

				_cache.Add("rates", rates, HostingEnvironment.IsDevelopmentEnvironment ? DateTimeOffset.Now.AddMinutes(1) : DateTimeOffset.Now.AddHours(1));
			}
		}
	}
}