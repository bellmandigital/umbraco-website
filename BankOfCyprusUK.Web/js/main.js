jQuery(document).ready(function ($) {

    // Slip link
    $(".skip-link").click(function (event) {

        // strip the leading hash and declare
        // the content we're skipping to
        var skipTo = "#" + this.href.split('#')[1];

        // Setting 'tabindex' to -1 takes an element out of normal
        // tab flow but allows it to be focused via javascript
        $(skipTo).attr('tabindex', -1).on('blur focusout', function () {

            // when focus leaves this element,
            // remove the tabindex attribute
            $(this).removeAttr('tabindex');

        }).focus(); // focus on the content container
    });

    // Search form
    $('.search-submit').on('click', function () {
        $('.search-field').toggle();
    });

    // Mobile nav
    $('.navbar-toggle').on('click', function () {
        $('.navbar-toggle').toggleClass('open', 500);
        $('#mobile-nav').toggle();
        $('#mobile-nav').toggleClass('open', 500);
        // $('#site-wrap').toggleClass('open', 500);
        $('.blackout').delay(250).fadeToggle(500);
    });

    $('#site-wrap').on('click', function () {
        if ($('.navbar-toggle').hasClass('open')) {
            $('.navbar-toggle').toggleClass('open', 500);
            $('#mobile-nav').toggleClass('open', 500);
            $('#mobile-nav').toggle();
            // $('#site-wrap').toggleClass('open', 500);
            $('.blackout').delay(250).fadeToggle(500);
        }
    });

    $('.dropdown-submenu .toggle').on("click", function (e) {
        $(this).next('ul').toggle();
        $(this).toggleClass('open');
        e.stopPropagation();
        e.preventDefault();
    });

    $('#cookie-consent').on("click", function () {
        $('.cookie-bar').slideToggle();
    });

    // Make Bootstrap tabs switch to accordian for mobile
    $('#tabs-full-width').tabCollapse();

    $('#tabs').tabCollapse({
        tabsClass: 'hidden-sm hidden-xs',
        accordionClass: 'visible-sm visible-xs'
    });

    $('#tabs-too-wide').tabCollapse({
        tabsClass: 'hidden-lg hidden-md hidden-sm hidden-xs',
        accordionClass: 'visible-lg visible-md visible-sm visible-xs'
    });

    // Add different bg for even numbered tabs / accordian sections
    $(".tabs .nav-tabs li:even").addClass('even');

    $('.tabs').on('shown-accordion.bs.tabcollapse', function () {
        // Add different bg for even numbered accordian sections
        $(".panel-group .panel:even .panel-heading").addClass('even');
    });

    $(".panel-group .panel:even .panel-heading").addClass('even');

    // FAQs nav
    $('#form-faqs').on("change", function () {
        var val = $(this).val();
        $("html, body").animate({
            scrollTop: $('#' + val).offset().top
        }, 1000);
    });

    // Interest rates tables
    var ratesTable = $('#form-rates').val();
    $('#' + ratesTable).show();

    $('#form-rates').on('change', function () {
        ratesTable = $('#form-rates').val();
        $('.interest-rates table').hide();
        $('#' + ratesTable).toggle();
    });

    //Match module height
    $(function () {
        $('.match-height').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });

        $('.alerts-carousel .text').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });

        $('.card').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });

        $('.card-title').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });

        $('.card-text').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });

        $('.card-type').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });

        $('.footer-nav').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });
    });

    // Alerts carousel
    $('.alerts-carousel').slick({
        centerMode: true,
        centerPadding: '15%',
        slidesToShow: 1,
        arrows: true,
        dots: true,
        prevArrow: '<a type="button" class="prev-alert"><img src="images/alert-left.png" /></a>',
        nextArrow: '<a type="button" class="next-alert"><img src="images/alert-right.png" /></a>',
    });

    $('.alerts-carousel').on('click', '.slick-slide', function (e) {
        e.stopPropagation();
        var index = $(this).data("slick-index");
        if ($('.slick-slider').slick('slickCurrentSlide') !== index) {
          $('.slick-slider').slick('slickGoTo', index);
        }
    });


    // Gallery
    $('[data-fancybox="gallery"]').fancybox({
        animationDuration: 600,
        thumbs : {
            autoStart : true,
            axis      : 'x'
          }
    });

    // Add swipe functionality to carousels
    $(".carousel").on("touchstart", function(event){
        var xClick = event.originalEvent.touches[0].pageX;
        $(this).one("touchmove", function(event){
            var xMove = event.originalEvent.touches[0].pageX;
            if( Math.floor(xClick - xMove) > 5 ){
                $(this).carousel('next');
            }
            else if( Math.floor(xClick - xMove) < -5 ){
                $(this).carousel('prev');
            }
        });
        $(".carousel").on("touchend", function(){
                $(this).off("touchmove");
        });
    });


    // Remove curve
    jQuery(document).ready(function($) {
      var alterClass = function() {
        var ww = document.body.clientWidth;
        if (ww < 720) {
          $('.page-header.curves').removeClass('curves');
        } else if (ww >= 721) {
          $('.page-header.blue').addClass('curves');
          $('.page-header.red').addClass('curves');
          $('.page-header.teal').addClass('curves');
          $('.page-header.orange').addClass('curves');
          $('.page-header.gold').addClass('curves');
        };
      };
      $(window).resize(function(){
        alterClass();
      });
      //Fire it when the page first loads:
      alterClass();
    });

});
