(function () {

    "use strict";

    var d = document,
        visible = false,
        main,
        tab,
        mobileTab,
        bubble,
        toggling = false,
        elements = [],
        speed,
        pos,
        go_to,
        prevHeight,
        debounceTimer,

        //configuration
        config = {

            "graceTime": .25, //how long to wait (in seconds) before showing the widget handle when the widget is ready
            "animationTime": 0.6, //how much time is spent (in seconds) animating the widget when you reveal/hide it
            "tabOffset": "320px", //distance from the top of the page to the tab; can be px or percent (eg '230px' or '5%')

            /*"logoURL": "http://www.bankofcyprus.co.uk/media/img/logo-lrg.png", //where to fetch the logo from*/
            "logoURL": "/images/logo-250px.png", //where to fetch the logo from
            /*"path": "/scripts/webwidget/res", //where to find the additional files, don't add a trailing slash*/
            "path": "/scripts/webwidget/res", //where to find the additional files, don't add a trailing slash
            "rant": "rant.png", // image name for ranting
            "rave": "rave.png", // image name for raving


            //colours used in the widget
            "colours": {
                "tabBackground": "#F9F9F9", // Background colour of the tab
                "tabBackgroundHover": "#F9F9F9", // Background colour of the tab when hovered
                "fieldBorder": "#d3d3d3", // Border of the input fields
                "fieldFocusBorder": "#00607e", // Border of the input fields when in focus
                "submitBackground1": "#003D4C",	// Submit button backgrund #f9ac18
                "submitBackground2": "", // Submit button background color 2 (for gradient); if left blank the first colour is used
                "title": "#003D4C" // Subtitle Colour #00607e
            },

            //messages strings to use
            "messaging": {
                "title": "Your opinion matters", // Title text string
                "subtitle": "How easy was it to get what you needed<br /> from Cynergy Bank?<br />", // Subtitle text string. You can use %scale% as a wildcard for the actual scale
                "scaleHint": "<br />Please rate your experience on a 1-5 scale, <br />where %scale1% is difficult and %scale2% is very easy.", // Information about the scale. You can use %scale1% and %scale2% as wildcards for bottom and top bounds.
                "thankyou": "Thank you for your feedback which will help us improve our service to you.", // Thank you message after succesfull send,
                "thankyouSub": "", //additional information for the thank you page
                "failed": "Sending failed! Please check that you are still connected to the internet.<br/><br/>We'll let you try again in a bit.", // Fail text,
                "send": "Submit", //Default button label
                "sendHint": "Submitting...", //Sending hint string on button
                "failHint": "Please wait", //Failed hint string on button

                //this is the list of errors to display
                //the message selection priority is field mapping (if existent) and then field type
                //for example CustomerName overwrites the generic text message
                "error": {
                    "CustomerName": "Please tell us your name",

                    "comment": "Please give us a reason for your score",
                    "email": "Please enter a valid email address",
                    "telephone": "Please enter a valid telephone number",
                    "text": "Oops, you forgot this field"
                }
            },
            "includeURL": "URL",

            "minScoreBound": 1, // does what it says on the box
            "maxScoreBound": 5,

            //fields to show
            //note: the mappings have to be fields existing in your project configuration, you can't add new ones dynamically; any field that doesn't exist will be ignored
            "fields": [{
                "type": "comment",
                "verbatim": "Please tell us about your experience and why you gave that score.",
                "required": true,
                "mapping": "comment"
            }, {
                "type": "text",
                "verbatim": "Name",
                "required": true,
                "mapping": "name"
            }, {
                "type": "telephone",
                "verbatim": "Mobile Number",
                "required": true,
                "mapping": "phone"
            }, {
                "type": "email",
                "verbatim": "Email Address",
                "required": true,
                "mapping": "email"
            }
            ]
        },

        //slider-related stuff
        slide = {
            "moving": false,
            "original": 0,
            "dx": 0,
            "offset": 0,
            "prev": 0,
            "start": 0,
            "max": 0,
            "moveStart": function (e) {

                if (e.stopPropagation) {
                    e.stopPropagation();
                }
                var handle = d.getElementById("rnr-widget-handle");
                var rail = d.getElementById("rnr-widget-rail");

                if (!slide.moving) {
                    slide.moving = true;
                    slide.start = (e.touches && e.touches[0].pageX) || e.pageX || e.clientX;
                    slide.max = rail.clientWidth - handle.clientWidth + 28;
                    slide.dx = slide.start - handle.offsetLeft - 14;
                    slide.offset = handle.offsetLeft;
                    slide.prev = handle.offsetLeft;

                }
            },
            "move": function (e) {
                var handle = d.getElementById("rnr-widget-handle"),
                    now,
                    at;

                if (slide.moving) {
                    now = ((e.touches && e.touches[0].pageX) || e.pageX || e.clientX) - slide.dx - slide.offset + slide.prev;

                    now = Math.max(12, Math.min(now, slide.max - 14));
                    css.call(handle, "left", now + "px");

                    at = config.minScoreBound + Math.round(((now - 12) / (slide.max - 26)) * (config.maxScoreBound - config.minScoreBound));
                    handle.innerHTML = at;

                    e.preventDefault && e.preventDefault();
                }
            },
            "end": function (e) {
                var handle = d.getElementById("rnr-widget-handle"),
                    current,
                    target,
                    distance,
                    animate;

                //if we are not on the first page anymore
                if (!handle || slide.moving === false) { return; }

                slide.moving = false;

                //animate in place
                if (go_to) {
                    clearTimeout(go_to);
                }

                current = 14 + handle.offsetLeft;
                target = 13 + (((parseInt(handle.innerHTML, 10) - config.minScoreBound) / (config.maxScoreBound - config.minScoreBound)) * (slide.max - 26));

                pos = current;
                speed = 2;

                animate = function () {
                    if (Math.abs(target - pos) > 0.1) {
                        if (pos > target) {
                            pos -= 1 * speed;
                            speed *= 0.9;
                            css.call(handle, "left", pos + "px");
                            setTimeout(animate, 1000 / 60);
                        }
                        if (pos < target) {
                            pos += 1 * speed;
                            speed *= 0.9;
                            css.call(handle, "left", pos + "px");
                            setTimeout(animate, 1000 / 60);
                        }
                    }
                }
                animate();

            }
        },

        //utility functions
        supportsInputPlaceholder = function () {
            return document.createElement('input').placeholder !== undefined;
        },
        css = function (prop, val) {
            var thing, text = "";

            if (val) {
                this.style[prop] = val;
            } else {
                for (thing in prop) {
                    if (prop.hasOwnProperty(thing)) {
                        text += thing + ":" + prop[thing] + ";";
                    }
                }
                this.style.cssText = text;
            }
        },
        bind = function (el, event, callback) {
            if (d.addEventListener) {
                return el.addEventListener(event, callback);
            } else {
                return el.attachEvent("on" + event, callback);
            }
        },
        placeholder = function (string) {
            var self = this,
                range;

            this.value = string;
            this.style.color = "#7b7e7a";

            bind(this, "focus", function () {
                if ((self.value === string) || (self.value.replace(/\s+/g, "") === "")) {
                    self.value = "";
                    self.style.color = "#7b7e7a";
                    self.value = string;
                }
            });
            bind(this, "blur", function () {
                if ((self.value === string) || (self.value.replace(/\s+/g, "") === "")) {
                    self.value = string;
                    self.style.color = "#7b7e7a";
                }
            });
            bind(this, "keydown", function () {
                if ((self.value === string) || (self.value.replace(/\s+/g, "") === "")) {
                    self.value = "";
                }
                self.style.color = "black";
            });
        },
        getAbsoluteOffset = function (el) {
            var total = 0,
                at = el;

            while (true) {

                total += at.offsetLeft;

                if (at.id === "rnr-feedback") {
                    break;
                }
                at = at.parentNode;
            }
            return total;
        },
        debounce = function () {
            if (debounceTimer) {
                clearTimeout(debounceTimer);
            }
            debounceTimer = setTimeout(function () {
                debounceTimer = null;
                return adjustSize();
            }, 50);
        },
        showError = function (el, msg) {
            var prev = d.createElement("div");

            prev.id = "rnr-widget-validation";
            prev.innerHTML = msg;
            css.call(prev, {
                "text-align": "left",
                "width": "284px",
                "margin": "0px 13px 13px 13px",
                "font-size": "13px",
                "color": "#bc0101"
            });

            el.parentNode.insertBefore(prev, el.nextSibling);
        },
        isFormValid = function (stuff) {
            var i,
                el = elements,
                len = el.length,
                val = "",
                type,
                mandatory,
                prev,
                mapping,

                reg = {
                    "email": /[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/,
                    "telephone": /[0-9]{9}/
                },

                //this is the list of error messages to display,

                err = config.messaging.error;

            //delete any previous error messages;
            prev = d.getElementById("rnr-widget-validation");
            if (prev) {
                prev.parentNode.removeChild(prev);
            }

            for (i = 0; i < len; i += 1) {
                val = stuff[i].value;
                type = stuff[i].getAttribute("data-type");
                mandatory = stuff[i].getAttribute("data-required");
                mapping = stuff[i].getAttribute("data-mapping");

                //innocent until proven guilty
                stuff[i].style.borderColor = "#d3d3d3";

                //if it's blank and mandatory, fail
                if ((mandatory === "true") && ((val.replace(/\s+/g, "") === "") || (val === config.fields[i].verbatim))) {
                    showError(stuff[i], err[mapping] || err[type]);
                    stuff[i].style.borderColor = "#bc0101";

                    return false;
                } else {
                    //it's not mandatory, but filled in anyway
                    //if we have a type for it, check against it
                    if (type === "telephone") { val = val.replace(/ /g, ''); stuff[i].value = val; };
                    if ((val.replace(/\s+/g, "") !== "") && (val !== config.fields[i].verbatim) && (reg[type] && !val.match(reg[type]))) {
                        showError(stuff[i], err[mapping] || err[type]);
                        stuff[i].style.borderColor = "#bc0101";

                        return false;
                    }
                }
            }

            return true;
        },

        adjustSize = function () {
            var content = d.getElementById("rnr-feedback-inner"),
                //inner = window.innerHeight || d.documentElement.clientHeight,
                responsive = window.innerWidth || d.documentElement.clientWidth;
            var rail = d.getElementById("rnr-widget-rail");


            /* RESPONSIVE "STYLE" */
            // check for mobile resolution
            if (responsive < 480) {
                //make main full screen
                if (visible) {
                    //make main full screen
                    main.style.position = "fixed";
                    //main.style.left = 0;
                    main.style.right = 0;
                    main.style.top = 0;
                    main.style.bottom = 0;
                    main.style.width = "100%";
                    main.style.height = "100%";
                    main.style.overflowY = "auto";
                    main.style.marginRight = 0;

                    bubble.style.height = "100%";
                    bubble.style.width = "100%";
                }
                else {
                    main.style.right = "-342px";
                }

                tab.style.display = "none";
                mobileTab.style.display = "block";


            }
            else {
                //tab.style.height = "105px";

                tab.style.display = "block";
                mobileTab.style.display = "none";

                //change the main container to fixed
                main.style.position = "fixed";
                main.style.width = "342px";
                bubble.style.height = "auto";
                bubble.style.width = "310px";

                //main.style.left = "auto";
                main.style.overflowY = "hidden";
                content.style.overflowY = "hidden";
                main.style.bottom = "auto";

                if (visible) {
                    main.style.right = 0;
                } else {
                    main.style.right = "-312px";
                }


            }
            return adjustHeight();
        },
        adjustHeight = function () {
            var fitHeight = window.innerHeight || d.documentElement.clientHeight;
            var content = d.getElementById("rnr-feedback-inner");
            var hoffset = main.scrollHeight + main.offsetTop;
            // check if it fits in height
            main.style.height = "auto";
            if (fitHeight < hoffset) {
                if ((fitHeight - hoffset) < 0) {
                    main.style.top = Math.max(0, (fitHeight - hoffset)) + "px";
                    main.style.top = 0;
                    main.style.bottom = "42px";
                    main.style.overflowY = "visible";

                    content.style.height = "100%";
                    content.style.overflowY = "scroll";


                }
                else {
                    main.style.bottom = "inherit";
                    main.style.overflowY = "hidden";

                    content.style.height = prevHeight || "auto";
                    content.style.overflowY = "hidden";
                }

            }
            else {
                main.style.top = Math.min(parseInt(config.tabOffset), ((fitHeight - main.clientHeight) / 2)) + "px";
            }

        },

        thankYou = function () {
            //this is essentially a duplicate of createContent's top bit
            //the reason why it's copied over is because in this case it's the same
            //but it doesn't have to be

            var f = d.createDocumentFragment(),
                close = d.createElement("div"),
                logo = d.createElement("img"),
                box = d.createElement("div"),
                power = d.createElement("img"),
                a = d.createElement("a"),
                m,
                h;

            //before clearing the content, get set the bubble height to fixed
            //bubble.style.height = (bubble.offsetHeight - 40) + "px";
            destroyContent();

            //add a close button
            close.innerHTML = "&times;";
            css.call(close, {
                "width": "20px",
                "height": "20px",
                "border-radius": "20px",
                "position": "absolute",
                "right": "10px",
                "top": "10px",
                "border": "1px solid #d3d3d3",
                "text-align": "center",
                "line-height": "20px",
                "cursor": "pointer",
                "color": "#d3d3d3",
                "font-weight": "bold",
                "font-size": "20px",
                "box-sizing": "content-box",
                "-moz-box-sizing": "content-box",
                "-webkit-box-sizing": "content-box"
            });
            bind(close, "tap", tabToggle);
            bind(close, "click", tabToggle);
            f.appendChild(close);

            //add the logo
            //logo.src = config.path + "/" + config.logoURL;
            logo.src = config.logoURL;
            css.call(logo, "margin", "0 auto");
            f.appendChild(logo);

            //add the messaging
            //title
            m = document.createElement("p");
            css.call(m, {
                "font-size": "15px",
                "font-weight": "bold",
                "line-height": "20px",
                "margin": "15px 5px",
                "color": config.colours.title
            });
            m.innerHTML = config.messaging.thankyou;
            f.appendChild(m);
            //subtitle
            m = document.createElement("p");
            css.call(m, {
                "margin": 0,
                "font-size": "13px",
                "line-height": "15px",
                "color": "#7b7e7a"
            });
            m.innerHTML = config.messaging.thankyouSub;
            f.appendChild(m);

            //add the logo
            power.src = config.path + "/poweredby.png";
            css.call(power, {
                //"position": "absolute",
                "bottom": "22px",
                "left": "50%",
                "margin": "5% auto 2.5% auto",
                "border": "none",
                "outline": "none"
            });

            //wrap the poweredby logo in an a tag
            a.setAttribute("href", "http://rantandrave.com");
            a.setAttribute("target", "_new");
            css.call(a, {
                "border": "none",
                "outline": "none"
            });
            a.appendChild(power);
            f.appendChild(a);

            bubble.appendChild(f);

            prevHeight = bubble.style.height;
            adjustSize();

        },
        Oops = function () {
            var btn = d.getElementById("rnr-widget-send"),
                elem,
                prev;

            btn.innerHTML = config.messaging.failHint;
            showError(btn.previousSibling, "<br/>" + config.messaging.failed);

            //reset elements after timeout
            setTimeout(function () {
                elem = elements;
                for (var i = 0; i < elem.length; i += 1) {
                    elem[i].disabled = false;
                }
                btn.disabled = false;
                btn.innerHTML = config.messaging.send;

                css.call(btn, {
                    "background-color": "#00AEEF",
                    "color": "white",
                    "padding": "0 10%",
                    "font-size": "18px",
                    "line-height": "36px",
                    "font-family": "inherit",
                    "border": "1px solid #0077CC",
                    "border-radius": "4px",
                    //"margin": "5% 0 2.5% 0",
                    "cursor": "pointer",
                    "display": "block",
                    "margin": "5% auto 2.5% auto"
                });

                //delete any previous error messages;
                prev = d.getElementById("rnr-widget-validation");
                if (prev) {
                    prev.parentNode.removeChild(prev);
                }
                return adjustSize();

            }, 10000);
            adjustHeight();
        },


        //sends the ajax request
        sendForm = function (query_string) {
            var ajax,
                uri = "https://demo.rantandrave.co.uk/RantAndRaveCCI/CaptureItem.do" + query_string;
            if (!window.XDomainRequest) {
                ajax = new XMLHttpRequest();

                ajax.onreadystatechange = function () {
                    if (ajax.readyState === 4) {
                        if (ajax.status === 200) {
                            return thankYou();
                        } else {
                            return Oops();
                        }
                    }
                };

                //fire away!
                ajax.open("GET", uri, true);
                ajax.setRequestHeader("Authorization", "Basic " + btoa("bocapiuserwebwidget:HJd739478UH"));
                ajax.send();
            } else {
                ajax = new XDomainRequest();

                ajax.onload = thankYou;
                ajax.onerror = ajax.ontimeout = Oops;

                //fire away!
                ajax.open("GET", uri + "&userid=bocapiuserwebwidget&password=HJd739478UH");
                ajax.send();
            }
        },

        //this gets called when the button is pressed
        prepareForm = function (e) {
            var i,
                el = elements,
                len = el.length,
                query = "?location=Web&channel=3&Score=" + d.getElementById("rnr-widget-handle").innerHTML,
                self = e.target || e.srcElement;

            //disable the send button immediately
            self.disabled = true;

            //before we do anything else, check if the form is valid
            if (isFormValid(el)) {

                //ok good, lock the elements
                self.innerHTML = config.messaging.sendHint;
                self.style.color = "#00AEEF";
                self.style.background = "white";

                // include url?

                if (config.includeURL !== "") {
                    query += "&" + config.includeURL + "=" + encodeURIComponent(window.location);
                }

                //go through the form elements and disable them
                for (i = 0; i < len; i += 1) {
                    el[i].disabled = true;
                    query += "&" + [el[i].getAttribute("data-mapping")] + "=" + encodeURIComponent(el[i].value);
                }

                sendForm(query);
            } else {
                self.disabled = false;
            }

        },

        //create the slider
        createSlider = function () {
            var f = d.createDocumentFragment(),
                image,
                box_left = f.appendChild(d.createElement("div")),
                box_center = f.appendChild(d.createElement("div")),
                box_right = f.appendChild(d.createElement("div")),
                rail = box_center.appendChild(d.createElement("div")),
                handle = rail.appendChild(d.createElement("div"));

            rail.id = "rnr-widget-rail";
            handle.id = "rnr-widget-handle";

            image = d.createElement("img");
            image.src = config.path + "/" + config.rant;
            image.width = 42;
            image.height = 57;

            css.call(image, {
                "position": "relative",
                "top": "19px",
                "left": "2px"
            });
            css.call(box_left, {
                "width": "42px",
                "height": "100%",
                "display": "inline-block",
                "vertical-align": "top"
            });
            box_left.appendChild(image);

            css.call(box_center, {
                "width": "200px",
                "height": "100%",
                "display": "inline-block",
                "vertical-align": "top",
                "position": "relative"
            });
            css.call(rail, {
                "background": "#e7e7e7",
                "height": "6px",
                "width": "174px",
                "position": "absolute",
                "top": "50%",
                "left": "13px",
                "border-radius": "6px",
                "border": "1px solid #d9d9d9"
            });
            css.call(handle, {
                "width": "28px",
                "height": "28px",
                "position": "relative",
                "margin-top": "-12px",
                "margin-left": "-14px",
                "border-radius": "28px",
                "border": "1px solid #cccccc",
                "background-color": "#dddddd",
                "background-image": "linear-gradient(#ffffff, #dddddd)",
                "box-shadow": "0px 1px 2px rgba(0,0,0,0.14)",
                "left": "50%",
                "text-align": "center",
                "line-height": "28px",
                "font-size": "15px",
                "font-weight": "bold",
                "text-shadow": "0 1px 0 rgba(255,255,255,0.65)",
                "cursor": "pointer",
                "color": "#01520c"
            });
            handle.innerHTML = config.minScoreBound + Math.round((config.maxScoreBound - config.minScoreBound) / 2);
            handle.onselectstart = function () { return false; };

            //mouse
            bind(handle, "mousedown", slide.moveStart);
            bind(main, "mousemove", slide.move);
            bind(document, "mouseup", slide.end);
            // touch
            bind(handle, "touchstart", slide.moveStart);
            bind(main, "touchmove", slide.move);
            bind(document, "touchend", slide.end);


            image = d.createElement("img");
            image.src = config.path + "/" + config.rave;
            image.width = 42;
            image.height = 57;

            css.call(image, {
                "position": "relative",
                "top": "34px"
            });
            css.call(box_right, {
                "width": "42px",
                "height": "100%",
                "display": "inline-block",
                "vertical-align": "top"
            });
            box_right.appendChild(image);

            return f;
        },

        //create a field
        createField = function (field_object) {
            var f, style = {
                "font-family": "inherit",
                "font-size": "13px",
                "min-height": "30px",
                "border-radius": "4px",
                "border": "1px solid #d3d3d3",
                //"width": "268px",
                //"margin": "5px 0",
                "outline": "none",
                "display": "block",
                "margin": "5px auto",
                "width": "90%"
            };

            if (field_object.type.toLowerCase() === "comment") {
                f = d.createElement("textarea");
                f.rows = 4;
                style["line-height"] = "15px";
                style.padding = "8px";
                style.resize = "vertical";
            } else {
                f = d.createElement("input");
                style["line-height"] = "30px";
                style.padding = "0 8px";
            }

            css.call(f, style);
            if (!supportsInputPlaceholder()) {
                placeholder.call(f, field_object.verbatim);
            }

            f.setAttribute("placeholder", field_object.verbatim);
            f.setAttribute("data-mapping", field_object.mapping);
            f.setAttribute("data-required", field_object.required);
            f.setAttribute("data-type", field_object.type);

            f.onfocus = function () {
                this.style.borderColor = config.colours.fieldFocusBorder;
            };
            f.onblur = function () {
                this.style.borderColor = config.colours.fieldBorder;
            };

            elements.push(f);
            return f;
        },

        //creates the content inside the bubble
        createContent = function (callback) {
            var f = d.createDocumentFragment(),
                close = d.createElement("div"),
                logo = d.createElement("img"),
                box = d.createElement("div"),
                fields = config.fields,
                len = fields.length,
                button = d.createElement("button"),
                i,
                m;

            //add a close button
            close.innerHTML = "&times;";
            css.call(close, {
                "width": "20px",
                "height": "20px",
                "border-radius": "20px",
                "position": "absolute",
                "right": "10px",
                "top": "10px",
                "border": "1px solid #d3d3d3",
                "text-align": "center",
                "line-height": "20px",
                "cursor": "pointer",
                "color": "#d3d3d3",
                "font-weight": "bold",
                "font-size": "20px",
                "box-sizing": "content-box",
                "-moz-box-sizing": "content-box",
                "-webkit-box-sizing": "content-box"
            });
            bind(close, "tap", tabToggle);
            bind(close, "click", tabToggle);
            f.appendChild(close);

            //add the logo
            //logo.src = config.path + "/" + config.logoURL;
            logo.src = config.logoURL;
            css.call(logo, "margin", "0 auto");
            f.appendChild(logo);

            //add the messaging
            //title
            m = document.createElement("p");
            css.call(m, {
                "margin": "5px",
                "font-size": "15px",
                "font-weight": "bold",
                "line-height": "30px",
                "color": config.colours.title
            });
            m.innerHTML = config.messaging.title;
            f.appendChild(m);
            //subtitle
            m = document.createElement("p");
            css.call(m, {
                "margin": 0,
                "font-size": "13px",
                "line-height": "15px",
                "color": "#7b7e7a"
            });
            m.innerHTML = config.messaging.subtitle.replace(/\%scale\%/g, config.minScoreBound + "-" + config.maxScoreBound);
            f.appendChild(m);
            //hint
            m = document.createElement("p");
            css.call(m, {
                "margin": 0,
                "font-size": "12px",
                "line-height": "15px",
                "color": "#7b7e7a"
            });
            m.innerHTML = config.messaging.scaleHint.replace(/\%scale1\%/g, config.minScoreBound).replace(/\%scale2\%/g, config.maxScoreBound);
            f.appendChild(m);


            //add a blank box instead of the score (for now)
            css.call(box, {
                "width": "284px",
                "height": "100px",
                //"margin": "0px 13px 4px 13px",
                "-moz-user-select": "none",
                "-khtml-user-select": "none",
                "-webkit-user-select": "none",
                "margin": "0 auto"

            });
            box.appendChild(createSlider());
            f.appendChild(box);

            //add the required fields, in the order they appear
            for (i = 0; i < len; i += 1) {
                f.appendChild(createField(fields[i]));
            }

            //add the button
            button.innerHTML = config.messaging.send;
            button.id = "rnr-widget-send";
            css.call(button, {
                "background-color": config.colours.submitBackground1,
                "background-image": "linear-gradient("
                    + config.colours.submitBackground1
                    + " 45%, "
                    + (config.colours.submitBackground2 || config.colours.submitBackground1)
                    + ")",
                "color": "white",
                "padding": "0 10%",
                "font-size": "18px",
                "line-height": "36px",
                "font-family": "inherit",
                "border": "none ",
                "border-radius": "4px",
                //"margin": "5% 0 2.5% 0",
                "cursor": "pointer",
                "display": "block",
                "margin": "5% auto 2.5% auto"
            });
            f.appendChild(button);
            bind(button, "tap", prepareForm);
            bind(button, "click", prepareForm);


            //finally, add the fragment in
            bubble.appendChild(f);

            //before we slide the thing into position, make sure it fits on screen
            prevHeight = 0;
            //main.style.top = "auto";
            //main.style.bottom = "auto";
            adjustSize();

            //we are done creating everything inside the bubble
            //let the slider do its thing
            return callback();
        },
        //clears the content inside the bubble
        destroyContent = function () {
            elements = [];
            bubble.innerHTML = "";
            //main.style.top = config.tabOffset;
            //tab.style.top = 0;
            toggling = false;
        },

        //makes the bubble toggle when you click on it
        tabToggle = function () {
            if (toggling) {
                return;
            }
            toggling = true;
            if (visible) {
                css.call(main, "right", "-312px");
                bubble.style.height = "auto";
                main.style.width = "342px";

                //d.getElementById("rnr-feedback-inner").style.width = "310px";
                setTimeout(destroyContent, config.animationTime * 1000);
                setTimeout(adjustSize, config.animationTime * 2);
            } else {
                createContent(function () {
                    css.call(main, "right", "0px");
                });
                setTimeout(adjustSize, config.animationTime * 2);
                setTimeout(function () { toggling = false; }, config.animationTime * 1000);
            }
            visible = !visible;
        },

        //creates widget dom element
        createBubble = function () {
            //create the main window
            //reuse it if we've got it, but clear everything
            main = main || d.getElementById("rnr-feedback") || d.body.appendChild(d.createElement("div"));
            main.id = "rnr-feedback";
            main.innerHTML = "";

            css.call(main, {
                "position": "fixed",
                "right": "-342px",
                "top": config.tabOffset,
                "width": "342px",
                "transition": "all " + config.animationTime + "s cubic-bezier(0.23, 1, 0.32, 1) 0s, margin " + config.animationTime + "s cubic-bezier(0.23, 1, 0.32, 1) 0s",
                "font-family": "Open Sans, Arial, Helvetica, sans-serif",
                "text-align": "center",
                "z-index": "8000"
            });

            //ok, now add the tab and the bubble
            tab = main.appendChild(d.createElement("div"));
            css.call(tab, {
                "width": "32px",
                "height": "105px",
                "position": "absolute",
                "left": 0,
                "top": 0,
                "border-radius": "3px 0 0 3px",
                "background": config.colours.tabBackground + " url(" + config.path + "/rnr_icon2.png) left center no-repeat",
                "cursor": "pointer"
            });
            tab.onmouseover = function () {
                this.style.backgroundColor = config.colours.tabBackgroundHover;
            };
            tab.onmouseout = function () {
                this.style.backgroundColor = config.colours.tabBackground;
            };

            //append mobile tab handler
            mobileTab = main.parentNode.appendChild(d.createElement("div"));
            css.call(mobileTab, {
                "width": "50px",
                "height": "28px",
                "position": "absolute",
                "z-index": "8500",
                "line-height": "25px",
                "right": 0,
                "top": config.tabOffset,
                "border-radius": "3px 0 0 3px",
                "background": config.colours.tabBackground, //" url(" + config.path + "/rnr_icon2.png) left center no-repeat",
                "cursor": "pointer",
                "transition": "width " + config.animationTime + "s cubic-bezier(0.23, 1, 0.32, 1) 0s",
                "display": "none"
            });

            // add images to the tab
            var rant = mobileTab.appendChild(d.createElement("div"));
            css.call(rant, {
                "background": "url(" + config.path + "/rant_small.png) left center no-repeat",
                "width": "20px",
                "height": "25px",
                "display": "inline-block",
                "margin-left": "5px",
                "transition": "all " + config.animationTime + "s cubic-bezier(0.23, 1, 0.32, 1) 0s"
            });

            var rave = mobileTab.appendChild(d.createElement("div"));
            css.call(rave, {
                "background-color": config.colours.tabBackground,
                "background": "url(" + config.path + "/rave_small.png) left center no-repeat",
                "width": "25px",
                "height": "25px",
                "display": "inline-block"

            });
            bind(mobileTab, "tap", tabToggle);
            bind(mobileTab, "click", tabToggle);

            bubble = main.appendChild(d.createElement("div"));
            bubble.id = "rnr-feedback-inner";
            css.call(bubble, {
                "width": "310px",
                "height": "auto",
                "float": "right",
                "min-height": "105px",
                "background": "white",
                "position": "relative",
                "border": "1px solid #e3e3e3",
                "padding": "20px 0",
                "border-bottom-left-radius": "14px"
            });

            //add the event handler for the tab, so it shows/hides when you press it
            bind(tab, "tap", tabToggle);
            bind(tab, "click", tabToggle);
            adjustSize();
        },

        //main initialization
        widget = function () {
            createBubble();
            bind(window, "resize", debounce);
            createContent(adjustHeight);
            setTimeout(destroyContent, config.animationTime * 1000);


            //allright everything done, we may now show the handle
            setTimeout(function () { css.call(main, "right", "-312px"); adjustSize() }, config.graceTime * 1000);
        };

    window.rnr_widget_load = widget;
}());