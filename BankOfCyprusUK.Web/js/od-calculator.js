﻿$(function () {

    $(document).ready(function () {
        $("#AMslider-value").val(0);
        $("#AM-slider").slider({
            range: "min",
            value: 0,
            min: 0,
            max: 5000,
            step: 5,   
            stop: function (event, ui) {
                var value = $("#AM-slider").slider("option", "value");
                $('#AMslider-value').val(value);
            },
            slide: function (event, ui) {
                var value = $("#AM-slider").slider("option", "value");
                $('#AMslider-value').val(value);
            }
        });

        $("#AMslider-value").keyup(function (e) {  
            $("#AM-slider").slider("option", "value", parseInt($(this).val()));         
        });
    });

    $(document).ready(function () {
        $("#odSlider-value").val(0);
        $("#odSlider").slider({
            range: "min",
            value: 0,
            min: 0,
            max: 5000,
            step: 5,   
            stop: function (event, ui) {
                var value = $("#odSlider").slider("option", "value");
                $('#odSlider-value').val(value);
            },
            slide: function (event, ui) {
                var value = $("#odSlider").slider("option", "value");
                $('#odSlider-value').val(value);
            }
        });

        $("#odSlider-value").keyup(function (e) {
            $("#odSlider").slider("option", "value", parseInt($(this).val()));          
        });
    });

    $(document).ready(function () {
        $("#odDaysSlider-value").val(1);
        $("#odDaysSlider").slider({
            range: "min",
            value: 1,
            min: 1,
            max: 31,
            step: 1,
            stop: function (event, ui) {
                var value = $("#odDaysSlider").slider("option", "value");
                $('#odDaysSlider-value').val(value);
            },
            slide: function (event, ui) {
                var value = $("#odDaysSlider").slider("option", "value");
                $('#odDaysSlider-value').val(value);
            }
        });

        $("#odDaysSlider-value").keyup(function () {
            $("#odDaysSlider").slider("option", "value", parseInt($(this).val()));
        });
    });

    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $("#resetLink").click(function () {

        // Cancel the default action
        e.preventDefault();

        $([document.documentElement, document.body]).animate({
            scrollTop: $("#calculator").offset().top - 60
        }, 1000);

        // Temp reset
        setTimeout(function () {      
            window.location.href = window.location.href;
        }, 3000);
    });
            
    // Restricts input for each element in the set of matched elements to the given inputFilter.
    (function($) {
        $.fn.inputFilter = function(inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
            if (inputFilter(this.value)) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
            this.value = "";
            }
        });
        };
    }(jQuery));


    // Install input filters.
    $("#AMslider-value").inputFilter(function(value) {
        return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 5000); });
    $("#odSlider-value").inputFilter(function(value) {
        return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 5000); });
    $("#odDaysSlider-value").inputFilter(function(value) {
        return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 31);
    });


});

function validateOverdraftCalculator() {
}

function resetCalculator() {
    $([document.documentElement, document.body]).animate({
        scrollTop: $("#calculator").offset().top - 60
    }, 1000);

    // Temp reset
    setTimeout(function () {
        location.reload();
    }, 800);
}

function CheckIfSliderIsValid(value) {
    if (value % 5 === 0) {
        return true;
    }
    else {
        return false;
    }
}

function delay(callback, ms) {
    var timer = 0;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}


window.Parsley.addValidator('multipleOf', {
    validateNumber: function (value, requirement) {
        return value % requirement === 0;
    },
    requirementType: 'integer',
    messages: {
        en: 'Please use multiples of £%s.'
    }
});

var $selector = $('#odCalculator'),
    form = $selector.parsley();

$selector.find('button').click(function () {
    var isValid = form.validate();
    if (isValid) {

        //var baseRate = 2.75;
        //var authorisedOverdraftMarginAboveBaseApplied = 8;
        //var unauthorisedOverdraftMarginAboveBaseApplied = 19;

        //var isOverdrawm = false;

        //var unauthorisedOverdraftBalance = amountOverdrawn;



        var data = {};
        data.arrangedOverdraft = $("#AMslider-value").val();
        data.amountOverdrawn = $("#odSlider-value").val();
        data.numberOfDays = $("#odDaysSlider-value").val();

        // Ajax post to controller
        var url = "/MainSurface/CalculateOverdraft";
        $.ajax({
            type: "GET",
            url: url,
            data: data,
            cache: false,
            dataType: "html",
            contentType: "application/html; charset=utf-8",
            traditional: true
        })
        .success(function (result) {
            // Update Partial View and show the results
            $("#results").html(result);
            $("#results").show();

            // Scroll to results
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#results").offset().top - 60
            }, 1000);
        })
        .fail(function (ex) {
            $.error(ex);
        });
    }
});