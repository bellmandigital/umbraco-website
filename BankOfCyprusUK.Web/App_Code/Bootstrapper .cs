﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using Umbraco.Core;

namespace BankOfCyprusUK.Web.App_Start
{
    public class Bootstrapper : IApplicationEventHandler
    {
        public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }

        public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }

        public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {

            BundleTable.Bundles.Add(new ScriptBundle("~/bundles/js")
                .Include("~/js/jquery-matchHeight.js")
                .Include("~/js/bootstrap-tabcollapse.js")
                .Include("~/js/slick.min.js")
                .Include("~/js/jquery-fancybox.min.js")
                .Include("~/js/parsley.min.js")
                .Include("~/js/od-calculator.js")
                .Include("~/js/main.js"));

            BundleTable.Bundles.Add(new StyleBundle("~/bundles/styles")
                .Include("~/css/style.css")
                .Include("~/css/jquery-ui.min.css")
                .Include("~/css/bootstrap-slider.css")
                .Include("~/css/calc-styles.css"));

            // Enable to force bundling on dev environment
            BundleTable.EnableOptimizations = true;
        }
    }
}