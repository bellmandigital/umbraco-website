﻿using System.Globalization;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace BankOfCyprusUK.Web.Models
{
	public class SearchRenderModel : RenderModel
	{
		public SearchRenderModel() : this(UmbracoContext.Current.PublishedContentRequest.PublishedContent, CultureInfo.CurrentCulture) { }

		public string Query { get; set; }

		public SearchRenderModel(IPublishedContent content, CultureInfo culture) : base(content, culture)
		{
		}

		public SearchRenderModel(IPublishedContent content) : base(content)
		{
		}
	}
}