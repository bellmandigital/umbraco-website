﻿using System.Collections.Generic;
using Umbraco.Core.Models;

namespace BankOfCyprusUK.Web.Models
{
	public class SearchResultViewModel
	{
		public IEnumerable<IPublishedContent> Results { get; set; }
		public string Query { get; set; }
	}

}