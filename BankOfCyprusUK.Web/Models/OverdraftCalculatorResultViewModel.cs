﻿namespace BankOfCyprus.OverdraftCalculator.Models
{
    public class OverdraftCalculatorResultViewModel
    {
        public decimal ArrangedOverdraftAmount { get; set; }
        public int NumberOfDays { get; set; }
        public bool IsUnauthorised { get; set; }
        public decimal UnArrangedOverdraftCharge { get; set; }
        public decimal CostOfBorrowing { get; set; }
        public decimal ChargedArrangeOverdraftInterestEAR { get; set; }

    }
}
