﻿using System.Collections.Generic;
using Umbraco.Web.PublishedContentModels;

namespace BankOfCyprusUK.Web.Models
{
	public class TeamMembersViewModel
	{
		public IList<TeamMember> Members { get; set; }
		public Team Team { get; set; }
	}
}