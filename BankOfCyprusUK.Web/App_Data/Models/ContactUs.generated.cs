//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace Umbraco.Web.PublishedContentModels
{
	/// <summary>Contact Us</summary>
	[PublishedContentModel("contactUs")]
	public partial class ContactUs : PublishedContentModel, IMeta
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "contactUs";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public ContactUs(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<ContactUs, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Display tag line: Sets if the tag line should be displayed or not
		///</summary>
		[ImplementPropertyType("displayTagLine")]
		public bool DisplayTagLine
		{
			get { return this.GetPropertyValue<bool>("displayTagLine"); }
		}

		///<summary>
		/// Icons
		///</summary>
		[ImplementPropertyType("icons")]
		public IEnumerable<IPublishedContent> Icons
		{
			get { return this.GetPropertyValue<IEnumerable<IPublishedContent>>("icons"); }
		}

		///<summary>
		/// Map Background
		///</summary>
		[ImplementPropertyType("mapBackground")]
		public IPublishedContent MapBackground
		{
			get { return this.GetPropertyValue<IPublishedContent>("mapBackground"); }
		}

		///<summary>
		/// Map Body
		///</summary>
		[ImplementPropertyType("mapBody")]
		public IHtmlString MapBody
		{
			get { return this.GetPropertyValue<IHtmlString>("mapBody"); }
		}

		///<summary>
		/// Map Link Title
		///</summary>
		[ImplementPropertyType("mapLinkTitle")]
		public string MapLinkTitle
		{
			get { return this.GetPropertyValue<string>("mapLinkTitle"); }
		}

		///<summary>
		/// Map Link Url
		///</summary>
		[ImplementPropertyType("mapLinkUrl")]
		public string MapLinkUrl
		{
			get { return this.GetPropertyValue<string>("mapLinkUrl"); }
		}

		///<summary>
		/// Page Header
		///</summary>
		[ImplementPropertyType("pageHeader")]
		public IEnumerable<IPublishedContent> PageHeader
		{
			get { return this.GetPropertyValue<IEnumerable<IPublishedContent>>("pageHeader"); }
		}

		///<summary>
		/// Tabs
		///</summary>
		[ImplementPropertyType("tabs")]
		public IEnumerable<IPublishedContent> Tabs
		{
			get { return this.GetPropertyValue<IEnumerable<IPublishedContent>>("tabs"); }
		}

		///<summary>
		/// Tag Line: It will default to using the home page tag line if this is not filled in and the use tag line box if ticked
		///</summary>
		[ImplementPropertyType("tagLine")]
		public string TagLine
		{
			get { return this.GetPropertyValue<string>("tagLine"); }
		}

		///<summary>
		/// Meta Description
		///</summary>
		[ImplementPropertyType("metaDescription")]
		public string MetaDescription
		{
			get { return Umbraco.Web.PublishedContentModels.Meta.GetMetaDescription(this); }
		}

		///<summary>
		/// Meta Keywords
		///</summary>
		[ImplementPropertyType("metaKeywords")]
		public string MetaKeywords
		{
			get { return Umbraco.Web.PublishedContentModels.Meta.GetMetaKeywords(this); }
		}

		///<summary>
		/// Page Title
		///</summary>
		[ImplementPropertyType("pageTitle")]
		public string PageTitle
		{
			get { return Umbraco.Web.PublishedContentModels.Meta.GetPageTitle(this); }
		}

		///<summary>
		/// Hide from Navigation: Tick to hide from navigation menu
		///</summary>
		[ImplementPropertyType("umbracoNaviHide")]
		public bool UmbracoNaviHide
		{
			get { return Umbraco.Web.PublishedContentModels.Meta.GetUmbracoNaviHide(this); }
		}
	}
}
