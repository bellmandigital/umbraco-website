//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace Umbraco.Web.PublishedContentModels
{
	/// <summary>Rates</summary>
	[PublishedContentModel("rates")]
	public partial class Rates : PublishedContentModel, IMeta
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "rates";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public Rates(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<Rates, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Base Rates Content
		///</summary>
		[ImplementPropertyType("baseRatesContent")]
		public IHtmlString BaseRatesContent
		{
			get { return this.GetPropertyValue<IHtmlString>("baseRatesContent"); }
		}

		///<summary>
		/// Benchmark Transparency
		///</summary>
		[ImplementPropertyType("benchmarkTransparency")]
		public IHtmlString BenchmarkTransparency
		{
			get { return this.GetPropertyValue<IHtmlString>("benchmarkTransparency"); }
		}

		///<summary>
		/// Display tag line: Sets if the tag line should be displayed or not
		///</summary>
		[ImplementPropertyType("displayTagLine")]
		public bool DisplayTagLine
		{
			get { return this.GetPropertyValue<bool>("displayTagLine"); }
		}

		///<summary>
		/// Footer Text: Copy to be placed AFTER the tabs
		///</summary>
		[ImplementPropertyType("footerText")]
		public IHtmlString FooterText
		{
			get { return this.GetPropertyValue<IHtmlString>("footerText"); }
		}

		///<summary>
		/// Foreign Exchange Post-Table Text: Copy to be place AFTER table on Exchange Rates tab
		///</summary>
		[ImplementPropertyType("foreignExchangePostTableText")]
		public IHtmlString ForeignExchangePostTableText
		{
			get { return this.GetPropertyValue<IHtmlString>("foreignExchangePostTableText"); }
		}

		///<summary>
		/// Foreign Exchange Pre-Table Text: Copy to be place BEFORE table on Exchange Rates tab
		///</summary>
		[ImplementPropertyType("foreignExchangePreTableText")]
		public IHtmlString ForeignExchangePreTableText
		{
			get { return this.GetPropertyValue<IHtmlString>("foreignExchangePreTableText"); }
		}

		///<summary>
		/// Interest Rates Post-Table Text: Copy to be place AFTER table on Interest Rates tab
		///</summary>
		[ImplementPropertyType("interestRatesPostTableText")]
		public IHtmlString InterestRatesPostTableText
		{
			get { return this.GetPropertyValue<IHtmlString>("interestRatesPostTableText"); }
		}

		///<summary>
		/// Interest Rates Pre-Table Text: Copy to be place BEFORE table on Interest Table tab
		///</summary>
		[ImplementPropertyType("interestRatesPreTableText")]
		public IHtmlString InterestRatesPreTableText
		{
			get { return this.GetPropertyValue<IHtmlString>("interestRatesPreTableText"); }
		}

		///<summary>
		/// Page Header
		///</summary>
		[ImplementPropertyType("pageHeader")]
		public IEnumerable<IPublishedContent> PageHeader
		{
			get { return this.GetPropertyValue<IEnumerable<IPublishedContent>>("pageHeader"); }
		}

		///<summary>
		/// Personal Savings Allowance Content
		///</summary>
		[ImplementPropertyType("personalSavingsAllowanceContent")]
		public IHtmlString PersonalSavingsAllowanceContent
		{
			get { return this.GetPropertyValue<IHtmlString>("personalSavingsAllowanceContent"); }
		}

		///<summary>
		/// Tag Line: It will default to using the home page tag line if this is not filled in and the use tag line box if ticked
		///</summary>
		[ImplementPropertyType("tagLine")]
		public string TagLine
		{
			get { return this.GetPropertyValue<string>("tagLine"); }
		}

		///<summary>
		/// Travel Money Content
		///</summary>
		[ImplementPropertyType("travelMoneyContent")]
		public IHtmlString TravelMoneyContent
		{
			get { return this.GetPropertyValue<IHtmlString>("travelMoneyContent"); }
		}

		///<summary>
		/// Meta Description
		///</summary>
		[ImplementPropertyType("metaDescription")]
		public string MetaDescription
		{
			get { return Umbraco.Web.PublishedContentModels.Meta.GetMetaDescription(this); }
		}

		///<summary>
		/// Meta Keywords
		///</summary>
		[ImplementPropertyType("metaKeywords")]
		public string MetaKeywords
		{
			get { return Umbraco.Web.PublishedContentModels.Meta.GetMetaKeywords(this); }
		}

		///<summary>
		/// Page Title
		///</summary>
		[ImplementPropertyType("pageTitle")]
		public string PageTitle
		{
			get { return Umbraco.Web.PublishedContentModels.Meta.GetPageTitle(this); }
		}

		///<summary>
		/// Hide from Navigation: Tick to hide from navigation menu
		///</summary>
		[ImplementPropertyType("umbracoNaviHide")]
		public bool UmbracoNaviHide
		{
			get { return Umbraco.Web.PublishedContentModels.Meta.GetUmbracoNaviHide(this); }
		}
	}
}
