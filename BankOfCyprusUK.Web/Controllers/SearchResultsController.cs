﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BankOfCyprusUK.Web.Models;
using Umbraco.Core.Models;
using Umbraco.Web.Mvc;
using Umbraco.Web.PublishedContentModels;

namespace BankOfCyprusUK.Web.Controllers
{
	public class SearchResultsController : RenderMvcController
	{
		// GET: StandardPage
		[HttpPost]
		public ActionResult SearchResults(SearchRenderModel model)
		{
			//var search = ExamineManager.Instance.SearchProviderCollection["ExternalSearcher"];
			//var query = model.Query.Replace(" ", "+");
			//var criteria = search.CreateSearchCriteria(BooleanOperation.Or);
			//var compiled = criteria.Field("nodeName", query).Or().Field("title", query).Or().Field("content", query).Or().Field("tagline", query).Compile();
			//var results = search.Search(compiled);

			//results = search.Search(model.Query, true);
			if (model.Query == null) model.Query = "";

			var list = Umbraco.TypedSearch(model.Query);

			var result = new SearchResultViewModel
			{
				Query = model.Query,
				Results = list
			};


			Session["Results"] = result;
			return base.Index(model);
		}
	}
}