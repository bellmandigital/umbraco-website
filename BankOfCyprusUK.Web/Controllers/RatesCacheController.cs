﻿using System;
using System.Web.Mvc;
using BankOfCyprusUK.Web.Code;

namespace BankOfCyprusUK.Web.Controllers
{
	public class RatesCacheController : Controller
	{
		// GET: Cache
		public ActionResult Refresh()
		{
			RatesManager.RefreshCache();
			return Content("Rates refreshed at " + DateTime.Now);
		}
	}
}