﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BankOfCyprus.OverdraftCalculator.Models;
using BankOfCyprusUK.Web.Code;
using BankOfCyprusUK.Web.Models;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace BankOfCyprusUK.Web.Controllers    
{
	public class MainSurfaceController : Controller
	{
        private ICalculatorService CalculatorService { get; set; } = new CalculatorService();
		private UmbracoHelper Umbraco = new UmbracoHelper(UmbracoContext.Current);

		public PartialViewResult RenderTeamMembers(string teamName, string memberName, int sectionNode)
		{
			var settings = Umbraco.TypedContentAtRoot().First(n => n.DocumentTypeAlias == "settings");
			var teamNode = settings.FirstChild<TeamFolder>();
			var section = teamNode.DescendantsOrSelf<TeamSection>().Single(n => n.Id == sectionNode);

			var teams = teamName == "All"
				? section.Teams.Select(n => new Team(n)).ToList()
				: teamNode.DescendantsOrSelf<Team>().Where(n => n.Name == teamName).ToList();

			var members = teams.SelectMany(n => n.Members.Select(m => new TeamMember(m)));
			if (memberName != "All") members = new List<TeamMember> {members.First(n => n.GetKey() == new Guid(memberName))};
			
			var model = new TeamMembersViewModel
			{
				Team = teams.Count== 1 ? teams[0] : null,
				Members = members.ToList()
			};

			return PartialView(model);
		}

		[HttpPost]
		public JsonResult GetTeamMembers(string teamName, int sectionNode)
		{
			var settings = Umbraco.TypedContentAtRoot().First(n => n.DocumentTypeAlias == "settings");
			var teamNode = settings.FirstChild<TeamFolder>();
			var section = teamNode.DescendantsOrSelf<TeamSection>().Single(n => n.Id == sectionNode);

			var teams = teamName == "All"
				? section.Teams.Select(n => new Team(n)).ToList()
				: teamNode.DescendantsOrSelf<Team>().Where(n => n.Name == teamName).ToList();

			//var teamIds = teams.Select(n => n.Id);

			var members = teams.SelectMany(n => n.Members.Select(m => new TeamMember(m)));

			return Json(members.Select(n => new
			{
				n.Name,
				Key = n.GetKey()
			}), JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult GetRatesTableNamesByCategory(string categoryName)
		{
			var settings = new Settings(Umbraco.TypedContentAtRoot().First(n => n.DocumentTypeAlias == "settings"));
			var rates = settings.Descendants<RatesTable>().Select(n => new RatesTable(n)).ToList();
			var names = rates.Where(n => n.Type == categoryName).ToList();

			return Json(names.Select(n => n.Name), JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult RenderRatesTable(string name, string category, bool isExchange)
		{
			var settings = new Settings(Umbraco.TypedContentAtRoot().First(n => n.DocumentTypeAlias == "settings"));
			var rates = settings.Descendants<RatesTable>().Select(n => new RatesTable(n)).ToList();
			var cats = isExchange ? new List<string> { "Foreign Exchange" } : rates.Where(n => n.Type != "Foreign Exchange").Select(n => n.Type).ToList(); 

			IList<string> names;

			if (name != "All" && category != "All")
			{
				names = new List<string> {name};
			}
			else if (name == "All" && category == "All")
			{

				names = rates.Where(n => cats.Contains(n.Type)).Select(n => n.Name).ToList();
			}
			else if (name == "All" && ! isExchange)
			{
				names = rates.Where(n => n.Type == category).Select(n => n.Name).ToList();
			}
			else if (name == "All" && isExchange)
			{
				names = rates.Where(n => cats.Contains(n.Type)).Select(n => n.Name).ToList();
			}
			else
			{
				names = new List<string>();
			}


			var result = rates.Where(n => names.Contains(n.Name)).Select(n => new RatesTable(n)).ToList();

			return PartialView(result);
		}

        public ActionResult RenderOverdraftCalculatorResults()
        {
            var viewModel = new OverdraftCalculatorResultViewModel
            {
                ArrangedOverdraftAmount = 0,
                UnArrangedOverdraftCharge = 0,
                ChargedArrangeOverdraftInterestEAR = 0,
                CostOfBorrowing = 0,
                NumberOfDays = 0
            };

            return PartialView("OverdraftCalculator/Results", viewModel);
        }

		/// <summary>
		/// This method is for the original overdraft calculator developed in the Overdraft section of the website.
		/// </summary>
		public ActionResult CalculateOverdraft(int arrangedOverdraft, int amountOverdrawn, int numberOfDays)
        {
            var overdraftCalculatorNode = Umbraco.TypedContent(Constants.OverdraftCalculatorNodeId);

            var baseRate = CalculatorService.GetBaseRate(overdraftCalculatorNode);
            var authRate = CalculatorService.GetAuthorisedRate(overdraftCalculatorNode);
            var unAuthRate = CalculatorService.GetUnauthorisedRate(overdraftCalculatorNode);

            var model = CalculatorService.GetResults(arrangedOverdraft, amountOverdrawn, numberOfDays, baseRate, authRate, unAuthRate);
            return PartialView("OverdraftCalculator/Results", model);
        }

		/// <summary>
		/// This method is for seperate rates in April 2020 so goes to a different content node in the backoffice to get different values for the rates.
		/// </summary>
		public ActionResult CalculateOverdraftApril(int arrangedOverdraft, int amountOverdrawn, int numberOfDays)
		{
			var overdraftCalculatorNode = Umbraco.TypedContent(Constants.OverdraftCalculatorAprilNodeId);

			var baseRate = CalculatorService.GetBaseRate(overdraftCalculatorNode);
			var authRate = CalculatorService.GetAuthorisedRate(overdraftCalculatorNode);
			var unAuthRate = CalculatorService.GetUnauthorisedRate(overdraftCalculatorNode);

			OverdraftCalculatorResultViewModel model = CalculatorService.GetResults(arrangedOverdraft, amountOverdrawn, numberOfDays, baseRate, authRate, unAuthRate);
			return PartialView("OverdraftCalculator/Results", model);
		}

	}
}